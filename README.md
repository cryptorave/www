CRYPTORAVE
==========

Intro
-----

Hello, this is CryptoRave's website repository.

Content
-------

  - CR2014, CR2015, CR2016: Basically HTML+PHP+CSS+js. You don't need any special program to deploy.
  - CR2017, CR2018, CR2019: `hugo`

Hugo instructions
-----------------

1. Install `hugo` in your machine, for example, `apt install hugo`
2. Clone this repository: `git clone https://0xacab.org/cryptorave/www.git`
3. Enter in the directory/CRYYYY and run `./start-hugo-webserver.sh`
4. The dev website will be available in http://localhost:1313
