---
language_code: "pt-br"
title: "CryptoRave 2019"

header:
  descricao:
    " "
sobre:
  id: "sobre"
  descricao:
    [
      "Entre os dias **3 e 4 de maio de 2019** será realizada a **sexta edição da CryptoRave**, evento anual que reúne, em 36 horas, diversas atividades sobre segurança, criptografia, hacking, anonimato, privacidade e liberdade na rede.",
      "A CryptoRave é **aberta e gratuita** - basta [inscrição online](#inscricao) - e será realizada na [cidade de São Paulo](#local). Inspirada em uma [ação global](https://cryptoparty.in), descentralizada para disseminar e democratizar o conhecimento e conceitos básicos de criptografia e software livre, o evento teve início em 2014, como reação à divulgação de informações que confirmaram a ação de governos para manter a população mundial sob vigilância e monitoramente constante. " 
    ]
  vimeo_link: "https://www.youtube.com/embed/Ow8CEj665xM"

nav:
  descricao:
    "Logo da CR2019, em cor branca com fundo preto: à esquerda, as letras 'CR' formadas pelos números 0 e 1; ao meio, uma imagem de um processador com uma chave pixelada em seu centro; à direita, o número '2019' escrito em duas linhas (com dois algarismos por linha) em fonte menor, de forma que a soma de suas alturas seja igual à altura do texto 'CR'."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamento"
  inscricao: "Inscrição"
  programacao: "Programação"
  realizacao: "Realização"
  apoio: "Apoio"
  contato: "Contato"
  local: "Local"
  menu_extra:
#    - name: "Grade"
#      url: "https://cpa.cryptorave.org/pt-BR/cr2019/public/schedule"
#      weight: 9"
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamento Coletivo"
  descricao: "A Cryptorave é realizada com apoio financeiro e de divulgação de uma comunidade distribuída, anônima, mas que garante, ano após ano, que mais pessoas participem do evento. Nós da organização fazemos a nossa parte ao elaborarmos recompesas maravilhosas para quem apoia, mas não ficamos com nada. Tudo é investido no evento. "
  link_catarse: "https://www.catarse.me/pt/projects/91191/embed"
  titulo_btc: "Colabore em Bitcoin"
  descricao_btc: "Nós temos uma carteira de bitcoin, você pode doar para o endereço abaixo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para o endereço Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
  recompensas:
    - linha:
      - valor: "R$15"
        img: ""
        descricao_img: ''
        descricao: "**FREE HUGS** - AGRADECIMENTO escrito nas nuvens (site)"
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168542"

      - valor: "R$25"
        img: ""
        descricao_img: ""
        descricao: "**P4553LIVRE** - 2 ADESIVOS CR2019 + 1 HABEAS CORPUS + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168543"

      - valor: "R$40"
        img: ""
        descricao_img: ""
        descricao: "**5T1CK3RLIVRE** - 4 ADESIVOS PARA COBRIR A WEBCAM + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168544"

      - valor: "R$70"
        img: ""
        descricao_img: ""
        descricao: "**C4M3R4LIVRE** - 1 TAMPA DE CÂMERA ABRE E FECHA + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168545"

      - valor: "R$100"
        img: ""
        descricao_img: ""
        descricao: "**B46LIVRE** - 1 ECOBAG CR2019 + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168546"

      - valor: "R$130"
        img: ""
        descricao_img: ""
        descricao: "**K4N3K4LIVRE** - 1 CANECA PRETA CR2019 + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168547"

      - valor: "R$150"
        img: ""
        descricao_img: ""
        descricao: "**C4M153T4LIVRE** - 1 CAMISETA CR2019 + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168548"

      - valor: "R$350"
        img: ""
        descricao_img: ""
        descricao: "**1NV3RN0LIVRE** - 1 MOLETOM CR2019 + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168550"

      - valor: "R$450"
        img: ""
        descricao_img: ""
        descricao: "**P4C073LIVRE** - 1 MOLETON CR2019 + 1 CAMISETA CR2019 + 4 ADESIVOS PARA COBRIR A WEBCAM + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168745"

      - valor: "R$650"
        img: ""
        descricao_img: ""
        descricao: "**7UD0LIVRE** - 1 MOLETON CR2019 + 1 CAMISETA CR2019 + 1 CANECA PRETA CR2019 + 1 ECOBAG CR2019 + 1 TAMPA CÂMERA ABRE E FECHA + 4 ADESIVOS PARA COBRIR A WEBCAM + 2 adesivos CR2019 + habeas corpus + agradecimento nas nuvens Todas as recompensas devem ser retiradas no dia do evento."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168746"

      - valor: "R$1000"
        img: ""
        descricao_img: ""
        descricao: "**0F1C1N4LIVRE** - 1 OFICINA DE SEGURANÇA PARA SEU GRUPO + agradecimento nas nuvens Essa recompensa deve ser agendada com a equipe da CryptoRave e executada até Outubro de 2019."
        url: "https://www.catarse.me/projects/91191/contributions/new?reward_id=168746"

inscricao:
  id: "inscricao"
  titulo: "Inscrição"
  descricao: "A inscrição e a participação na CryptoRave é totalmente gratuita. No dia do evento, você deverá apresentar o ticket da sua inscrição." 
  link_ticket: "[Clique aqui para fazer a sua inscrição na CryptoRave](https://tickets.cryptorave.org/cr2019/free)"
  descricao_boletim: "Para acompanhar as atividades e saber mais sobre a programação,"
  link_boletim: "[receba as últimas novidades da CryptoRave](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: ""
  nome: "[]()"
  bio: "."
  descricao: "."


programacao:
  id: "programacao"
  titulo: "Programação"
  descricao:
    [
      "A versão 0.1 da programação da CryptoRave 2019 já está disponível: [agenda.cryptorave.org](https://agenda.cryptorave.org)!", 
      "A CR2019 começará às 19 horas do dia 3 de maio de 2019 e vai até às 22h do dia 4 de maio de 2019. Nessa edição o after party da CryptoRave será na [trackers](https://trackers.cx) conjuntamente com o projeto Dispersão e Pure Data, a partir das 23h!",
      "Baixe o APP da programação para [Android](https://play.google.com/store/apps/details?id=org.cryptorave.schedule&hl=en_US)."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'Logo da CR2017.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'Logo da CR2016.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'Logo da CR2015.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'Logo da CR2014.'
        url: "https://2014.cryptorave.org"


local:
  id: "local"
  titulo: "Local"
  descricao:
    [
      "A CryptoRave 2019 será realizada na [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "Endereço:  R. da Consolação, 94 - República, São Paulo - SP. Ao lado da estação do metrô Anhangabaú."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64346992969513%2C-23.548138354058587%2C-46.64142608642579%2C-23.546663037375726&amp;layer=mapnik&amp;marker=-23.547400697786546%2C-46.642448008060455"
    link: "https://osm.org/go/M~ziJkQVL?way=48661210"
    tooltip: "Clique para abrir em outra aba"


realizacao:
  id: "realizacao"
  titulo: "Realização"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo da Escola de Ativismo: o texto 'ESCOLA DE ATIVISMO' em branco no centro de um retângulo preto. O retângulo tem largura pouco maior que a do texto e altura aproximadamente quatro vezes a altura do texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo da Actantes: com fundo branco, as letras 'ACT' em vermelho, e, em seguida, as letras 'ANTES' formadas pelos números 0 e 1 em preto. Não há espaçamento entre as duas partes; o logo é uma palavra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo do Saravá: ao centro de um fundo retangular preto, a palavra 'SARAVÁ' em branco em uma fonte que se assemelha a texto manuscrito. A altura das letras varia entre cerca de um terço e dois terços da altura do retângulo. Horizontalmente, o texto ocupa cerca de quatro quintos do comprimento do retângulo."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Logo do Intervozes, laranja em fundo branco: um quadrado com vértices arredondados com arcos de círculo brancos de larguras diversas atravessando-o; à direita, em duas linhas com altura total pouco menor que a do quadrado, os textos 'intervozes' (na primeira liha) e 'coletivo brasil de comunicação social (na segunda linha)."
        url: "http://intervozes.org.br"
    - linha:
      - img: "encriptatudo.png"
        descricao_img: "Logo do Encripta, em branco: centralizados em um fundo retangular preto, a imagem pixelada de uma chave com uma extremidade cercada por colchetes à esquerda, seguida do texto 'encripta'. A imagem ocupa cerca de metade da altura do retângulo, e o texto, cerca de um terço. A largura do retângulo de fundo é ligeiramente maior do que a imagem seguida do texto."
        url: "https://encripta.org"

apoio:
  id: "apoio"
  titulo: "Apoio"
  parceiros:
    - linha:
      - img: "ccc.jpg"
        descricao_img: "Logo do Chaos Computer Club (CCC): em preto em um fundo branco, os contornos de uma imagem que se assemelha a uma imagem e a um circuito eletrônico. De dentro de um retângulo com os cantos chanfrados, 4 linhas se direcionam ao centro do lado direito; de lá, se alinham perpendicularmente a este lado e saem do retângulo, paralelas entre si. Após uma distância de cerca de metade do comprimento do retângulo, as linhas se torcem, formando um nó. Se assemelhando a fios, as linhas pendem torcidas abaixo do nó."
        url: "https://ccc.de/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Patrocinadores"
  descricao: "Agradecemos de coração a todas e todos que contribuíram com a nossa campanha de [financiamento coletivo](#financiamento):"
  doadores: 
    [
[
"Adriano pqL",
"Alan Fachini",
"Alana Domit Bittar",
"Alessandra Iturrieta",
"Alexandre Augusto Prudente de Oliveira Nascimento",
"Alexandre de Lima",
"Alexandre Martins Araujo",
"Alexandre Okita",
"Alisson Moro Neocatto",
"Amanda Julia Vieira",
"Amanda Rahra ",
"Ana Clara Nobre",
"Ana Cláudia Alencar ",
"Ana Laura Chioca Vieira",
"Ana Luiza Portello Bastos",
"Ana Maria Ghiringhello",
"Ana Rüsche",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson da Silva Croccia",
"Anderson Pereira Leal",
"Anderson Queiroz de Oliveira",
"André Fernandes",
"Andre Giolito",
"Andre Henrique Cunha de Moraes",
"André Pasti",
"André Scherma Soléo",
"Andrew Fishman",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Antonio Marino Brigagão Rodrigues",
"Ari Mensdes",
"Arikkk",
"Armando Caputi",
"Arthur Fucher",
"Augusto Bennemann",
"Avelino Morganti Neto",
"Barbara Borba",
"Bárbara Fernandes",
"Barbara Pelais de Oliveira",
"Barbara Pozzan dos Santos Duarte",
"Beatriz Caroline de Alcantara Sabô",
"Beatriz Pedreira",
"Bernardo Botelho Fontes",
"Bianca Araujo",
"Bianca Moretto Ribeiro",
"Bruna Bazaluk M Videira",
"Bruna Roberta Gil",
"Bruno Arine",
"Bruno Caldas Vianna",
"Bruno Gama Silva Miranda",
"Bruno Wiezel",
"brunz",
"Caio Augusto Cunha Volpato",
"Caio Eduardo Zangirolami Saldanha",
"Caio Emanuel Rhoden",
"Caio Fernandes",
"Camila Cruz",
"Camila Faraco dos Santos",
"Camila Monteiro Chaves",
"Camille Moura",
"Carla Albertuni",
"Carla Oliveira Santos",
"Carlos Cabral",
"carlos liguori",
"Carlota Mingolla",
"Carolina Lemos de Oliveira",
"carolina pires ferreira",
"Caroline de Lima",
"Cecilia Marins",
"Cecilia Tanaka",
"Christiane Borges",
"clara ribeiro sacco",
"Cristiana Gonzalez",
"Daniel Lenharo de Souza",
"Daniel Morais Assuncao",
"Daniel Santini",
"Daniel Sousa Fest",
"Daniel Teixeira",
"Daniela de Souza Pritsch",
"Daniela Favero",
"Danilo Pereira",
"Dann Luciano",
"Dario Honsho",
"Debora Franco Machado",
"Deivite Cardoso",
"Dener Stassun Christinele",
"Diego Borin Reeberg",
"DINAH P R VENTURINI",
"DOUGLAS DA SILVA COSTA",
"Duilio Beojone Neto",
"Eduardo Dias de Andrade",
"Elisa X",
"Elson Almeida",
"Elvio Pedroso Martinelli",
"Emerson Marques Pedro",
"Emile Badran Filho",
"Emilio S. Cordeiro",
"Emily L. Z. A. Silva",
"Enzzo Pessanha Cavallo",
"Emily Fonseca"
],
[
"Escola de Ativismo",
"Euler Neto",
"Evelize Pacheco Simões",
"Éverton Antônio Ribeiro",
"Ewerton Queiroz",
"Fabiana de Oliveira Benedito",
"Fabio Beneditto",
"Fábio Botler",
"Fábio de Oliveira Serrão",
"Fábio Meneghetti",
"FABIO MONTARROIOS",
"Fabio Seitsugo",
"Fabrício Yamamoto",
"Felipe Barreto Bergamo",
"Felipe Becker Delwing",
"Felipe Cardoso Resende",
"Felipe Ferrari",
"Fernanda Campagnucci",
"Fernanda Cardoso",
"Fernanda Shirakawa",
"Fernando Costa Nogueira",
"Fernando Mercês",
"Fernao Vellozo",
"Flávia Lefèvre Guimarães",
"Flavio Lima",
"Flavio Siqueira Junior",
"Francine Emilia Costa",
"Francisco Antunes Caminati",
"Francisco dos S. Ekman Simões",
"Frederico Guilherme Shigueo Fujimori Arruda",
"Gabriel Gavasso",
"Gabriel Ratto Domiciano",
"Gabriel Soares de Almeida",
"Gabriel Torres Gomes Pato",
"GABRIELA AKEMI SHIMABUKO",
"Gabriela Bittencourt",
"Gabriela Catunda Peres",
"Gabriela Gannam",
"Gabriela Garcia Juns",
"Gabriela Nardy de Vasconcellos Leitão",
"Gabriella De Biaggi",
"Gilberto Vieira",
"Giovana Vieira de Morais",
"Gisele Kauer",
"Gláucia Isidoro",
"Gleyson Sales",
"Gonzalo Cuéllar Mansilla",
"Guilherme Antoneli Nakashima",
"Guilherme Botelho Diniz Junqueira",
"Guilherme Flynn",
"Guilherme Foresti",
"Guilherme Gondim",
"Guilherme Quintella",
"Guilherme Scholz Ramos",
"Gustavo F. V. de Alencar",
"Gustavo Gus",
"Gustavo Pereira Dutra",
"Gustavo Suto",
"Helen Fornazier",
"Henrique Bronzoni",
"henrique dos reis miguel",
"Igor Felipe Sodré Ribeiro Carneiro",
"Ines Aisengart Menezes",
"Instituto Update ",
"Intervozes Coletivo Brasil de Comunicação Social",
"Iracema Serrat Vergotti",
"Ivo Nascimento",
"Jaime Daniel Rojas",
"Jakeliny Gracielly",
"Janaina Menegaz Spode",
"Jaqueline Venturim",
"Jean Carlos Sales Pantoja",
"Jefferson Souza",
"Jéssica Nathany Carvalho Freitas",
"joao moreno rodrigues falcao",
"João Paulo Guidi Xavier",
"João Paulo Vicente",
"Joao Victor de Araujo Costa",
"João Vitor Bizotto Ferreira",
"Jonas Alves",
"Jonatas Rossi",
"Jorge Henrique Oliveira da Silva",
"José Ahirton Batista Lopes Filho",
"Jose Coura",
"Julia Nader Dietrich",
"Juliana Amoasei dos Reis",
"Juliana Aziz Miriani Russar",
"Juliana de Melo Barbosa",
"Juliana Felicidade Armede",
"Juliana Machado",
"Juliana Silva",
"Juliana Soares Rosa",
"Júlio Boaro",
"Kerick Sousa",
"Laís Figueiredo",
"Larissa Dionisio ",
"Larissa Gomes de Stefano Escaliante",
"Larissa Maruyama",
"Laura Wadden",
"Leandro Corbelo"
],
[
"Leo Barbosa Reis",
"Léo Cruz",
"Leonardo Feltrin Foletto",
"Leonardo Pascholati do Amaral",
"Leonardo Piccioni de Almeida",
"leticia de sousa magalhaes",
"Leticia Rodrigues",
"Ligia Luz Lopes",
"Lourenço Piuma Soares",
"Lucas Albertine De Godoi",
"Lucas de Almeida Taraia",
"Lucas Ferreira de Lara",
"Lucas Gonçalves Vilela Fernandez",
"Lucas Lago",
"Lucas Monteiro de Oliveira",
"Lucas Pereira Baumgartner",
"Lucas Sampaio Magalhães",
"Luciana Capuani Masini",
"Luciana Minami",
"LUCILENE MIZUE HIDAKA",
"Luis Arantes",
"Luis Fagundes",
"Luísa Côrtes Gregp",
"Luiz Henrique Aguiar De Lima Alves",
"luiza peixoto costa carvalho",
"Magali Andreia Rossi",
"Maranha",
"Marcel C J Pereira",
"Marcela Barella",
"Marcelo Kei Sato",
"Marcelo Palladino",
"Marcelo Tavares de Santana",
"Marcia Ohlson",
"Márcio Moretto Ribeiro",
"Marcio Yonamine",
"Marcus Danilo Leite Rodrigues",
"Maria Belotti",
"Maria Rita Casagrande",
"maria teresa de arruda campos",
"Maria Teresa Ghiringhello",
"Mariana Afeche Cipolla",
"Mariana Kimie Nito",
"Marilia Monteiro",
"Marina",
"Marina Godoy Frota",
"Mario Balan",
"Marisa Sanematsu",
"Marjorie Nunes Ribeiro",
"Mateus Rodrigues Costa",
"Mateus Zitelli",
"Matheus Vanzella",
"Max Santos",
"Mellina Yonashiro",
"Miguel Faggioni Fernandes",
"Miguel Peixoto Costa Carvalho",
"Miguel Said Vieira",
"Milton Braga",
"Muriaki Nissiuti",
"Nádia Regina Oliveira",
"Narrira Lemos de Souza",
"Natália de Souza Lima Murgel ",
"Natasha Nishida",
"Nathalia Vieira Ferreira",
"Nayana Munhoz Fernandez",
"Nelson Assumpção",
"Nícolas Schirmer",
"Nina Weingrill",
"Olavo Luppi Silva",
"Olívia Bandeira",
"Omayr B. C. Zanata",
"Paola Maués",
"Patricia Cornils",
"Patricia Saldanha",
"Paula Ghiringhello Sato",
"Paulo Eduardo Crivelli Botaro",
"Paulo Eduardo de Almeida Santos",
"Pedro Antonio Ribeiro Teles",
"Pedro Henrique Braz",
"Pedro Lemos",
"Pedro Rodrigues Lobo",
"Pedro Telles",
"Pedro Vilaça",
"Perla Sachs Kindi",
"Phi R",
"Rafael Bantu",
"Rafael Damasceno Ramalho Pereira",
"Rafael Gonçalves",
"Rafael Horvath",
"Rafael Mantovani Espíndola Pessôa",
"Rafael Matrone Munduruca",
"Rafael Quirino de Castro",
"Rafael Wild",
"Rafaela Rossi",
"Rangel Soares de Souza",
"Raphael Leite",
"Rebeca Spencer Hartmann",
"Refinaria de Dados ",
"Reinaldo Bispo",
"Renata Barros Souto Maior Baião",
"Renato Norberto dos Santos",
"Roberto Rodrigues",
"Rodrigo Tribiolli Moreira",
"Rômulo Silviano"
],
[
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo de Souza",
"Rodrigo Alexandre Costa",
"Rodrigo Ceccato Freitas",
"Rodrigo Collacio",
"Rodrigo Orem",
"Rodrigo Troian",
"RODRIGO VIANA QUINTAS MAGARAO",
"Rogério Munhoz",
"Ronaldo Toshio",
"Rondineli Saad",
"Rony Marques",
"Samuel Reghim Silva",
"Sarah Barreto Marques",
"Sergio Amadeu da Silveira",
"Silas Batista",
"SITAWI",
"Susana Mieko",
"Tabbys Macedo",
"Tainah Marques Fernandes Moreira de Souza",
"Tanous Camara Azzi Melo",
"Tatiana Helena Criscione",
"Teodoro Bertolozzi Mendes",
"Thais de Almeida Olmos",
"Thais Teodoro Perez",
"Thales Douglas Paiva",
"Thiago",
"Thiago Benicchio",
"Thiago Cipriano Queiroz",
"Thiago Cunha da Silva",
"Thiago Firbida",
"Thiago Maia",
"Tiago Fassoni Alves dos Alencar Leite",
"TIAGO JAIME MACHADO",
"Tulio Malaspina",
"Valdir Torres Borges",
"Vanessa ",
"Vanessa Cardoso",
"Vanessa Me Tonini",
"vanessa pereira dos anjos",
"Veridiana Alimonti",
"Victor Bruno Rodrigues Negri",
"Victor Perin",
"Vinicius Cezar",
"Vinícius Felisberto",
"Vinicius guilherme da silva",
"Vinicius Rezende Costa",
"Vinícius Shoiti Koike Graciliano",
"Vinicius Tadeu Batista Pereira",
"Vitor",
"Vitor Cheregati",
"Vitor de Andrade Pinto",
"Vitor Massao Sugai",
"Wagner Antonucci Vale",
"Wallace Felipe Rodrigues Francisco",
"William Fernandes",
"William Oliveira",
"Willy Hsu",
"Yorik van Havre",
"Zeilane Victória"
]
     ] 

contato:
  id: "contato"
  titulo: "Contato"
  subtitulo: "Envie sua sugestão ou dúvida."
  descricao: "Imagem noturna de longa exposição de uma cidade. Um céu sem estrelas ocupa o quinto superior da imagem, e prédios com janelas iluminadas ocupam o espaço restante. Na parte inferior esquerda, parte de uma rua é visível, com os trajetos de faróis de carros visíveis devido à exposição longa."
  button_text: "Enviar"
  formspree_language_code: "pt-BR"
  validacao:
    nome:
      placeholder: "Nome (opcional)"
    email:
      placeholder: "Email *"
      aviso: "Digite seu endereço de e-mail."
    mensagem:
      placeholder: "Sua mensagem *"
      aviso: "Digite a sua mensagem."

footer:
  copyright: "CryptoRave.ORG 2019 - Sem privacidade não há liberdade - [Onion service](https://onion.cryptorave.org/)"
  antiassedio:
    texto: "Código de Conduta"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    - icon: "fa-ticket"
      link: "https://tickets.cryptorave.org/cr2019/free"
---
