---
language_code: "es"
title: "CryptoRave 2019"

header:
  descricao:
    ""
sobre:
  id: "sobre"
  descricao:
    [
      "En los días **3 y 4 de Mayo de 2019** se realizará la **sexta edición de la CryptoRave**, un evento anual de 36 horas que acoge actividades diversas sobre seguridad, criptografía, hacking, anonimato, privacidad y libertad en la red.",
      "CryptoRave es **abierta y gratuita** - sólo hace falta [inscripción online](#inscricao) y será realizada en la [Ciudad de São Paulo](#local). Inspirada por una [acción global](https://cryptoparty.in), descentralizada para diseminar y democratizar el conocimiento y conceptos básicos de criptografía y software libre, el evento nació en 2014 como reacción a la difusión de informaciones que confirmaron la acción de gobiernos para mantener a la población mundial bajo permanente vigilancia y escucha.",
     "La participación en las actividades es abierta mediante la [inscripción en línea gratuita](#inscricao). Mire [aquí](#programacao) la programacíon de CR2019!"
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "Logo de la CR2019, en color blanca con fondo negro: a la izquierda, las letras 'CR' compuestas por los números 0 y 1; al medio, una imagen de un procesador con una llave pixelada en su centro; a la derecha, un número '2019' escrito en dos lineas (con dos guarismos por linea) en fuente mas pequeña, de manera que la soma de sus alturas sea igual a la altura del texto 'CR'."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamiento"
  inscricao: "Inscripciones"
  keynotes: "Keynotes"
  programacao: "Programación"
  festa: "Fiesta"
  realizacao: "Realización"
  apoio: "Soporte"
  patrocinadores: "Auspiciantes"
  contato: "Contacto"
  local: "Ubicación"
  menu_extra:
#    - name: "CPA"
#      url: "https://cpa.cryptorave.org/pt-br/cr2019/cfp/session/new"
#      weight: 9"
#    - name: "Onion"
#      url:  "https://onion.cryptorave.org"
#      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamiento Colectivo"
  descricao: "La CryptoRave sólo es posible gracias al apoyo - financiero y de difusión -  de una comunidad distribuida y anónima pero que garantiza que cada año más personas participen en el evento. Nosotros, de la organización, aportamos nuestro granito de arena creando recompensas fantásticas para los donadores pero no nos quedamos con nada: todo se invierte en el evento. Muchas gracias a [todos los que contribuyeran](#patrocinadores), el evento no podría acontecer sin su colaboración."
  link_catarse: "https://www.catarse.me/pt/projects/70453/embed"
  titulo_btc: "Colabore con Bitcoin"
  descricao_btc: "Tenemos una billetera de bitcoin, a que puedes donar con la dirección abajo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para la dirección Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
  recompensas:
    - linha:
      - valor: "R$15"
        img: ""
        descricao_img: ''
        descricao: "**FREE HUGS** - AGRADECIMIENTO escrito en las nubes (sitio)"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"

      - valor: "R$25"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOSTICKER** - 2 ETIQUETAS ADHESIVAS DE LA CRYPTORAVE 2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"

      - valor: "R$40"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCAM** - 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"

    - linha:
      - valor: "R$50"
        img: ""
        descricao_img: ""
        descricao: "**SENIORSTICKER** - 4 ETIQUETAS ADHESIVAS ESPECIALES + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"

      - valor: "R$60"
        img: ""
        descricao_img: ""
        descricao: "**ESFEROMÁGYCA** - NOVIDAD!!! Escriba con tinta invisible. Revele con luz mágica. 1 ESFERO MÁGYCA + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"

      - valor: "R$75"
        img: ""
        descricao_img: ""
        descricao: "**MASTERSTICKER** - 4 ETIQUETAS ADHESIVAS ESPECIALES + 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"

    - linha:
      - valor: "R$120"
        img: ""
        descricao_img: ""
        descricao: "**CRIPTOKNEK** - 1 TAZA DE LA CRYPTORAVE 2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"

      - valor: "R$135"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTORAVER** - 1 CAMISETA ESPECIAL DE LOS APOYADORES + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimento en las nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"

      - valor: "R$375"
        img: ""
        descricao_img: ''
        descricao: "**MASTER CRYPTORAVER** - 1 CHOMPA CRYPTORAVE 2018 + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"

    - linha:
      - valor: "R$475"
        img: ""
        descricao_img: ''
        descricao: "**HEROIC CRYPTORAVER** - 1 CHOMPA CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE LOS APOYADORES + 2 ETIQUETA ADHESIVA CR2018 + agradecimientos en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"

      - valor: "R$700"
        img: ""
        descricao_img: ''
        descricao: "**CRYPTOJEDI** - 1 CHOMPA CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE LOS APOYADORES + 1 TAZA CRYPTORAVE 2018 + 4 ETIQUETAS ADHESIVAS ESPECIALES + 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimientos en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"

      - valor: "R$1000"
        img: ""
        descricao_img: ''
        descricao: "**CRIPTOJORNADA** - TALLER DE SEGURIDAD DE LA INFORMACIÓN PARA SU COLECTIVO O ORGANIZACIÓN + agradecimiento en las nubes"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Inscripciones"
  descricao_ticket: "La inscripción y participación en la CryptoRave son totalmente gratuitas. La presentación de la entrada será imprescindible para poder acceder al evento."
  link_ticket: "[Haz click aquí para inscribirte en la CryptoRave y generar gratis tu entrada.](https://tickets.cryptorave.org/cr2019/free)"
  link_boletim: "Para seguir las actividades y saber más sobre la programación, [recibe las novedades más recientes sobre la CryptoRave](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brasil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Coordenadora dos times do Projeto Tor, recentemente foi anunciada como a próxima Diretora Executiva do projeto."
  descricao: "O problema é muito maior que o Facebook. É o modelo econômico da Internet: o modelo de vigilância da Internet. Comunicar, compartilhar e acessar informações não deveria tornar você um alvo ou um produto. Você não deveria ser explorado/a ao usar a Internet."

programacao:
  id: "programacao"
  titulo: "Programación"
  descricao:
    [
      "La versión 0.1 de la programación de CryptoRave 2019 ya está disponible en [agenda.cryptorave.org](https://agenda.cryptorave.org/). La programación está sujeta a cambios y hay todavia actividades no confirmadas. El evento empieza a las 19 horas del 3 de mayo."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'Logo de la CR2017.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'Logo de la CR2016.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'Logo de la CR2015.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'Logo de la CR2014.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Ubicación"
  descricao:
    [
      "La CryptoRave 2019 será realizada en [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "Dirección: Rua da Consolação, 94 - República, São Paulo - SP. Cerca de la estación del metro Anhangabaú."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64346992969513%2C-23.548138354058587%2C-46.64142608642579%2C-23.546663037375726&amp;layer=mapnik&amp;marker=-23.547400697786546%2C-46.642448008060455"
    link: "https://osm.org/go/M~ziJkQVL?way=48661210"
    tooltip: "Click para abrir en otra pestaña"


festa:
  id: "festa"
  titulo: "Fiesta en Hangar 110"
  descricao:
    [
      "La fiesta de CryptoRave será el sábado, el 05/05, a las 20h en [Hangar 110](http://www.hangar110.com.br/). La entrada es gratis y exclusiva para participantes de CryptoRave!",
      "Dirección: Rua Rodolfo Miranda, 110 - Bom Retiro (5 minutos a pie del metro Armênia - linea azul)"
    ]
  img: "hangar110.jpg"
  link: "http://www.hangar110.com.br/"
  atracoes:
    [
      "20:00 - abertura de la casa!",
      "**LINEUP**",
      "21:00 - 21:45 - cabeça",
      "22:00 - 23:00 - afrorep",
      "23:00 - 00:00 - datamosh",
      "00:00 - 01:00 - octarina",
      "01:00 - 01:45 - show do retrigger",
      "01:45 - 03:00 - arkanoid",
      "03:00 - 04:15 - swaaag",
      "04:45 - 06:00 - lines",
      "**PROJEÇÕES**",
      "21:45 - 23:00 - Terms and conditions may apply",
      "23:30 - 00:45 - Bruno Treviso",
      "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (trecho)",
      "01:45 - 03:00 - NVVE MVE",
      "03:00 - 04:15 - chr0ma",
      "04:15 - 06:00 - Downloaded"
    ]

realizacao:
  id: "realizacao"
  titulo: "Realización"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo de la Escola de Ativismo: el texto 'ESCOLA DE ATIVISMO' (escuela de activismo) en blanco en el centro de uno rectángulo negro. El largo del rectángulo es un pouco mas grande que el largo del texto y su altura casi cuatro veces mas que la altura del texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo del Actantes: con fondo blanco, las letras 'ACT' en rojo, y en secuencia las letras 'ANTES' hechas con los números 0 e 1 en negro. No hay espacio entre las dos partes; el logo es una palabra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo del Saravá: al centro de un fondo rectangular negro, la palabra 'SARAVÁ' en blanco en una fuente que se asemeja al texto manuscrito. La altura de las letras varia entre cerca de un tercio y dos tercios de la altura del rectángulo. Horizontalmente, el texto ocupa cerca de cuatro quintos del ancho del rectángulo."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Logo del Intervozes, naranja en fondo blanco: un cuadrado con vértices redondeadas y con arcos de círculos blancos con diversas larguras le atravesando; en su derecha, en dos lineas con altura total poco mas pequeña que la altura del cuadrado, los textos 'intervozes' (en la primeira linea) y 'coletivo brasil de comunicação social' (en la segunda linea)."
        url: "http://intervozes.org.br"
    - linha:
      - img: "encriptatudo.png"
        descricao_img: "Logo del Encripta, en blanco: centralizados en un fondo rectangular negro, en la izquierda una imagen pixelada de una llave con una extremidad rodeada por corchetes, seguida del texto 'encripta'. La imagen ocupa cerca de la mitad de la altura del rectángulo, y el texto, cerca de un tercio. El largo del rectángulo en el fondo es ligeramente mas grande que la imagen seguida del texto."
        url: "https://encripta.org"

apoio:
  id: "apoio"
  titulo: "Soporte"
  parceiros:
    - linha:
      - img: "ccc.jpg"
        descricao_img: "Logo de Chaos Computer Club (CCC): en negro sobre un fondo blanco, los contornos de una imagen que se parece a una llave y a un circuito electrónico. Desde el interior de un rectángulo con las esquinas chaflanadas, 4 lineas direccionanse al centro del lado derecho; desde allá, alineanse perpendicularmente a este lado y salen del rectángulo, paralelas entre si. Después de una distancia de cerca de la mitad del comprimento del rectángulo, las lineas se retorcen, formando un nudo. Como hilos, las lineas colgan abajo del nudo."
        url: "https://ccc.de/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Auspiciantes"
  descricao: "Nuestro más sincero agradecimiento a todos y todas las que contribuyeran con nuestra [campaña de financiamento colectivo](#financiamento):"
  doadores:
    [
      [
"Adriano pqL",
"Alan Fachini",
"Alana Domit Bittar",
"Alessandra Iturrieta",
"Alexandre Augusto Prudente de Oliveira Nascimento",
"Alexandre de Lima",
"Alexandre Martins Araujo",
"Alexandre Okita",
"Alisson Moro Neocatto",
"Amanda Julia Vieira",
"Amanda Rahra ",
"Ana Clara Nobre",
"Ana Cláudia Alencar ",
"Ana Laura Chioca Vieira",
"Ana Luiza Portello Bastos",
"Ana Maria Ghiringhello",
"Ana Rüsche",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson da Silva Croccia",
"Anderson Pereira Leal",
"Anderson Queiroz de Oliveira",
"André Fernandes",
"Andre Giolito",
"Andre Henrique Cunha de Moraes",
"André Pasti",
"André Scherma Soléo",
"Andrew Fishman",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Antonio Marino Brigagão Rodrigues",
"Ari Mensdes",
"Arikkk",
"Armando Caputi",
"Arthur Fucher",
"Augusto Bennemann",
"Avelino Morganti Neto",
"Barbara Borba",
"Bárbara Fernandes",
"Barbara Pelais de Oliveira",
"Barbara Pozzan dos Santos Duarte",
"Beatriz Caroline de Alcantara Sabô",
"Beatriz Pedreira",
"Bernardo Botelho Fontes",
"Bianca Araujo",
"Bianca Moretto Ribeiro",
"Bruna Bazaluk M Videira",
"Bruna Roberta Gil",
"Bruno Arine",
"Bruno Caldas Vianna",
"Bruno Gama Silva Miranda",
"Bruno Wiezel",
"brunz",
"Caio Augusto Cunha Volpato",
"Caio Eduardo Zangirolami Saldanha",
"Caio Emanuel Rhoden",
"Caio Fernandes",
"Camila Cruz",
"Camila Faraco dos Santos",
"Camila Monteiro Chaves",
"Camille Moura",
"Carla Albertuni",
"Carla Oliveira Santos",
"Carlos Cabral",
"carlos liguori",
"Carlota Mingolla",
"Carolina Lemos de Oliveira",
"carolina pires ferreira",
"Caroline de Lima",
"Cecilia Marins",
"Cecilia Tanaka",
"Christiane Borges",
"clara ribeiro sacco",
"Cristiana Gonzalez",
"Daniel Lenharo de Souza",
"Daniel Morais Assuncao",
"Daniel Santini",
"Daniel Sousa Fest",
"Daniel Teixeira",
"Daniela de Souza Pritsch",
"Daniela Favero",
"Danilo Pereira",
"Dann Luciano",
"Dario Honsho",
"Debora Franco Machado",
"Deivite Cardoso",
"Dener Stassun Christinele",
"Diego Borin Reeberg",
"DINAH P R VENTURINI",
"DOUGLAS DA SILVA COSTA",
"Duilio Beojone Neto",
"Eduardo Dias de Andrade",
"Elisa X",
"Elson Almeida",
"Elvio Pedroso Martinelli",
"Emerson Marques Pedro",
"Emile Badran Filho",
"Emilio S. Cordeiro",
"Emily L. Z. A. Silva",
"Enzzo Pessanha Cavallo",
"Emily Fonseca"
],
[
"Escola de Ativismo",
"Euler Neto",
"Evelize Pacheco Simões",
"Éverton Antônio Ribeiro",
"Ewerton Queiroz",
"Fabiana de Oliveira Benedito",
"Fabio Beneditto",
"Fábio Botler",
"Fábio de Oliveira Serrão",
"Fábio Meneghetti",
"FABIO MONTARROIOS",
"Fabio Seitsugo",
"Fabrício Yamamoto",
"Felipe Barreto Bergamo",
"Felipe Becker Delwing",
"Felipe Cardoso Resende",
"Felipe Ferrari",
"Fernanda Campagnucci",
"Fernanda Cardoso",
"Fernanda Shirakawa",
"Fernando Costa Nogueira",
"Fernando Mercês",
"Fernao Vellozo",
"Flávia Lefèvre Guimarães",
"Flavio Lima",
"Flavio Siqueira Junior",
"Francine Emilia Costa",
"Francisco Antunes Caminati",
"Francisco dos S. Ekman Simões",
"Frederico Guilherme Shigueo Fujimori Arruda",
"Gabriel Gavasso",
"Gabriel Ratto Domiciano",
"Gabriel Soares de Almeida",
"Gabriel Torres Gomes Pato",
"GABRIELA AKEMI SHIMABUKO",
"Gabriela Bittencourt",
"Gabriela Catunda Peres",
"Gabriela Gannam",
"Gabriela Garcia Juns",
"Gabriela Nardy de Vasconcellos Leitão",
"Gabriella De Biaggi",
"Gilberto Vieira",
"Giovana Vieira de Morais",
"Gisele Kauer",
"Gláucia Isidoro",
"Gleyson Sales",
"Gonzalo Cuéllar Mansilla",
"Guilherme Antoneli Nakashima",
"Guilherme Botelho Diniz Junqueira",
"Guilherme Flynn",
"Guilherme Foresti",
"Guilherme Gondim",
"Guilherme Quintella",
"Guilherme Scholz Ramos",
"Gustavo F. V. de Alencar",
"Gustavo Gus",
"Gustavo Pereira Dutra",
"Gustavo Suto",
"Helen Fornazier",
"Henrique Bronzoni",
"henrique dos reis miguel",
"Igor Felipe Sodré Ribeiro Carneiro",
"Ines Aisengart Menezes",
"Instituto Update ",
"Intervozes Coletivo Brasil de Comunicação Social",
"Iracema Serrat Vergotti",
"Ivo Nascimento",
"Jaime Daniel Rojas",
"Jakeliny Gracielly",
"Janaina Menegaz Spode",
"Jaqueline Venturim",
"Jean Carlos Sales Pantoja",
"Jefferson Souza",
"Jéssica Nathany Carvalho Freitas",
"joao moreno rodrigues falcao",
"João Paulo Guidi Xavier",
"João Paulo Vicente",
"Joao Victor de Araujo Costa",
"João Vitor Bizotto Ferreira",
"Jonas Alves",
"Jonatas Rossi",
"Jorge Henrique Oliveira da Silva",
"José Ahirton Batista Lopes Filho",
"Jose Coura",
"Julia Nader Dietrich",
"Juliana Amoasei dos Reis",
"Juliana Aziz Miriani Russar",
"Juliana de Melo Barbosa",
"Juliana Felicidade Armede",
"Juliana Machado",
"Juliana Silva",
"Juliana Soares Rosa",
"Júlio Boaro",
"Kerick Sousa",
"Laís Figueiredo",
"Larissa Dionisio ",
"Larissa Gomes de Stefano Escaliante",
"Larissa Maruyama",
"Laura Wadden",
"Leandro Corbelo"
],
[
"Leo Barbosa Reis",
"Léo Cruz",
"Leonardo Feltrin Foletto",
"Leonardo Pascholati do Amaral",
"Leonardo Piccioni de Almeida",
"leticia de sousa magalhaes",
"Leticia Rodrigues",
"Ligia Luz Lopes",
"Lourenço Piuma Soares",
"Lucas Albertine De Godoi",
"Lucas de Almeida Taraia",
"Lucas Ferreira de Lara",
"Lucas Gonçalves Vilela Fernandez",
"Lucas Lago",
"Lucas Monteiro de Oliveira",
"Lucas Pereira Baumgartner",
"Lucas Sampaio Magalhães",
"Luciana Capuani Masini",
"Luciana Minami",
"LUCILENE MIZUE HIDAKA",
"Luis Arantes",
"Luis Fagundes",
"Luísa Côrtes Gregp",
"Luiz Henrique Aguiar De Lima Alves",
"luiza peixoto costa carvalho",
"Magali Andreia Rossi",
"Maranha",
"Marcel C J Pereira",
"Marcela Barella",
"Marcelo Kei Sato",
"Marcelo Palladino",
"Marcelo Tavares de Santana",
"Marcia Ohlson",
"Márcio Moretto Ribeiro",
"Marcio Yonamine",
"Marcus Danilo Leite Rodrigues",
"Maria Belotti",
"Maria Rita Casagrande",
"maria teresa de arruda campos",
"Maria Teresa Ghiringhello",
"Mariana Afeche Cipolla",
"Mariana Kimie Nito",
"Marilia Monteiro",
"Marina",
"Marina Godoy Frota",
"Mario Balan",
"Marisa Sanematsu",
"Marjorie Nunes Ribeiro",
"Mateus Rodrigues Costa",
"Mateus Zitelli",
"Matheus Vanzella",
"Max Santos",
"Mellina Yonashiro",
"Miguel Faggioni Fernandes",
"Miguel Peixoto Costa Carvalho",
"Miguel Said Vieira",
"Milton Braga",
"Muriaki Nissiuti",
"Nádia Regina Oliveira",
"Narrira Lemos de Souza",
"Natália de Souza Lima Murgel ",
"Natasha Nishida",
"Nathalia Vieira Ferreira",
"Nayana Munhoz Fernandez",
"Nelson Assumpção",
"Nícolas Schirmer",
"Nina Weingrill",
"Olavo Luppi Silva",
"Olívia Bandeira",
"Omayr B. C. Zanata",
"Paola Maués",
"Patricia Cornils",
"Patricia Saldanha",
"Paula Ghiringhello Sato",
"Paulo Eduardo Crivelli Botaro",
"Paulo Eduardo de Almeida Santos",
"Pedro Antonio Ribeiro Teles",
"Pedro Henrique Braz",
"Pedro Lemos",
"Pedro Rodrigues Lobo",
"Pedro Telles",
"Pedro Vilaça",
"Perla Sachs Kindi",
"Phi R",
"Rafael Bantu",
"Rafael Damasceno Ramalho Pereira",
"Rafael Gonçalves",
"Rafael Horvath",
"Rafael Mantovani Espíndola Pessôa",
"Rafael Matrone Munduruca",
"Rafael Quirino de Castro",
"Rafael Wild",
"Rafaela Rossi",
"Rangel Soares de Souza",
"Raphael Leite",
"Rebeca Spencer Hartmann",
"Refinaria de Dados ",
"Reinaldo Bispo",
"Renata Barros Souto Maior Baião",
"Renato Norberto dos Santos",
"Roberto Rodrigues",
"Rômulo Silviano"
],
[
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo de Souza",
"Rodrigo Alexandre Costa",
"Rodrigo Ceccato Freitas",
"Rodrigo Collacio",
"Rodrigo Orem",
"Rodrigo Troian",
"RODRIGO VIANA QUINTAS MAGARAO",
"Rogério Munhoz",
"Ronaldo Toshio",
"Rondineli Saad",
"Rony Marques",
"Samuel Reghim Silva",
"Sarah Barreto Marques",
"Sergio Amadeu da Silveira",
"Silas Batista",
"SITAWI",
"Susana Mieko",
"Tabbys Macedo",
"Tainah Marques Fernandes Moreira de Souza",
"Tanous Camara Azzi Melo",
"Tatiana Helena Criscione",
"Teodoro Bertolozzi Mendes",
"Thais de Almeida Olmos",
"Thais Teodoro Perez",
"Thales Douglas Paiva",
"Thiago",
"Thiago Benicchio",
"Thiago Cipriano Queiroz",
"Thiago Cunha da Silva",
"Thiago Firbida",
"Thiago Maia",
"Tiago Fassoni Alves dos Alencar Leite",
"TIAGO JAIME MACHADO",
"Tulio Malaspina",
"Valdir Torres Borges",
"Vanessa ",
"Vanessa Cardoso",
"Vanessa Me Tonini",
"vanessa pereira dos anjos",
"Veridiana Alimonti",
"Victor Bruno Rodrigues Negri",
"Victor Perin",
"Vinicius Cezar",
"Vinícius Felisberto",
"Vinicius guilherme da silva",
"Vinicius Rezende Costa",
"Vinícius Shoiti Koike Graciliano",
"Vinicius Tadeu Batista Pereira",
"Vitor",
"Vitor Cheregati",
"Vitor de Andrade Pinto",
"Vitor Massao Sugai",
"Wagner Antonucci Vale",
"Wallace Felipe Rodrigues Francisco",
"William Fernandes",
"William Oliveira",
"Willy Hsu",
"Yorik van Havre",
"Zeilane Victória"
]
    ]

contato:
  id: "contato"
  titulo: "Contacto"
  subtitulo: "Envíe su sugerencia o inquietud."
  descricao: "Imagen de una ciudad por la noche, foto tomada con longa exposición. En la parte superior, un cielo sin estrellas rellena un quinto de la imagen, edificios con ventanas alumbradas llenan el espacio restante. En la parte inferior izquierda esta visible parte de una calle, con camino hechos con los faros de los coches debido a la longa exposición."
  button_text: "Enviar"
  enviando: "Aguardando la confirmación del envio..."
  obrigado: "Gracias por ponerse en contacto con nosotros!"
  erro: "Ups, la mensaje no logró ser enviada. Por favor póngase en contacto por el correo: contato@cryptorave.org."
  validacao:
    nome:
      placeholder: "Nombre (opcional)"
    email:
      placeholder: "Correo *"
      aviso: "Digite la dirección de su correo electrónico."
    mensagem:
      placeholder: "Su mensaje *"
      aviso: "Digite su mensaje."


footer:
  copyright: "CryptoRave.ORG 2019 - Sem privacidade não há liberdade - [Onion service](https://onion.cryptorave.org/)"
  antiassedio:
    texto: "Código de Conducta"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    - icon: "fa-ticket"
      link: "https://tickets.cryptorave.org/cr2019/free"

---
