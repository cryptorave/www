---
language_code: "en"
title: "CryptoRave 2019"

header:
  descricao:
    ""
sobre:
  id: "sobre"
  descricao:
    [
      "The sixth edition of CryptoRave will be held between **May 3 and 4, 2019**. The annual event brings together in 36 hours various activities on security, encryption, hacking, anonymity, privacy and network freedom.",
      "CryptoRave is **open and free** - with just an [online registration](#inscricao) - and will be held in [São Paulo, Brazil](#local). It's inspired by the decentralized [global action](https://cryptoparty.in) to disseminate and democratize knowledge and basic concepts of cryptography and free software. The event began in 2014 as a reaction to the disclosure of information that confirmed the action governments to keep the world population under constant surveillance and monitoring."
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "CR2019 logo in white on a black background: to the left, the letters 'CR' formed by tiny zeroes and ones; at the center, an image of a processor with a pixelated key at its center; to the right, the number '2019' written in two lines with a smaller font, so that the total height of both lines is equal to that of the letters 'CR'."
  logo: "crlogo.png"
  sobre: "About"
  financiamento: "Contribute"
  inscricao: "Attend"
  keynotes: "Keynotes"
  programacao: "Schedule"
  festa: "Party"
  realizacao: "Partners"
  apoio: "Support"
  patrocinadores: "Backers"
  contato: "Contact"
  local: "Venue"
  menu_extra:
#    - name: "CFP"
#      url: "https://cpa.cryptorave.org/pt-br/cr2019/cfp/session/new"
#      weight: 9"
#    - name: "Onion"
#      url:  "https://onion.cryptorave.org"
#      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Crowdfunding"
  descricao: "CryptoRave 2019's crowdfunding campaign ended successfully! Thank you very much to [all who contributed](#patrocinadores), our event could not happen without your collaboration."
  link_catarse: "https://www.catarse.me/pt/projects/91191/embed"
  titulo_btc: "Contribute with Bitcoin"
  descricao_btc: "We also have a Bitcoin wallet, which you can donate to using the address below! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code for the Bitcoin address 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
  recompensas:
    - linha:
      - valor: "R$15"
        img: ""
        descricao_img: ''
        descricao: "**FREE HUGS** - 'THANK YOU' on the cloud (in our website)"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"

      - valor: "R$25"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOSTICKER** - 2 CRYPTORAVE 2019 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"

      - valor: "R$40"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCAM** - 4 STICKERS TO COVER YOUR WEBCAM + 1 CR2019 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"

    - linha:
      - valor: "R$50"
        img: ""
        descricao_img: ""
        descricao: "**SENIORSTICKER** - 4 SPECIAL STICKERS + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"

      - valor: "R$60"
        img: ""
        descricao_img: ""
        descricao: "**MAGYCPEN** - NEW!!! Write with invisible ink. Reveal with blacklight. 1 MAGYC PEN + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"

      - valor: "R$75"
        img: ""
        descricao_img: ""
        descricao: "**MASTERSTICKER** - 4 SPECIAL STICKERS + 4 WEBCAM-COVERING STICKERS + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"

    - linha:
      - valor: "R$120"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCUP** - 1 CRYPTORAVE 2018 REUSABLE PLASTIC CUP + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"

      - valor: "R$135"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTORAVER** - 1 SPECIAL SUPPORTER T-SHIRT + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"

      - valor: "R$375"
        img: ""
        descricao_img: ''
        descricao: "**MASTER CRYPTORAVER** - 1 CRYPTORAVE 2018 HOODIE + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"

    - linha:
      - valor: "R$475"
        img: ""
        descricao_img: ''
        descricao: "**HEROIC CRYPTORAVER** - 1 CRYPTORAVE 2018 HOODIE + 1 SPECIAL SUPPORTER T-SHIRT + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"

      - valor: "R$700"
        img: ""
        descricao_img: ''
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"
        descricao: "**CRYPTOJEDI** - 1 CRYPTORAVE 2018 HOODIE + 1 SPECIAL SUPPORTER T-SHIRT + 1 CRYPTORAVE 2018 REUSABLE PLASTIC CUP + 4 SPECIAL STICKERS + 4 WEBCAM-COVERING STICKERS + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"

      - valor: "R$1000"
        img: ""
        descricao_img: ''
        descricao: "**CRYPTOJOURNEY** - INFORMATION SECURITY WORKSHOP FOR YOUR COLLECTIVE OR ORGANIZATION + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Attend"
  descricao: "The registration and participation in the CryptoRave is completely free. You must present the registration ticket on the day of the event."
  link_ticket: "[Click here to register for CryptoRave](https://tickets.cryptorave.org/cr2019/free)"
  descricao_boletim: "Get the latest news about the event and the activities,"
  link_boletim: "[subscribe to CryptoRave newsletter](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resisting the dystopia of borderless surveillance"
  data: "May 4, Friday, at 8PM"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brazil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Team coordinator of the Tor Project, recently announced as the next Executive Director of the project."
  descricao: "The problem is much larger than Facebook. It is the economic model of the internet: the surveillance model if the internet. Comminucating, sharing and accessing information should not make you a target or a product. You should not be exploited when using the internet."

programacao:
  id: "programacao"
  titulo: "Schedule"
  descricao:
    [
      "CryptoRave schedule is available in [agenda.cryptorave.org](https://agenda.cryptorave.org/)! The schedule is tentative and subject to changes. The event starts at 7PM on May 3."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'CR2017 logo.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'CR2016 logo.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'CR2015 logo.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'CR2014 logo.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Venue"
  descricao:
    [
      "CryptoRave 2019 will be hosted at [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/)!",
      "Address: Rua da Consolação, 94 - República, São Paulo. Close to subway station Anhangabaú."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64346992969513%2C-23.548138354058587%2C-46.64142608642579%2C-23.546663037375726&amp;layer=mapnik&amp;marker=-23.547400697786546%2C-46.642448008060455"
    link: "https://osm.org/go/M~ziJkQVL?way=48661210"
    tooltip: "Click to open in a new tab"


festa:
  id: "festa"
  titulo: "Party"
  descricao:
    [
      "CryptoRave's after party begins on Saturday, May 4, at 11PM at [Trackers](http://www.trackers.cx/). The entrance is free for CryptoRave 2019 [supporters](#patrocinadores)!",
      "Address: Avenida São João,  (10 minutes walk from Biblioteca Mário de Andrade)"
    ]
  img: "hangar110.jpg"
  link: "http://www.trackers.cx/"
#  atracoes:
#    [
#      "20:00 - opening!",
#      "**LINEUP**",
#      "21:00 - 21:45 - cabeça",
#      "22:00 - 23:00 - afrorep",
#      "23:00 - 00:00 - datamosh",
#      "00:00 - 01:00 - octarina",
#      "01:00 - 01:45 - show do retrigger",
#      "01:45 - 03:00 - arkanoid",
#      "03:00 - 04:15 - swaaag",
#      "04:45 - 06:00 - lines",
#      "**PROJECTIONS**",
#      "21:45 - 23:00 - Terms and conditions may apply",
#       "23:30 - 00:45 - Bruno Treviso",
#       "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (section)",
#       "01:45 - 03:00 - NVVE MVE",
#      "03:00 - 04:15 - chr0ma",
#      "04:15 - 06:00 - Downloaded"
#    ]

realizacao:
  id: "realizacao"
  titulo: "Partners"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Escola de Ativismo's logo: the text 'ESCOLA DE ATIVISMO' in white at the center of a black background rectangle. The rectangle is a bit wider than the text and height approximately four times that of the text."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Actantes's logo: on a white background, the letters 'ACT' in red, followed by the letters 'ANTES' formed by tiny zeroes and ones in black. There is no spacing between the two parts; the logo is a contiguous word."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Saravá's logo: at the center of a black background rectangle, the word 'SARAVÁ' written in white in a font resembling handwriting. The height of the letters varies from about a third to two thirds the height of the rectangle. The text occupies around four fifths of the rectangle's width."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Intervozes's logo, orange over a white background: a square with rounded vertices, overlapped by arcs of circles of various widths; to the right, in two lines, with total height a bit smaller than the sqare's, the following text: 'intervozes' in the first line, and 'coletivo brasil de comunicação social' (Brazil social communication collective)."
        url: "http://intervozes.org.br"
    - linha:
      - img: "encriptatudo.png"
        descricao_img: "Encripta's logo, in white: centered in a black background rectangle, the pixelated image of a key with one extremity surrounded by brackets, to the left. At its right, the text 'encripta'. The image occupies about half of the rectangle's height, while the text occupies about a third. The rectangle's width is a little larger than that of the image followed by the text."
        url: "https://encripta.org"

apoio:
  id: "apoio"
  titulo: "Support"
  parceiros:
    - linha:
      - img: "ccc.jpg"
        descricao_img: "Chaos Computer Club (CCC)'s logo': in black over a white background, the outline of an image that resembles a key and also an electronic circuit. From the inside of a rectangle with chamfered corners, 4 lines direct point toward the midpoint of its right side; from there, they align perpendicularly to this side and leave the rectangle, parallel to one another. After a distance of about half the width of the rectangle, the lines twist, forming a knot. Resembling wires, the lines dangle, twisted, below the knot."
        url: "https://ccc.de/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Backers"
  descricao: "We sincerely thank everyone who contributed to our [crowdfunding campaign](#financiamento):"
  doadores:
    [
      ["Adriano pqL",
       "Alan Fachini",
       "Alana Domit Bittar",
       "Alessandra Iturrieta",
       "Alexandre Augusto Prudente de Oliveira Nascimento",
       "Alexandre de Lima",
       "Alexandre Martins Araujo",
       "Alexandre Okita",
       "Alisson Moro Neocatto",
	"Amanda Julia Vieira",
	"Amanda Rahra ",
	"Ana Clara Nobre",
	"Ana Cláudia Alencar ",
	"Ana Laura Chioca Vieira",
	"Ana Luiza Portello Bastos",
	"Ana Maria Ghiringhello",
"Ana Rüsche",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson da Silva Croccia",
"Anderson Pereira Leal",
"Anderson Queiroz de Oliveira",
"André Fernandes",
"Andre Giolito",
"Andre Henrique Cunha de Moraes",
"André Pasti",
"André Scherma Soléo",
"Andrew Fishman",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Antonio Marino Brigagão Rodrigues",
"Ari Mensdes",
"Arikkk",
"Armando Caputi",
"Arthur Fucher",
"Augusto Bennemann",
"Avelino Morganti Neto",
"Barbara Borba",
"Bárbara Fernandes",
"Barbara Pelais de Oliveira",
"Barbara Pozzan dos Santos Duarte",
"Beatriz Caroline de Alcantara Sabô",
"Beatriz Pedreira",
"Bernardo Botelho Fontes",
"Bianca Araujo",
"Bianca Moretto Ribeiro",
"Bruna Bazaluk M Videira",
"Bruna Roberta Gil",
"Bruno Arine",
"Bruno Caldas Vianna",
"Bruno Gama Silva Miranda",
"Bruno Wiezel",
"brunz",
"Caio Augusto Cunha Volpato",
"Caio Eduardo Zangirolami Saldanha",
"Caio Emanuel Rhoden",
"Caio Fernandes",
"Camila Cruz",
"Camila Faraco dos Santos",
"Camila Monteiro Chaves",
"Camille Moura",
"Carla Albertuni",
"Carla Oliveira Santos",
"Carlos Cabral",
"carlos liguori",
"Carlota Mingolla",
"Carolina Lemos de Oliveira",
"carolina pires ferreira",
"Caroline de Lima",
"Cecilia Marins",
"Cecilia Tanaka",
"Christiane Borges",
"clara ribeiro sacco",
"Cristiana Gonzalez",
"Daniel Lenharo de Souza",
"Daniel Morais Assuncao",
"Daniel Santini",
"Daniel Sousa Fest",
"Daniel Teixeira",
"Daniela de Souza Pritsch",
"Daniela Favero",
"Danilo Pereira",
"Dann Luciano",
"Dario Honsho",
"Debora Franco Machado",
"Deivite Cardoso",
"Dener Stassun Christinele",
"Diego Borin Reeberg",
"DINAH P R VENTURINI",
"DOUGLAS DA SILVA COSTA",
"Duilio Beojone Neto",
"Eduardo Dias de Andrade",
"Elisa X",
"Elson Almeida",
"Elvio Pedroso Martinelli",
"Emerson Marques Pedro",
"Emile Badran Filho",
"Emilio S. Cordeiro",
"Emily L. Z. A. Silva",
"Enzzo Pessanha Cavallo",
"Emily Fonseca"
],
[
"Escola de Ativismo",
"Euler Neto",
"Evelize Pacheco Simões",
"Éverton Antônio Ribeiro",
"Ewerton Queiroz",
"Fabiana de Oliveira Benedito",
"Fabio Beneditto",
"Fábio Botler",
"Fábio de Oliveira Serrão",
"Fábio Meneghetti",
"FABIO MONTARROIOS",
"Fabio Seitsugo",
"Fabrício Yamamoto",
"Felipe Barreto Bergamo",
"Felipe Becker Delwing",
"Felipe Cardoso Resende",
"Felipe Ferrari",
"Fernanda Campagnucci",
"Fernanda Cardoso",
"Fernanda Shirakawa",
"Fernando Costa Nogueira",
"Fernando Mercês",
"Fernao Vellozo",
"Flávia Lefèvre Guimarães",
"Flavio Lima",
"Flavio Siqueira Junior",
"Francine Emilia Costa",
"Francisco Antunes Caminati",
"Francisco dos S. Ekman Simões",
"Frederico Guilherme Shigueo Fujimori Arruda",
"Gabriel Gavasso",
"Gabriel Ratto Domiciano",
"Gabriel Soares de Almeida",
"Gabriel Torres Gomes Pato",
"GABRIELA AKEMI SHIMABUKO",
"Gabriela Bittencourt",
"Gabriela Catunda Peres",
"Gabriela Gannam",
"Gabriela Garcia Juns",
"Gabriela Nardy de Vasconcellos Leitão",
"Gabriella De Biaggi",
"Gilberto Vieira",
"Giovana Vieira de Morais",
"Gisele Kauer",
"Gláucia Isidoro",
"Gleyson Sales",
"Gonzalo Cuéllar Mansilla",
"Guilherme Antoneli Nakashima",
"Guilherme Botelho Diniz Junqueira",
"Guilherme Flynn",
"Guilherme Foresti",
"Guilherme Gondim",
"Guilherme Quintella",
"Guilherme Scholz Ramos",
"Gustavo F. V. de Alencar",
"Gustavo Gus",
"Gustavo Pereira Dutra",
"Gustavo Suto",
"Helen Fornazier",
"Henrique Bronzoni",
"henrique dos reis miguel",
"Igor Felipe Sodré Ribeiro Carneiro",
"Ines Aisengart Menezes",
"Instituto Update ",
"Intervozes Coletivo Brasil de Comunicação Social",
"Iracema Serrat Vergotti",
"Ivo Nascimento",
"Jaime Daniel Rojas",
"Jakeliny Gracielly",
"Janaina Menegaz Spode",
"Jaqueline Venturim",
"Jean Carlos Sales Pantoja",
"Jefferson Souza",
"Jéssica Nathany Carvalho Freitas",
"joao moreno rodrigues falcao",
"João Paulo Guidi Xavier",
"João Paulo Vicente",
"Joao Victor de Araujo Costa",
"João Vitor Bizotto Ferreira",
"Jonas Alves",
"Jonatas Rossi",
"Jorge Henrique Oliveira da Silva",
"José Ahirton Batista Lopes Filho",
"Jose Coura",
"Julia Nader Dietrich",
"Juliana Amoasei dos Reis",
"Juliana Aziz Miriani Russar",
"Juliana de Melo Barbosa",
"Juliana Felicidade Armede",
"Juliana Machado",
"Juliana Silva",
"Juliana Soares Rosa",
"Júlio Boaro",
"Kerick Sousa",
"Laís Figueiredo",
"Larissa Dionisio ",
"Larissa Gomes de Stefano Escaliante",
"Larissa Maruyama",
"Laura Wadden",
"Leandro Corbelo"
],
[
"Leo Barbosa Reis",
"Léo Cruz",
"Leonardo Feltrin Foletto",
"Leonardo Pascholati do Amaral",
"Leonardo Piccioni de Almeida",
"leticia de sousa magalhaes",
"Leticia Rodrigues",
"Ligia Luz Lopes",
"Lourenço Piuma Soares",
"Lucas Albertine De Godoi",
"Lucas de Almeida Taraia",
"Lucas Ferreira de Lara",
"Lucas Gonçalves Vilela Fernandez",
"Lucas Lago",
"Lucas Monteiro de Oliveira",
"Lucas Pereira Baumgartner",
"Lucas Sampaio Magalhães",
"Luciana Capuani Masini",
"Luciana Minami",
"LUCILENE MIZUE HIDAKA",
"Luis Arantes",
"Luis Fagundes",
"Luísa Côrtes Gregp",
"Luiz Henrique Aguiar De Lima Alves",
"luiza peixoto costa carvalho",
"Magali Andreia Rossi",
"Maranha",
"Marcel C J Pereira",
"Marcela Barella",
"Marcelo Kei Sato",
"Marcelo Palladino",
"Marcelo Tavares de Santana",
"Marcia Ohlson",
"Márcio Moretto Ribeiro",
"Marcio Yonamine",
"Marcus Danilo Leite Rodrigues",
"Maria Belotti",
"Maria Rita Casagrande",
"maria teresa de arruda campos",
"Maria Teresa Ghiringhello",
"Mariana Afeche Cipolla",
"Mariana Kimie Nito",
"Marilia Monteiro",
"Marina",
"Marina Godoy Frota",
"Mario Balan",
"Marisa Sanematsu",
"Marjorie Nunes Ribeiro",
"Mateus Rodrigues Costa",
"Mateus Zitelli",
"Matheus Vanzella",
"Max Santos",
"Mellina Yonashiro",
"Miguel Faggioni Fernandes",
"Miguel Peixoto Costa Carvalho",
"Miguel Said Vieira",
"Milton Braga",
"Muriaki Nissiuti",
"Nádia Regina Oliveira",
"Narrira Lemos de Souza",
"Natália de Souza Lima Murgel ",
"Natasha Nishida",
"Nathalia Vieira Ferreira",
"Nayana Munhoz Fernandez",
"Nelson Assumpção",
"Nícolas Schirmer",
"Nina Weingrill",
"Olavo Luppi Silva",
"Olívia Bandeira",
"Omayr B. C. Zanata",
"Paola Maués",
"Patricia Cornils",
"Patricia Saldanha",
"Paula Ghiringhello Sato",
"Paulo Eduardo Crivelli Botaro",
"Paulo Eduardo de Almeida Santos",
"Pedro Antonio Ribeiro Teles",
"Pedro Henrique Braz",
"Pedro Lemos",
"Pedro Rodrigues Lobo",
"Pedro Telles",
"Pedro Vilaça",
"Perla Sachs Kindi",
"Phi R",
"Rafael Bantu",
"Rafael Damasceno Ramalho Pereira",
"Rafael Gonçalves",
"Rafael Horvath",
"Rafael Mantovani Espíndola Pessôa",
"Rafael Matrone Munduruca",
"Rafael Quirino de Castro",
"Rafael Wild",
"Rafaela Rossi",
"Rangel Soares de Souza",
"Raphael Leite",
"Rebeca Spencer Hartmann",
"Refinaria de Dados ",
"Reinaldo Bispo",
"Renata Barros Souto Maior Baião",
"Renato Norberto dos Santos",
"Roberto Rodrigues",
"Rômulo Silviano"
],
[
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo de Souza",
"Rodrigo Alexandre Costa",
"Rodrigo Ceccato Freitas",
"Rodrigo Collacio",
"Rodrigo Orem",
"Rodrigo Troian",
"RODRIGO VIANA QUINTAS MAGARAO",
"Rogério Munhoz",
"Ronaldo Toshio",
"Rondineli Saad",
"Rony Marques",
"Samuel Reghim Silva",
"Sarah Barreto Marques",
"Sergio Amadeu da Silveira",
"Silas Batista",
"SITAWI",
"Susana Mieko",
"Tabbys Macedo",
"Tainah Marques Fernandes Moreira de Souza",
"Tanous Camara Azzi Melo",
"Tatiana Helena Criscione",
"Teodoro Bertolozzi Mendes",
"Thais de Almeida Olmos",
"Thais Teodoro Perez",
"Thales Douglas Paiva",
"Thiago",
"Thiago Benicchio",
"Thiago Cipriano Queiroz",
"Thiago Cunha da Silva",
"Thiago Firbida",
"Thiago Maia",
"Tiago Fassoni Alves dos Alencar Leite",
"TIAGO JAIME MACHADO",
"Tulio Malaspina",
"Valdir Torres Borges",
"Vanessa ",
"Vanessa Cardoso",
"Vanessa Me Tonini",
"vanessa pereira dos anjos",
"Veridiana Alimonti",
"Victor Bruno Rodrigues Negri",
"Victor Perin",
"Vinicius Cezar",
"Vinícius Felisberto",
"Vinicius guilherme da silva",
"Vinicius Rezende Costa",
"Vinícius Shoiti Koike Graciliano",
"Vinicius Tadeu Batista Pereira",
"Vitor",
"Vitor Cheregati",
"Vitor de Andrade Pinto",
"Vitor Massao Sugai",
"Wagner Antonucci Vale",
"Wallace Felipe Rodrigues Francisco",
"William Fernandes",
"William Oliveira",
"Willy Hsu",
"Yorik van Havre",
"Zeilane Victória"
]

          ]

contato:
  id: "contato"
  titulo: "Contact"
  subtitulo: "Send your questions and suggestions."
  descricao: "Long exposure image of a city at night. A starless sky occupies the upper fifth of the image, while buildings with lit windows occupy the remaining space. In the left lower half of the image, part of a street is visible, with the trajectories of headlights visible due to the long exposure."
  button_text: "Send"
  formspree_language_code: "en"
  validacao:
    nome:
      placeholder: "Name (optional)"
    email:
      placeholder: "Email *"
      aviso: "Enter your email address."
    mensagem:
      placeholder: "Your message *"
      aviso: "Type your message here."

footer:
  copyright: "CryptoRave.ORG 2019 - Sem privacidade não há liberdade - [Onion service](https://onion.cryptorave.org/)"
  antiassedio:
    texto: "Code of Conduct"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    - icon: "fa-ticket"
      link: "https://tickets.cryptorave.org/cr2019/free"

---
