---
language_code: "es"
title: "CryptoRave 2018"

header:
  descricao:
    "Imagen de noche de una ciudad, hecha con longa exposición, con una calle en el primer plan, arboles en el segundo plan, calles y edificios alumbrados al fondo. Los caminos de los faros de los coches son visibles por culpa de la longa exposición. Sobrepuesto a la imagen, el logo de la CR2018 en color blanca: existe una inclinación de cerca de unos 30 grados para bajo, las letras 'CR' formadas por los números 0 y 1, un procesador con una imagen pixelada de una llave en su centro, y el texto 'UM SPECTRE NOS RONDA' en la linea abajo. A la derecha y sien inclinación, el numero ‘2018’ en dos lineas (con dos guarismos por linea), con altura total aproximadamente igual al del logo inclinado."

sobre:
  id: "sobre"
  descricao:
    [
      "Entre los días 4 y 5 de mayo de 2018, en la ciudad de São Paulo, durante 36 horas, la CryptoRave (CR) trae actividades sobre seguridad, criptografía, hacking, anonimato, privacidad y libertad en la red. Esa es la **5ª edición del evento!**",
      "La CryptoRave inspirase en la acción global y descentralizada de la [CryptoParty](https://cryptoparty.in/), la cual tiene como objetivo difundir los conceptos fundamentales y softwares básicos de la criptografía. Criada en 2014, a cada edición más de 2500 personas atienderon!",
      "La participación en las actividades es abierta mediante la [inscripción en línea gratuita](#inscricao). Mire [aquí](#programacao) la programacíon de CR2018!"
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "Logo de la CR2018, en color blanca con fondo negro: a la izquierda, las letras 'CR' compuestas por los números 0 y 1; al medio, una imagen de un procesador con una llave pixelada en su centro; a la derecha, un número '2018' escrito en dos lineas (con dos guarismos por linea) en fuente mas pequeña, de manera que la soma de sus alturas sea igual a la altura del texto 'CR'."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamiento"
  inscricao: "Inscripciones"
  keynotes: "Keynotes"
  programacao: "Programación"
  festa: "Fiesta"
  realizacao: "Realización"
  apoio: "Soporte"
  patrocinadores: "Auspiciantes"
  contato: "Contacto"
  local: "Ubicación"
  menu_extra:
    - name: "CPA"
      url: "https://cpa.cryptorave.org/pt-br/cr2018/cfp/session/new"
      weight: 9"
    - name: "Onion"
      url:  "https://onion.cryptorave.org"
      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamiento Colectivo"
  descricao: "La campaña de financiamiento colectivo de CryptoRave 2018 fue encerrada con suceso! Muchas gracias a [todos los que contribuyeran](#patrocinadores), el evento no podría acontecer sin su colaboración."
  link_catarse: "https://www.catarse.me/pt/projects/70453/embed"
  titulo_btc: "Colabore con Bitcoin"
  descricao_btc: "Tenemos una billetera de bitcoin, a que puedes donar con la dirección abajo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para la dirección Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
  recompensas:
    - linha:
      - valor: "R$15"
        img: ""
        descricao_img: ''
        descricao: "**FREE HUGS** - AGRADECIMIENTO escrito en las nubes (sitio)"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"

      - valor: "R$25"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOSTICKER** - 2 ETIQUETAS ADHESIVAS DE LA CRYPTORAVE 2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"

      - valor: "R$40"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCAM** - 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"

    - linha:
      - valor: "R$50"
        img: ""
        descricao_img: ""
        descricao: "**SENIORSTICKER** - 4 ETIQUETAS ADHESIVAS ESPECIALES + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"

      - valor: "R$60"
        img: ""
        descricao_img: ""
        descricao: "**ESFEROMÁGYCA** - NOVIDAD!!! Escriba con tinta invisible. Revele con luz mágica. 1 ESFERO MÁGYCA + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"

      - valor: "R$75"
        img: ""
        descricao_img: ""
        descricao: "**MASTERSTICKER** - 4 ETIQUETAS ADHESIVAS ESPECIALES + 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"

    - linha:
      - valor: "R$120"
        img: ""
        descricao_img: ""
        descricao: "**CRIPTOKNEK** - 1 TAZA DE LA CRYPTORAVE 2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"

      - valor: "R$135"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTORAVER** - 1 CAMISETA ESPECIAL DE LOS APOYADORES + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimento en las nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"

      - valor: "R$375"
        img: ""
        descricao_img: ''
        descricao: "**MASTER CRYPTORAVER** - 1 CHOMPA CRYPTORAVE 2018 + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimiento en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"

    - linha:
      - valor: "R$475"
        img: ""
        descricao_img: ''
        descricao: "**HEROIC CRYPTORAVER** - 1 CHOMPA CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE LOS APOYADORES + 2 ETIQUETA ADHESIVA CR2018 + agradecimientos en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"

      - valor: "R$700"
        img: ""
        descricao_img: ''
        descricao: "**CRYPTOJEDI** - 1 CHOMPA CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE LOS APOYADORES + 1 TAZA CRYPTORAVE 2018 + 4 ETIQUETAS ADHESIVAS ESPECIALES + 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimientos en las nubes."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"

      - valor: "R$1000"
        img: ""
        descricao_img: ''
        descricao: "**CRIPTOJORNADA** - TALLER DE SEGURIDAD DE LA INFORMACIÓN PARA SU COLECTIVO O ORGANIZACIÓN + agradecimiento en las nubes"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Inscripciones"
  descricao: "Para atender el evento y conocer los preparativos de la CryptoRave, suscríbase en nuestro boletín de novedades."
  link: "[Haga clic para inscribirse en la CryptoRave](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brasil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Coordenadora dos times do Projeto Tor, recentemente foi anunciada como a próxima Diretora Executiva do projeto."
  descricao: "O problema é muito maior que o Facebook. É o modelo econômico da Internet: o modelo de vigilância da Internet. Comunicar, compartilhar e acessar informações não deveria tornar você um alvo ou um produto. Você não deveria ser explorado/a ao usar a Internet."

programacao:
  id: "programacao"
  titulo: "Programación"
  descricao:
    [
      "La versión 1.0 (casi final) de la programación de CryptoRave 2018 ya está [aquí](https://cpa.cryptorave.org/pt-br/cr2018/public/events)! La programación está sujeta a cambios y hay todavia actividades no confirmadas. El evento empieza a las 19 horas del 4 de mayo."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'Logo de la CR2017.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'Logo de la CR2016.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'Logo de la CR2015.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'Logo de la CR2014.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Ubicación"
  descricao:
    [
      "La CryptoRave 2018 será realizada en [Cinemateca Brasileira](http://www.cinemateca.gov.br/)!",
      "Dirección: Largo Senador Raul Cardoso, 207"
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64816915988922%2C-23.59343726458705%2C-46.64406001567841%2C-23.59066461201592&amp;layer=mapnik&amp;marker=-23.592050945626116%2C-46.64611458778381"
    link: "https://www.openstreetmap.org/way/33968285"
    tooltip: "Haga clic para abrir en una nueva pestaña"

festa:
  id: "festa"
  titulo: "Fiesta en Hangar 110"
  descricao:
    [
      "La fiesta de CryptoRave será el sábado, el 05/05, a las 20h en [Hangar 110](http://www.hangar110.com.br/). La entrada es gratis y exclusiva para participantes de CryptoRave!",
      "Dirección: Rua Rodolfo Miranda, 110 - Bom Retiro (5 minutos a pie del metro Armênia - linea azul)"
    ]
  img: "hangar110.jpg"
  link: "http://www.hangar110.com.br/"
  atracoes:
    [
      "20:00 - abertura de la casa!",
      "**LINEUP**",
      "21:00 - 21:45 - cabeça",
      "22:00 - 23:00 - afrorep",
      "23:00 - 00:00 - datamosh",
      "00:00 - 01:00 - octarina",
      "01:00 - 01:45 - show do retrigger",
      "01:45 - 03:00 - arkanoid",
      "03:00 - 04:15 - swaaag",
      "04:45 - 06:00 - lines",
      "**PROJEÇÕES**",
      "21:45 - 23:00 - Terms and conditions may apply",
      "23:30 - 00:45 - Bruno Treviso",
      "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (trecho)",
      "01:45 - 03:00 - NVVE MVE",
      "03:00 - 04:15 - chr0ma",
      "04:15 - 06:00 - Downloaded"
    ]

realizacao:
  id: "realizacao"
  titulo: "Realización"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo de la Escola de Ativismo: el texto 'ESCOLA DE ATIVISMO' (escuela de activismo) en blanco en el centro de uno rectángulo negro. El largo del rectángulo es un pouco mas grande que el largo del texto y su altura casi cuatro veces mas que la altura del texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo del Actantes: con fondo blanco, las letras 'ACT' en rojo, y en secuencia las letras 'ANTES' hechas con los números 0 e 1 en negro. No hay espacio entre las dos partes; el logo es una palabra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo del Saravá: al centro de un fondo rectangular negro, la palabra 'SARAVÁ' en blanco en una fuente que se asemeja al texto manuscrito. La altura de las letras varia entre cerca de un tercio y dos tercios de la altura del rectángulo. Horizontalmente, el texto ocupa cerca de cuatro quintos del ancho del rectángulo."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Logo del Intervozes, naranja en fondo blanco: un cuadrado con vértices redondeadas y con arcos de círculos blancos con diversas larguras le atravesando; en su derecha, en dos lineas con altura total poco mas pequeña que la altura del cuadrado, los textos 'intervozes' (en la primeira linea) y 'coletivo brasil de comunicação social' (en la segunda linea)."
        url: "http://intervozes.org.br"
    - linha:
      - img: "encriptatudo.png"
        descricao_img: "Logo del Encripta, en blanco: centralizados en un fondo rectangular negro, en la izquierda una imagen pixelada de una llave con una extremidad rodeada por corchetes, seguida del texto 'encripta'. La imagen ocupa cerca de la mitad de la altura del rectángulo, y el texto, cerca de un tercio. El largo del rectángulo en el fondo es ligeramente mas grande que la imagen seguida del texto."
        url: "https://encripta.org"

apoio:
  id: "apoio"
  titulo: "Soporte"
  parceiros:
    - linha:
      - img: "cinemateca.png"
        descricao_img: "Logo de Cinemateca Brasileira: em rojo, com fondo transparente, alineados a la derecha de la imagen, dos círculos posicionados verticalmente con un rectángulo entre ellos. El largo del rectángulo es aproximadamente tres veces el diámetro de los círculos, mientras su altura es de cerca de un quarto. Abajo, el texto 'cinemateca brasileira' centrado, en negro."
        url: "http://www.cinemateca.gov.br/"
      - img: "ccc.jpg"
        descricao_img: "Logo de Chaos Computer Club (CCC): en negro sobre un fondo blanco, los contornos de una imagen que se parece a una llave y a un circuito electrónico. Desde el interior de un rectángulo con las esquinas chaflanadas, 4 lineas direccionanse al centro del lado derecho; desde allá, alineanse perpendicularmente a este lado y salen del rectángulo, paralelas entre si. Después de una distancia de cerca de la mitad del comprimento del rectángulo, las lineas se retorcen, formando un nudo. Como hilos, las lineas colgan abajo del nudo."
        url: "https://ccc.de/"
      - img: "popsp.png"
        descricao_img: "Logo do Ponto de Presença da Rede Nacional de Pesquisa: em azul em um fundo branco, alinhado à esquerda da imagem, um quadrado azul com o contorno do estado de São Paulo e um ramificações eletrônicas. Do lado direito do quadrado azul, as letras PoP-SP ocupam a parte inferior."
        url: "http://www.pop-sp.rnp.br/"
      - img: "rnp.png"
        descricao_img: "Logo da Rede Nacional de Pesquisa: quadrado azul à esquerda com linhas brancas com pontos que se conectam numa formata geométrica não definida. Ao lado direito da imagem, as siglas em azul da organização RNP."
        url: "http://rnp.br"


patrocinadores:
  id: "patrocinadores"
  titulo: "Auspiciantes"
  descricao: "Nuestro más sincero agradecimiento a todos y todas las que contribuyeran con nuestra [campaña de financiamento colectivo](#financiamento):"
  doadores:
    [
      [
        "Adriano Ferreira",
        "Afonso Coutinho",
        "Airton Aparecido Zanon",
        "Akari Ueda",
        "Alan Capassi Zara",
        "Alberto Zanella",
        "Alexandra de Paula Yusiasu dos Santos",
        "Alexandre de Carvalho Dias",
        "Alexandre Gomes",
        "Alexandre Okita",
        "Alisson Moro Neocatto",
        "Álvaro Justen",
        "Amanda Rahra ",
        "Amanda Segnini",
        "Ana Carolina Guimarães Marques",
        "Ana Carolina Rezende",
        "Ana Cristina Lima",
        "Ana da Silva de Paula",
        "Ana Facundes",
        "Ana Luiza Portello Bastos",
        "Ana Maria Ghiringhello",
        "ANA SOUZA",
        "Anchises Moraes Guimaraes de Paula",
        "Anderson Campos de Oliveira",
        "Anderson Pereira Leal",
        "Anderson Ramos",
        "Andrei de Mesquita Almeida",
        "Andrius de Camargo",
        "André Andrade Baceti",
        "André Bonfatti",
        "André Figueiredo de Almeida",
        "André Moncaio Afonso",
        "André Pasti",
        "André Rodrigues Ferraz Barbosa",
        "André Scherma Soléo",
        "Angela Gennari",
        "Anna Cruz",
        "Anna Luíza Gannam",
        "Antonela",
      ],
      [
        "Antonio Arles dos Anjos Junior",
        "Ariel Morelli",
        "Arthur Fücher",
        "Aryel Bargas",
        "Augusto Bennemann",
        "Augusto Nery",
        "Beatriz Martins",
        "Beatriz Massaioli Franco dos Reis",
        "Bernardo Carvalho",
        "Bia Bouskela",
        "Bianca Moretto Ribeiro",
        "boklm",
        "brian lima dos santos",
        "Bruna Gonçalves",
        "Bruno Antunes Magrini",
        "Bruno Buys",
        "Bruno Caldas Vianna",
        "Bruno de Vasconcelos Cardoso",
        "Bruno Gama Silva Miranda",
        "Bruno Hannud",
        "brunz",
        "Caio Eduardo Zangirolami Saldanha",
        "Caio Emanuel Rhoden",
        "Caio Henrique Silva Ramos",
        "Caio Teixeira",
        "Cairo Aparecido Campos",
        "Camila Lainetti de Morais",
        "Capi Etheriel",
        "Carla Albertuni",
        "Carla Oliveira Santos",
        "Carlos Cabral",
        "Carlos Hifume",
        "Carol Caires",
        "Carol Caracol",
        "Carol Gutierrez",
        "Carol Pires",
        "Carolina Lemos de Oliveira",
        "Carolina Morandini",
        "Cauê Thiago Ribeiro",
        "Cecília Nunes do Lago Oliveira",
        "Corvolino"
      ],
      [
        "Centro de Autonomia Digital",
        "Cintia Sestelo Campos",
        "Clarissa Pellegrini Braga",
        "Clímax Brasil",
        "Cristiano Lincoln Mattos",
        "Cristina Barreto de Menezes Lopes",
        "César A. Kamiya",
        "Daniel Benvenutti",
        "Daniel Lenharo",
        "Daniel Mendes Cassiano",
        "Daniel Moro Iglesias",
        "Daniel Santini",
        "Daniel Seda",
        "Daniela Floriano Teixeira",
        "Daniela Lerda",
        "Daniela Palumbo",
        "Danilo Mendes",
        "Danilo Restaino",
        "Dann Luciano",
        "Davi Abreu Wasserberg",
        "Davi Amorim",
        "Davi Teofilo",
        "Deborah Lima Happ",
        "Dener Stassun Christinele",
        "Diego Borin Reeberg",
        "Diego de Freitas Aranha",
        "Diego Fernandes Barbosa",
        "Diego Rabatone Oliveira",
        "Diego Rossini Vieira",
        "Divina Vitorino",
        "Douglas Anunciação",
        "Douglas da Silva Costa",
        "Débora Prado ",
        "Edouard Jean Guillaume de Fraipont",
        "Eduardo Dias de Andrade",
        "Eduardo Marcondes",
        "Eduardo Mendes",
        "Elias Tandel Barrionovo",
        "Elisabete Santana",
        "Elisa X",
        "Emerson Marques Pedro"
      ],
      [
        "Emmanuel G Brandão",
        "Enzzo Cavallo",
        "Eric Ribeiro",
        "Erick Muller",
        "Erik Teixeira Gonçalves Rodrigues",
        "Escola de Ativismo",
        "Evandro Ap Dos Santos",
        "Evelize Pacheco Simões",
        "Evelyn Gomes ",
        "Ewerton Queiroz",
        "fabianne batista balvedi",
        "Fabio Diniz",
        "Fabricio Biazzotto",
        "Fatima Conti",
        "Felipe Celeri",
        "Felipe Correa",
        "Felipe da Silva Souza",
        "Felipe Duarte Domingues",
        "Felipe Pait",
        "felippe santos",
        "Fernanda Beirao",
        "Fernanda Bruno",
        "Fernanda Campagnucci",
        "Fernanda Fantelli",
        "Fernanda Monteiro",
        "Fernanda Shirakawa",
        "Fernanda Tosta",
        "Fernando Boaglio",
        "Fernando Lobato",
        "Fernando Paiva",
        "Filipe Meirelles",
        "Flavia Gannam",
        "Flora Cytrynowicz",
        "Florence Poznanski",
        "Flávia Lefèvre Guimarães",
        "Flávio Amieiro",
        "Flávio Sousa",
        "Francele Cocco",
        "Francine Emilia Costa",
        "Francisco Borges Fernandes Júnior"
      ],
      [
        "Francisco dos Santos Ekman Simões",
        "Francisco Haroldo Barbosa da Silva",
        "Francisco Namias Vicente",
        "Gabi Juns",
        "GABRIEL ALEJANDRO DA SILVA MARQUEZ ME EPP",
        "Gabriel Andrade",
        "Gabriel Becker",
        "Gabriel Borges",
        "Gabriel Caropreso",
        "Gabriel D'Ambrosio",
        "Gabriel Fanelli",
        "Gabriel Gortan",
        "Gabriel Lindenbach",
        "Gabriel Morais",
        "Gabriel S.",
        "Gabriel Torres Gomes Pato",
        "gabriel vituri",
        "Gabriela Dias Lopes",
        "Gabriela Gannam",
        "Gabriela Guerra",
        "Gabriela Pompermayer",
        "Gabriella De Biaggi",
        "Georgia Nicolau",
        "Geraldo Aleandro",
        "Geraldo Barros",
        "Gibran Sirena",
        "Giovane Santos Liberato",
        "Giovani Ferreira",
        "Giovanni Leoni Brito",
        "Gisele Kauer",
        "Graciela Natansohn",
        "Guilherme Augusto",
        "Guilherme Bigois",
        "Guilherme Botelho Diniz Junqueira",
        "Guilherme Gondim",
        "Guilherme Rodrigues Bueno",
        "Guilherme Xavier Silva",
        "Gustavo Avila",
        "Gustavo Felipe Vieira de Alencar",
        "gustavo gus"
      ],
      [
        "Gustavo Padovan",
        "Gustavo Pereira Dutra",
        "GUSTAVO ROBERTO RODRIGUES GONÇALVES",
        "Gut Simon",
        "HacKan",
        "Hanna F. Mariano",
        "Hans Zwolsman",
        "Haydee Svab",
        "Helder Betiol",
        "Henrique Bispo Dos Santos",
        "Henrique Bronzoni",
        "henrique dos reis miguel",
        "Henrique Fernanndes",
        "Henrique Machado Gonçalves",
        "Henrique Parra",
        "Henrique Specian",
        "Hiro",
        "Hitalo Cesar Alves",
        "Ian Fernandez",
        "Ian Thomaz Puech",
        "Ieremies Vieira da Fonseca Romero",
        "Igor Frederick Cabral Ferreira da Silva",
        "Igor Santos",
        "Igor Schwab Hoyer",
        "inae batistoni e silva",
        "Ingrid Elisabeth Spangler",
        "Instituto Alana ",
        "Intervozes",
        "isabela dias fernandes",
        "Ivan Korkischko",
        "Ivo Nascimento",
        "Ivone Rocha",
        "Izabel Marques Meo",
        "Jamila Venturini",
        "Janaina Centini",
        "Janaina Menegaz Spode",
        "Jean Paul Ruiz Depraz",
        "Jean Tible",
        "Jefferson Souza",
        "JENIFFER BRITO JOHANSEN"
      ],
      [
        "Jeronimo Cordoni Pellegrini",
        "Jessé Rodrigues de Souza",
        "joao moreno rodrigues falcao",
        "Joao Rafael Bonilha",
        "Jonas Lima de Amorim",
        "Jonaya de Castro Garbe",
        "JOSE ALONSO BALBI",
        "Joselia Leite Costa",
        "José",
        "Joyce Ariane de Souza",
        "João Cassino",
        "João Corrêa",
        "João Phillipe Cardenuto",
        "João Victor Maruca",
        "Julee D'Addio",
        "Julia Gimenes C Ferreira",
        "Julia Issa",
        "Juliana Amoasei dos Reis",
        "Juliana Felicidade Armede",
        "Juliana Ito",
        "Juliana Myaki Bueno",
        "julio antonio mendonca de marins",
        "Julio Monteiro",
        "Julio Pancracio Valim",
        "Jussara Ribeiro de Oliveira",
        "Júlia Carmona",
        "Júlio Cortijo",
        "Kevin Martins",
        "Laila Dell'Antonia Scarassati",
        "Lais Mastelari",
        "Laisa Beatris Silva Pereira",
        "Larissa Dominique",
        "Larissa Marques",
        "Larissa Pinho Alves",
        "Laura Alves Gonzaga",
        "Laura Sobral",
        "Laís Figueiredo",
        "Leandro Salvador",
        "Leo Barbosa Reis",
        "Leo Milano"
      ],
      [
        "Leonardo B.",
        "Leonardo Yvens Schwarzstein",
        "Ligia Luz ",
        "Liliane Moiteiro Caetano",
        "Lucas Albertine De Godoi",
        "LUCAS DAIKI",
        "Lucas dos Santos Tinti",
        "Lucas Lago",
        "Lucas Matheus Testa",
        "Lucas Matias Tei",
        "Lucas Monteiro de Oliveira",
        "Lucas Pereira Baumgartner",
        "Lucas Pretti",
        "Lucas Rogério de Oliveira",
        "Lucas Teixeira",
        "Luciana Masini",
        "Luciana Minami",
        "Luhdy Cavalcanti Sardinha",
        "Luis Arantes",
        "Luis Fernando Fagundes Rovai",
        "Luis Otavio Ribeiro",
        "Luiz Filipe Moresco da Silva",
        "Luiz Karl",
        "Luiza Peixe",
        "Luã Fergus Oliveira da Cruz",
        "Lívia Dias",
        "maiana abi",
        "Maicon Gabriel de Oliveira",
        "Maranha",
        "Marcela Cristina Fogaça Vieira",
        "Marcelo Calheiros",
        "Marcelo Gimenes de Oliveira",
        "Marcelo Gonçalves da Cruz",
        "Marcelo Marquesini",
        "Marcelo Miky Mine",
        "Marcio Ferreira de Araujo Junior",
        "Marcio Ivan",
        "Marco Antonio de Castro Melo",
        "Marcos Felipe",
        "Marcos Flávio de Oliveira"
      ],
      [
        "Marcus Tenório",
        "Marcus V. Kuquert",
        "Maria Rita Casagrande",
        "Maria Rita Guedes Carvalho",
        "Maria Teresa Aarao",
        "Mariana Belmont",
        "Mariana Camargo Simão",
        "Mariana Jó de Souza",
        "Marilia Ramos",
        "marina frota",
        "Marina Kohler Harkot",
        "Marina Veloso",
        "Mario Balan",
        "Marisa Sanematsu",
        "mariuza pregnolato",
        "Marília M Pisani",
        "Marília Nunes Paulino da Silva",
        "Mateus Rodrigues Costa",
        "Matheus Gaboardi Tralli",
        "Matheus Tadeu Rabelo Querino",
        "Mayara Ferreira",
        "Maíra Mendes Galvão",
        "Mellina Yonashiro",
        "meskio",
        "Micah",
        "Michel Marechal",
        "Miguel Faggioni Fernandes",
        "Miguel Vieira",
        "Mobi Yabiku Neto",
        "Monalisa Santos",
        "MUNDANO",
        "Narrira",
        "Natan Rodrigues",
        "Nathalia Sautchuk Patricio",
        "Natália Satie Odashima",
        "Nicole Zafalon Kovacs",
        "Nikos Nikola",
        "Odélio Porto Jr.",
        "Ola Bini",
        "Olavo Silva"
      ],
      [
        "Omayr B. C. Zanata",
        "Oscar Esgalha",
        "Pablo Valério Polônia",
        "Patrícia Sodré",
        "PAULO ALMEIDA",
        "Paulo Faltay",
        "Paulo Henrique de Lima Santana",
        "Pedro Abrahao Lameirinhas Malina",
        "Pedro Henrique Braz",
        "Pedro Kelson Batinga de Mendonça",
        "Pedro Luiz Santos",
        "Pedro Markun",
        "Pedro Pereira",
        "Pedro Roberto",
        "Pedro Teles",
        "Pedro Telles",
        "Philip Benutte Oliveira Lima",
        "Philip Sampaio Silva",
        "Pietro Di Consolo Gregorio",
        "PIMP MY CARROÇA",
        "Rafael Bantu",
        "Rafael Beraldo",
        "Rafael Chaud Frazão",
        "Rafael Humberto de Lima Moretti",
        "Rafael Perazza Mendes",
        "Rafael Rabelo",
        "Rafael Santos Sampaio",
        "rafaela rodrigues rossi",
        "Raul Luiz",
        "Regiane Silva",
        "Renata Aquino Ribeiro",
        "Renata Assumpção",
        "Renata Carvalho de Lara Campos",
        "Renato Contessotto",
        "Renato Leite Monteiro",
        "Ricardo Borges Martins",
        "Ricardo Oliveira",
        "Ricardo Poppi",
        "Ricardo Shiota Yasuda"
      ],
      [
        "Roberta Fernandes",
        "Rodolfo Rodrigues",
        "Rodrigo Alexandre Costa",
        "Rodrigo Ardissom de Souza",
        "Rodrigo José Firmino",
        "Rodrigo Machado ",
        "Rodrigo Perez",
        "Rodrigo Primo",
        "Rodrigo Vieira Campos",
        "Rosana Pereira de Miranda",
        "RTFM",
        "Samantha Rosa da Rosa",
        "Samuel Reghim Silva",
        "Sarah Nery",
        "Sergio Amadeu da Silveira",
        "Sergio da Silva",
        "Sergio Leandro do Nascimento Garcia",
        "Silas Batista",
        "Silas Justiniano Veiga da Silva",
        "Stefanie Melo",
        "Talita Rodrigues",
        "Tanous Camara Azzi Melo",
        "Tarcizio Matheus Zanfolin",
        "Tatiana Helena Criscione",
        "Tatiane Bolsonaro Guimarães ",
        "Thais de Almeida Olmos",
        "Thais Nascimento Dantas",
        "Thais Teodoro Perez",
        "Thamy Matarozzi",
        "Thaynan Wigor Magalhães de Almeida",
        "thiago augusto de siqueira",
        "Thiago Benicchio",
        "Thiago Carrapatoso",
        "Thiago Firbida",
        "Thiago Maia",
        "Thiago Nacao",
        "Thiago Ruiz da Silva Matos",
        "Tiago Kiill",
        "Tiago P L Rubini",
        "Tica Moreno"
      ],
      [
        "Tobias Sette Ferreira",
        "Tuanny Ruiz",
        "Tulio Malaspina",
        "Túlio Casagrande Alberto",
        "Vanessa da Silva",
        "Vanessa Me Tonini",
        "Veridiana Alimonti",
        "Victor Ferreira Silva",
        "Victor Miguez",
        "Victor Perin",
        "Victor Veloso",
        "Vilma Santos",
        "Vinicius Barrionuevo",
        "Vinicius Freitas",
        "Vinicius guilherme da silva",
        "Vinicius Rezende Costa",
        "Vinícius Andrade",
        "Vinícius de Jesus Smadeski",
        "Vinícius Felisberto",
        "Vitor",
        "Vitor Abud",
        "Wagner Bertolini Junior",
        "Wallace Felipe Rodrigues Francisco",
        "Wandemberg Gibaut",
        "Wenceslao Machado de Oliveira Jr",
        "Wesley Mataran",
        "William Oliveira",
        "William Saraiva Cimino",
        "Yoav Passy",
        "Yorik van Havre",
        "Yumi Sakate",
        "Yuri Maia Santos",
        "Yuri Robert Censi",
        "Zeesy Powers"
      ]
    ]

contato:
  id: "contato"
  titulo: "Contacto"
  subtitulo: "Envíe su sugerencia o inquietud."
  descricao: "Imagen de una ciudad por la noche, foto tomada con longa exposición. En la parte superior, un cielo sin estrellas rellena un quinto de la imagen, edificios con ventanas alumbradas llenan el espacio restante. En la parte inferior izquierda esta visible parte de una calle, con camino hechos con los faros de los coches debido a la longa exposición."
  button_text: "Enviar"
  enviando: "Aguardando la confirmación del envio..."
  obrigado: "Gracias por ponerse en contacto con nosotros!"
  erro: "Ups, la mensaje no logró ser enviada. Por favor póngase en contacto por el correo: contato@cryptorave.org."
  validacao:
    nome:
      placeholder: "Nombre (opcional)"
    email:
      placeholder: "Correo *"
      aviso: "Digite la dirección de su correo electrónico."
    mensagem:
      placeholder: "Su mensaje *"
      aviso: "Digite su mensaje."

footer:
  copyright: "CryptoRave * https://cryptorave.org/ * 2018 | Onion service: http://utw4svtv5ccjastc.onion/"
  antiassedio:
    texto: "Código de Conduta"
    link: "https://we.riseup.net/cryptorave/codigo-de-conduta"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
---
