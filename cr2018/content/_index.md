---
language_code: "pt-br"
title: "CryptoRave 2018"

header:
  descricao:
    "Imagem noturna de longa exposição de uma cidade, com uma rua em primeiro plano, árvores em segundo plano, ruas e prédios iluminados ao fundo. Os trajetos dos faróis de veículos são visíveis devido à exposição longa. À frente, sobreposto à imagem, o logo da CR2018 em cor branca: a uma inclinação de cerca de 30 graus para baixo, as letras 'CR' formadas pelos números 0 e 1, um processador com uma imagem pixelada de uma chave em seu centro, e o texto 'UM SPECTRE NOS RONDA' na linha abaixo. À direita e sem inclinação, o número '2018' em duas linhas (com dois algarismos por linha), com altura total aproximadamente igual à do logo inclinado."

sobre:
  id: "sobre"
  descricao:
    [
      "Entre os dias 4 e 5 de maio de 2018, na cidade de São Paulo – SP, durante 36 horas, a CryptoRave (CR) traz atividades sobre segurança, criptografia, hacking, anonimato, privacidade e liberdade na rede. Essa é a **5ª edição do evento!**",
      "A CryptoRave inspira-se na ação global e descentralizada da [CryptoParty](https://cryptoparty.in/), a qual têm como objetivo difundir os conceitos fundamentais e softwares básicos de criptografia. Criada em 2014, a cada edição mais de 2500 pessoas participaram!",
      "A participação nas atividades é aberta mediante a [inscrição online gratuita](#inscricao). Confira [aqui](#programacao) a programação da CR2018!"
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "Logo da CR2018, em cor branca com fundo preto: à esquerda, as letras 'CR' formadas pelos números 0 e 1; ao meio, uma imagem de um processador com uma chave pixelada em seu centro; à direita, o número '2018' escrito em duas linhas (com dois algarismos por linha) em fonte menor, de forma que a soma de suas alturas seja igual à altura do texto 'CR'."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamento"
  inscricao: "Inscrição"
  keynotes: "Keynotes"
  programacao: "Programação"
  festa: "Festa"
  realizacao: "Realização"
  apoio: "Apoio"
  patrocinadores: "Patrocinadores"
  contato: "Contato"
  local: "Local"
  menu_extra:
    - name: "Grade"
      url: "https://cpa.cryptorave.org/pt-BR/cr2018/public/schedule"
      weight: 9"
    - name: "Onion"
      url:  "https://onion.cryptorave.org"
      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamento Coletivo"
  descricao: "A campanha de financiamento da CryptoRave 2018 foi encerrada com sucesso! Muito obrigado a [todos os que contribuíram](#patrocinadores), o evento não poderia acontecer sem a sua colaboração."
  link_catarse: "https://www.catarse.me/pt/projects/70453/embed"
  titulo_btc: "Colabore em Bitcoin"
  descricao_btc: "Nós temos uma carteira de bitcoin, você pode doar para o endereço abaixo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para o endereço Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
  recompensas:
    - linha:
      - valor: "R$15"
        img: ""
        descricao_img: ''
        descricao: "**FREE HUGS** - AGRADECIMENTO escrito nas nuvens (site)"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"

      - valor: "R$25"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOSTICKER** - 2 ADESIVOS DA CRYPTORAVE 2018 + agradecimento nas nuvens"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"

      - valor: "R$40"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCAM** - 4 ADESIVOS PARA COBRIR A WEBCAM + 1 BOTTOM CR2018 + 2 ADESIVOS CR2018 + agradecimento nas nuvens"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"

    - linha:
      - valor: "R$50"
        img: ""
        descricao_img: ""
        descricao: "**SENIORSTICKER** - 4 ADESIVOS ESPECIAIS + 2 ADESIVOS DA CR2018 + agradecimento nas nuvens"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"

      - valor: "R$60"
        img: ""
        descricao_img: ""
        descricao: "**CANETAMÁGYCA** - NOVIDADE!!! Escreva com tinta invisível. Revele com luz mágyca. 1 CANETA MÁGYCA + 2 ADESIVOS CR2018 + agradecimento nas nuvens"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"

      - valor: "R$75"
        img: ""
        descricao_img: ""
        descricao: "**MASTERSTICKER** - 4 ADESIVOS ESPECIAIS + 4 ADESIVOS PARA COBRIR A WEBCAM + 1 BOTTOM CR2018 + 2 ADESIVOS CR2018 + agradecimento nas nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"

    - linha:
      - valor: "R$120"
        img: ""
        descricao_img: ""
        descricao: "**CRIPTOKNEK** - 1 CANECA DA CRYPTORAVE 2018 + 2 ADESIVOS CR2018 + agradecimento nas nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"

      - valor: "R$135"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTORAVER** - 1 CAMISETA ESPECIAL DE APOIADOR + 2 ADESIVOS DA CR2018 + agradecimento nas nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"

      - valor: "R$375"
        img: ""
        descricao_img: ''
        descricao: "**MASTER CRYPTORAVER** - 1 MOLETON CRYPTORAVE 2018 + 2 ADESIVOS DA CR2018 + agradecimento nas nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"

    - linha:
      - valor: "R$475"
        img: ""
        descricao_img: ''
        descricao: "**HEROIC CRYPTORAVER** - 1 MOLETON CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE APOIADOR + 2 ADESIVOS CR2018 + agradecimento nas nuvens"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"

      - valor: "R$700"
        img: ""
        descricao_img: ''
        descricao: "**CRYPTOJEDI** - 1 MOLETON CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE APOIADOR + 1 CANECA CRYPTORAVE 2018 + 4 ADESIVOS ESPECIAIS + 4 ADESIVOS PARA COBRIR A WEBCAM + 1 BOTTOM CR2018 + 2 ADESIVOS CR2018 + agradecimento nas nuvens."
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"

      - valor: "R$1000"
        img: ""
        descricao_img: ''
        descricao: "**CRIPTOJORNADA** - OFICINA DE SEGURANÇA DA INFORMAÇÃO PARA SEU COLETIVO OU ORGANIZAÇÃO + agradecimento nas nuvens"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Inscrição"
  descricao: "Para participar do evento e saber sobre os preparativos da CryptoRave, inscreva-se no nosso boletim de novidades."
  link: "[Clique para se inscrever na CryptoRave](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brasil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Coordenadora dos times do Projeto Tor, recentemente foi anunciada como a próxima Diretora Executiva do projeto."
  descricao: "O problema é muito maior que o Facebook. É o modelo econômico da Internet: o modelo de vigilância da Internet. Comunicar, compartilhar e acessar informações não deveria tornar você um alvo ou um produto. Você não deveria ser explorado/a ao usar a Internet."


programacao:
  id: "programacao"
  titulo: "Programação"
  descricao:
    [
      "A versão 1.0 (quase final) da programação da CryptoRave 2018 está disponível [aqui](https://cpa.cryptorave.org/pt-BR/cr2018/public/schedule)! A programação está sujeita a alterações e ainda há atividades pendentes. O evento começa às 19 horas do dia 4 de maio."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'Logo da CR2017.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'Logo da CR2016.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'Logo da CR2015.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'Logo da CR2014.'
        url: "https://2014.cryptorave.org"


local:
  id: "local"
  titulo: "Local"
  descricao:
    [
      "A CryptoRave 2018 será realizada na [Cinemateca Brasileira](http://www.cinemateca.gov.br/)!",
      "Endereço: Largo Senador Raul Cardoso, 207"
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64816915988922%2C-23.59343726458705%2C-46.64406001567841%2C-23.59066461201592&amp;layer=mapnik&amp;marker=-23.592050945626116%2C-46.64611458778381"
    link: "https://www.openstreetmap.org/way/33968285"
    tooltip: "Clique para abrir em outra aba"

festa:
  id: "festa"
  titulo: "Festa no Hangar 110"
  descricao:
    [
      "A festa da CryptoRave acontecerá no sábado, dia 05/05, às 20h no [Hangar 110](http://www.hangar110.com.br/). A entrada é gratuita e exclusiva para participantes da CryptoRave!",
      "Endereço: Rua Rodolfo Miranda, 110 - Bom Retiro (5 minutos a pé do metrô Armênia - linha azul)"
    ]
  img: "hangar110.jpg"
  link: "http://www.hangar110.com.br/"
  atracoes:
    [
      "20:00 - abertura da casa!",
      "**LINEUP**",
      "21:00 - 21:45 - [cabeça em movimento](https://youtu.be/SuPf8r1mFBU)",
      "22:00 - 23:00 - [afrorep](https://www.youtube.com/channel/UCEuMHCeXbTnX5dPxPc_hW8A/featured)",
      "23:00 - 00:00 - [datamosh](https://soundcloud.com/itsdatamosh)",
      "00:00 - 01:00 - [octarina](https://soundcloud.com/octarinalux)",
      "01:00 - 01:45 - show do retrigger",
      "01:45 - 03:00 - arkanoid",
      "03:00 - 04:15 - [swaaag](http://swaaag.bandcamp.com/)",
      "04:45 - 06:00 - [lines](https://www.mixcloud.com/meistache/)",
      "**PROJEÇÕES**",
      "21:45 - 23:00 - Terms and conditions may apply",
      "23:30 - 00:45 - Bruno Treviso",
      "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (trecho)",
      "01:45 - 03:00 - NVVE MVE",
      "03:00 - 04:15 - chr0ma",
      "04:15 - 06:00 - Downloaded"
    ]

realizacao:
  id: "realizacao"
  titulo: "Realização"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo da Escola de Ativismo: o texto 'ESCOLA DE ATIVISMO' em branco no centro de um retângulo preto. O retângulo tem largura pouco maior que a do texto e altura aproximadamente quatro vezes a altura do texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo da Actantes: com fundo branco, as letras 'ACT' em vermelho, e, em seguida, as letras 'ANTES' formadas pelos números 0 e 1 em preto. Não há espaçamento entre as duas partes; o logo é uma palavra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo do Saravá: ao centro de um fundo retangular preto, a palavra 'SARAVÁ' em branco em uma fonte que se assemelha a texto manuscrito. A altura das letras varia entre cerca de um terço e dois terços da altura do retângulo. Horizontalmente, o texto ocupa cerca de quatro quintos do comprimento do retângulo."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Logo do Intervozes, laranja em fundo branco: um quadrado com vértices arredondados com arcos de círculo brancos de larguras diversas atravessando-o; à direita, em duas linhas com altura total pouco menor que a do quadrado, os textos 'intervozes' (na primeira liha) e 'coletivo brasil de comunicação social (na segunda linha)."
        url: "http://intervozes.org.br"
    - linha:
      - img: "encriptatudo.png"
        descricao_img: "Logo do Encripta, em branco: centralizados em um fundo retangular preto, a imagem pixelada de uma chave com uma extremidade cercada por colchetes à esquerda, seguida do texto 'encripta'. A imagem ocupa cerca de metade da altura do retângulo, e o texto, cerca de um terço. A largura do retângulo de fundo é ligeiramente maior do que a imagem seguida do texto."
        url: "https://encripta.org"

apoio:
  id: "apoio"
  titulo: "Apoio"
  parceiros:
    - linha:
      - img: "cinemateca.png"
        descricao_img: "Logo da Cinemateca Brasileira: em vermelho, com fundo transparente, alinhados à direita da imagem, dois círculos posicionados verticalmente com um retângulo vermelho entre eles. O retângulo tem comprimento de aproximadamente três vezes o diâmetro dos círculos e altura de um quarto do diâmetro. Abaixo, o texto 'cinemateca brasileira' centralizado em preto."
        url: "http://www.cinemateca.gov.br/"
      - img: "ccc.jpg"
        descricao_img: "Logo do Chaos Computer Club (CCC): em preto em um fundo branco, os contornos de uma imagem que se assemelha a uma imagem e a um circuito eletrônico. De dentro de um retângulo com os cantos chanfrados, 4 linhas se direcionam ao centro do lado direito; de lá, se alinham perpendicularmente a este lado e saem do retângulo, paralelas entre si. Após uma distância de cerca de metade do comprimento do retângulo, as linhas se torcem, formando um nó. Se assemelhando a fios, as linhas pendem torcidas abaixo do nó."
        url: "https://ccc.de/"
      - img: "popsp.png"
        descricao_img: "Logo do Ponto de Presença da Rede Nacional de Pesquisa: em azul em um fundo branco, alinhado à esquerda da imagem, um quadrado azul com o contorno do estado de São Paulo e um ramificações eletrônicas. Do lado direito do quadrado azul, as letras PoP-SP ocupam a parte inferior."
        url: "http://www.pop-sp.rnp.br/"
      - img: "rnp.png"
        descricao_img: "Logo da Rede Nacional de Pesquisa: quadrado azul à esquerda com linhas brancas com pontos que se conectam numa formata geométrica não definida. Ao lado direito da imagem, as siglas em azul da organização RNP."
        url: "http://rnp.br"


patrocinadores:
  id: "patrocinadores"
  titulo: "Patrocinadores"
  descricao: "Agradecemos de coração a todas e todos que contribuíram com a nossa [campanha de financiamento coletivo](#financiamento):"
  doadores:
    [
      [
        "Adriano Ferreira",
      	"Afonso Coutinho",
      	"Airton Aparecido Zanon",
      	"Akari Ueda",
      	"Alan Capassi Zara",
      	"Alberto Zanella",
      	"Alexandra de Paula Yusiasu dos Santos",
      	"Alexandre de Carvalho Dias",
      	"Alexandre Gomes",
      	"Alexandre Okita",
      	"Alisson Moro Neocatto",
      	"Álvaro Justen",
      	"Amanda Rahra ",
      	"Amanda Segnini",
      	"Ana Carolina Guimarães Marques",
      	"Ana Carolina Rezende",
      	"Ana Cristina Lima",
      	"Ana da Silva de Paula",
      	"Ana Facundes",
      	"Ana Luiza Portello Bastos",
      	"Ana Maria Ghiringhello",
      	"ANA SOUZA",
      	"Anchises Moraes Guimaraes de Paula",
      	"Anderson Campos de Oliveira",
      	"Anderson Pereira Leal",
      	"Anderson Ramos",
      	"Andrei de Mesquita Almeida",
      	"Andrius de Camargo",
      	"André Andrade Baceti",
      	"André Bonfatti",
      	"André Figueiredo de Almeida",
      	"André Moncaio Afonso",
      	"André Pasti",
      	"André Rodrigues Ferraz Barbosa",
      	"André Scherma Soléo",
      	"Angela Gennari",
      	"Anna Cruz",
      	"Anna Luíza Gannam",
      	"Antonela",
      ],
      [
        "Antonio Arles dos Anjos Junior",
      	"Ariel Morelli",
      	"Arthur Fücher",
      	"Aryel Bargas",
      	"Augusto Bennemann",
      	"Augusto Nery",
      	"Beatriz Martins",
      	"Beatriz Massaioli Franco dos Reis",
      	"Bernardo Carvalho",
      	"Bia Bouskela",
      	"Bianca Moretto Ribeiro",
      	"boklm",
      	"brian lima dos santos",
      	"Bruna Gonçalves",
      	"Bruno Antunes Magrini",
      	"Bruno Buys",
      	"Bruno Caldas Vianna",
      	"Bruno de Vasconcelos Cardoso",
      	"Bruno Gama Silva Miranda",
      	"Bruno Hannud",
      	"brunz",
      	"Caio Eduardo Zangirolami Saldanha",
      	"Caio Emanuel Rhoden",
      	"Caio Henrique Silva Ramos",
      	"Caio Teixeira",
      	"Cairo Aparecido Campos",
      	"Camila Lainetti de Morais",
      	"Capi Etheriel",
      	"Carla Albertuni",
      	"Carla Oliveira Santos",
      	"Carlos Cabral",
      	"Carlos Hifume",
      	"Carol Caires",
      	"Carol Caracol",
      	"Carol Gutierrez",
      	"Carol Pires",
      	"Carolina Lemos de Oliveira",
      	"Carolina Morandini",
      	"Cauê Thiago Ribeiro",
      	"Cecília Nunes do Lago Oliveira",
        "Corvolino"
      ],
      [
        "Centro de Autonomia Digital",
      	"Cintia Sestelo Campos",
      	"Clarissa Pellegrini Braga",
      	"Clímax Brasil",
      	"Cristiano Lincoln Mattos",
      	"Cristina Barreto de Menezes Lopes",
      	"César A. Kamiya",
      	"Daniel Benvenutti",
      	"Daniel Lenharo",
      	"Daniel Mendes Cassiano",
      	"Daniel Moro Iglesias",
      	"Daniel Santini",
      	"Daniel Seda",
      	"Daniela Floriano Teixeira",
      	"Daniela Lerda",
      	"Daniela Palumbo",
      	"Danilo Mendes",
      	"Danilo Restaino",
      	"Dann Luciano",
      	"Davi Abreu Wasserberg",
      	"Davi Amorim",
      	"Davi Teofilo",
      	"Deborah Lima Happ",
      	"Dener Stassun Christinele",
      	"Diego Borin Reeberg",
      	"Diego de Freitas Aranha",
      	"Diego Fernandes Barbosa",
      	"Diego Rabatone Oliveira",
      	"Diego Rossini Vieira",
      	"Divina Vitorino",
      	"Douglas Anunciação",
      	"Douglas da Silva Costa",
      	"Débora Prado ",
      	"Edouard Jean Guillaume de Fraipont",
      	"Eduardo Dias de Andrade",
      	"Eduardo Marcondes",
      	"Eduardo Mendes",
      	"Elias Tandel Barrionovo",
      	"Elisabete Santana",
        "Elisa X",
      	"Emerson Marques Pedro"
      ],
      [
        "Emmanuel G Brandão",
      	"Enzzo Cavallo",
      	"Eric Ribeiro",
      	"Erick Muller",
      	"Erik Teixeira Gonçalves Rodrigues",
      	"Escola de Ativismo",
      	"Evandro Ap Dos Santos",
      	"Evelize Pacheco Simões",
      	"Evelyn Gomes ",
      	"Ewerton Queiroz",
      	"fabianne batista balvedi",
      	"Fabio Diniz",
      	"Fabricio Biazzotto",
      	"Fatima Conti",
      	"Felipe Celeri",
      	"Felipe Correa",
      	"Felipe da Silva Souza",
      	"Felipe Duarte Domingues",
      	"Felipe Pait",
      	"felippe santos",
      	"Fernanda Beirao",
      	"Fernanda Bruno",
      	"Fernanda Campagnucci",
      	"Fernanda Fantelli",
      	"Fernanda Monteiro",
      	"Fernanda Shirakawa",
      	"Fernanda Tosta",
      	"Fernando Boaglio",
      	"Fernando Lobato",
      	"Fernando Paiva",
      	"Filipe Meirelles",
      	"Flavia Gannam",
      	"Flora Cytrynowicz",
      	"Florence Poznanski",
      	"Flávia Lefèvre Guimarães",
      	"Flávio Amieiro",
      	"Flávio Sousa",
      	"Francele Cocco",
      	"Francine Emilia Costa",
      	"Francisco Borges Fernandes Júnior"
      ],
      [
        "Francisco dos Santos Ekman Simões",
      	"Francisco Haroldo Barbosa da Silva",
      	"Francisco Namias Vicente",
      	"Gabi Juns",
      	"GABRIEL ALEJANDRO DA SILVA MARQUEZ ME EPP",
      	"Gabriel Andrade",
      	"Gabriel Becker",
      	"Gabriel Borges",
      	"Gabriel Caropreso",
      	"Gabriel D'Ambrosio",
      	"Gabriel Fanelli",
      	"Gabriel Gortan",
      	"Gabriel Lindenbach",
      	"Gabriel Morais",
      	"Gabriel S.",
      	"Gabriel Torres Gomes Pato",
      	"gabriel vituri",
      	"Gabriela Dias Lopes",
      	"Gabriela Gannam",
      	"Gabriela Guerra",
      	"Gabriela Pompermayer",
      	"Gabriella De Biaggi",
      	"Georgia Nicolau",
      	"Geraldo Aleandro",
      	"Geraldo Barros",
      	"Gibran Sirena",
      	"Giovane Santos Liberato",
      	"Giovani Ferreira",
      	"Giovanni Leoni Brito",
      	"Gisele Kauer",
      	"Graciela Natansohn",
      	"Guilherme Augusto",
      	"Guilherme Bigois",
      	"Guilherme Botelho Diniz Junqueira",
      	"Guilherme Gondim",
      	"Guilherme Rodrigues Bueno",
      	"Guilherme Xavier Silva",
      	"Gustavo Avila",
      	"Gustavo Felipe Vieira de Alencar",
      	"gustavo gus"
      ],
      [
        "Gustavo Padovan",
      	"Gustavo Pereira Dutra",
      	"GUSTAVO ROBERTO RODRIGUES GONÇALVES",
      	"Gut Simon",
      	"HacKan",
      	"Hanna F. Mariano",
      	"Hans Zwolsman",
      	"Haydee Svab",
      	"Helder Betiol",
      	"Henrique Bispo Dos Santos",
      	"Henrique Bronzoni",
      	"henrique dos reis miguel",
      	"Henrique Fernanndes",
      	"Henrique Machado Gonçalves",
      	"Henrique Parra",
      	"Henrique Specian",
      	"Hiro",
      	"Hitalo Cesar Alves",
      	"Ian Fernandez",
      	"Ian Thomaz Puech",
      	"Ieremies Vieira da Fonseca Romero",
      	"Igor Frederick Cabral Ferreira da Silva",
      	"Igor Santos",
      	"Igor Schwab Hoyer",
      	"inae batistoni e silva",
      	"Ingrid Elisabeth Spangler",
      	"Instituto Alana ",
      	"Intervozes",
      	"isabela dias fernandes",
      	"Ivan Korkischko",
      	"Ivo Nascimento",
      	"Ivone Rocha",
      	"Izabel Marques Meo",
      	"Jamila Venturini",
      	"Janaina Centini",
      	"Janaina Menegaz Spode",
      	"Jean Paul Ruiz Depraz",
      	"Jean Tible",
      	"Jefferson Souza",
      	"JENIFFER BRITO JOHANSEN"
      ],
      [
        "Jeronimo Cordoni Pellegrini",
      	"Jessé Rodrigues de Souza",
      	"joao moreno rodrigues falcao",
      	"Joao Rafael Bonilha",
      	"Jonas Lima de Amorim",
      	"Jonaya de Castro Garbe",
      	"JOSE ALONSO BALBI",
      	"Joselia Leite Costa",
      	"José",
      	"Joyce Ariane de Souza",
      	"João Cassino",
      	"João Corrêa",
      	"João Phillipe Cardenuto",
      	"João Victor Maruca",
      	"Julee D'Addio",
      	"Julia Gimenes C Ferreira",
      	"Julia Issa",
      	"Juliana Amoasei dos Reis",
      	"Juliana Felicidade Armede",
      	"Juliana Ito",
      	"Juliana Myaki Bueno",
      	"julio antonio mendonca de marins",
      	"Julio Monteiro",
      	"Julio Pancracio Valim",
      	"Jussara Ribeiro de Oliveira",
      	"Júlia Carmona",
      	"Júlio Cortijo",
      	"Kevin Martins",
      	"Laila Dell'Antonia Scarassati",
      	"Lais Mastelari",
      	"Laisa Beatris Silva Pereira",
      	"Larissa Dominique",
      	"Larissa Marques",
      	"Larissa Pinho Alves",
      	"Laura Alves Gonzaga",
      	"Laura Sobral",
      	"Laís Figueiredo",
      	"Leandro Salvador",
      	"Leo Barbosa Reis",
      	"Leo Milano"
      ],
      [
        "Leonardo B.",
      	"Leonardo Yvens Schwarzstein",
      	"Ligia Luz ",
      	"Liliane Moiteiro Caetano",
      	"Lucas Albertine De Godoi",
      	"LUCAS DAIKI",
      	"Lucas dos Santos Tinti",
      	"Lucas Lago",
      	"Lucas Matheus Testa",
      	"Lucas Matias Tei",
      	"Lucas Monteiro de Oliveira",
      	"Lucas Pereira Baumgartner",
      	"Lucas Pretti",
      	"Lucas Rogério de Oliveira",
      	"Lucas Teixeira",
      	"Luciana Masini",
      	"Luciana Minami",
      	"Luhdy Cavalcanti Sardinha",
      	"Luis Arantes",
      	"Luis Fernando Fagundes Rovai",
      	"Luis Otavio Ribeiro",
      	"Luiz Filipe Moresco da Silva",
      	"Luiz Karl",
      	"Luiza Peixe",
      	"Luã Fergus Oliveira da Cruz",
      	"Lívia Dias",
      	"maiana abi",
      	"Maicon Gabriel de Oliveira",
      	"Maranha",
      	"Marcela Cristina Fogaça Vieira",
      	"Marcelo Calheiros",
      	"Marcelo Gimenes de Oliveira",
      	"Marcelo Gonçalves da Cruz",
      	"Marcelo Marquesini",
      	"Marcelo Miky Mine",
      	"Marcio Ferreira de Araujo Junior",
      	"Marcio Ivan",
      	"Marco Antonio de Castro Melo",
      	"Marcos Felipe",
      	"Marcos Flávio de Oliveira"
      ],
      [
        "Marcus Tenório",
      	"Marcus V. Kuquert",
      	"Maria Rita Casagrande",
      	"Maria Rita Guedes Carvalho",
      	"Maria Teresa Aarao",
      	"Mariana Belmont",
      	"Mariana Camargo Simão",
      	"Mariana Jó de Souza",
      	"Marilia Ramos",
      	"marina frota",
      	"Marina Kohler Harkot",
      	"Marina Veloso",
      	"Mario Balan",
      	"Marisa Sanematsu",
      	"mariuza pregnolato",
      	"Marília M Pisani",
      	"Marília Nunes Paulino da Silva",
      	"Mateus Rodrigues Costa",
      	"Matheus Gaboardi Tralli",
      	"Matheus Tadeu Rabelo Querino",
      	"Mayara Ferreira",
      	"Maíra Mendes Galvão",
      	"Mellina Yonashiro",
      	"meskio",
      	"Micah",
      	"Michel Marechal",
      	"Miguel Faggioni Fernandes",
      	"Miguel Vieira",
      	"Mobi Yabiku Neto",
      	"Monalisa Santos",
      	"MUNDANO",
      	"Narrira",
      	"Natan Rodrigues",
      	"Nathalia Sautchuk Patricio",
      	"Natália Satie Odashima",
      	"Nicole Zafalon Kovacs",
      	"Nikos Nikola",
      	"Odélio Porto Jr.",
      	"Ola Bini",
      	"Olavo Silva"
      ],
      [
        "Omayr B. C. Zanata",
      	"Oscar Esgalha",
      	"Pablo Valério Polônia",
      	"Patrícia Sodré",
      	"PAULO ALMEIDA",
      	"Paulo Faltay",
      	"Paulo Henrique de Lima Santana",
      	"Pedro Abrahao Lameirinhas Malina",
      	"Pedro Henrique Braz",
      	"Pedro Kelson Batinga de Mendonça",
      	"Pedro Luiz Santos",
      	"Pedro Markun",
      	"Pedro Pereira",
      	"Pedro Roberto",
      	"Pedro Teles",
      	"Pedro Telles",
      	"Philip Benutte Oliveira Lima",
      	"Philip Sampaio Silva",
      	"Pietro Di Consolo Gregorio",
      	"PIMP MY CARROÇA",
      	"Rafael Bantu",
      	"Rafael Beraldo",
      	"Rafael Chaud Frazão",
      	"Rafael Humberto de Lima Moretti",
      	"Rafael Perazza Mendes",
      	"Rafael Rabelo",
      	"Rafael Santos Sampaio",
      	"rafaela rodrigues rossi",
      	"Raul Luiz",
      	"Regiane Silva",
      	"Renata Aquino Ribeiro",
      	"Renata Assumpção",
      	"Renata Carvalho de Lara Campos",
      	"Renato Contessotto",
      	"Renato Leite Monteiro",
      	"Ricardo Borges Martins",
      	"Ricardo Oliveira",
      	"Ricardo Poppi",
      	"Ricardo Shiota Yasuda"
      ],
      [
        "Roberta Fernandes",
      	"Rodolfo Rodrigues",
      	"Rodrigo Alexandre Costa",
      	"Rodrigo Ardissom de Souza",
      	"Rodrigo José Firmino",
      	"Rodrigo Machado ",
      	"Rodrigo Perez",
      	"Rodrigo Primo",
      	"Rodrigo Vieira Campos",
      	"Rosana Pereira de Miranda",
      	"RTFM",
      	"Samantha Rosa da Rosa",
      	"Samuel Reghim Silva",
      	"Sarah Nery",
      	"Sergio Amadeu da Silveira",
      	"Sergio da Silva",
      	"Sergio Leandro do Nascimento Garcia",
      	"Silas Batista",
      	"Silas Justiniano Veiga da Silva",
      	"Stefanie Melo",
      	"Talita Rodrigues",
      	"Tanous Camara Azzi Melo",
      	"Tarcizio Matheus Zanfolin",
      	"Tatiana Helena Criscione",
      	"Tatiane Bolsonaro Guimarães ",
      	"Thais de Almeida Olmos",
      	"Thais Nascimento Dantas",
      	"Thais Teodoro Perez",
      	"Thamy Matarozzi",
      	"Thaynan Wigor Magalhães de Almeida",
      	"thiago augusto de siqueira",
      	"Thiago Benicchio",
      	"Thiago Carrapatoso",
      	"Thiago Firbida",
      	"Thiago Maia",
      	"Thiago Nacao",
      	"Thiago Ruiz da Silva Matos",
      	"Tiago Kiill",
      	"Tiago P L Rubini",
      	"Tica Moreno"
      ],
      [
        "Tobias Sette Ferreira",
      	"Tuanny Ruiz",
      	"Tulio Malaspina",
      	"Túlio Casagrande Alberto",
      	"Vanessa da Silva",
      	"Vanessa Me Tonini",
      	"Veridiana Alimonti",
      	"Victor Ferreira Silva",
      	"Victor Miguez",
      	"Victor Perin",
      	"Victor Veloso",
      	"Vilma Santos",
      	"Vinicius Barrionuevo",
      	"Vinicius Freitas",
      	"Vinicius guilherme da silva",
      	"Vinicius Rezende Costa",
      	"Vinícius Andrade",
      	"Vinícius de Jesus Smadeski",
      	"Vinícius Felisberto",
      	"Vitor",
      	"Vitor Abud",
      	"Wagner Bertolini Junior",
      	"Wallace Felipe Rodrigues Francisco",
      	"Wandemberg Gibaut",
      	"Wenceslao Machado de Oliveira Jr",
      	"Wesley Mataran",
      	"William Oliveira",
      	"William Saraiva Cimino",
      	"Yoav Passy",
      	"Yorik van Havre",
      	"Yumi Sakate",
      	"Yuri Maia Santos",
      	"Yuri Robert Censi",
      	"Zeesy Powers"
      ]
    ]

contato:
  id: "contato"
  titulo: "Contato"
  subtitulo: "Envie sua sugestão ou dúvida."
  descricao: "Imagem noturna de longa exposição de uma cidade. Um céu sem estrelas ocupa o quinto superior da imagem, e prédios com janelas iluminadas ocupam o espaço restante. Na parte inferior esquerda, parte de uma rua é visível, com os trajetos de faróis de carros visíveis devido à exposição longa."
  button_text: "Enviar"
  formspree_language_code: "pt-BR"
  validacao:
    nome:
      placeholder: "Nome (opcional)"
    email:
      placeholder: "Email *"
      aviso: "Digite seu endereço de e-mail."
    mensagem:
      placeholder: "Sua mensagem *"
      aviso: "Digite a sua mensagem."

footer:
  copyright: "CryptoRave * https://cryptorave.org/ * 2018 | Onion service: http://utw4svtv5ccjastc.onion/"
  antiassedio:
    texto: "Código de Conduta"
    link: "https://we.riseup.net/cryptorave/codigo-de-conduta"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
---
