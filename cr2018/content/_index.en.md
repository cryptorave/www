---
language_code: "en"
title: "CryptoRave 2018"

header:
  descricao:
    "Long exposure image of a city at night, with a street about 100 meters away at a 45 degree downward angle. Trees are visible behind the street, and buildings with their windows lit still further behind. The trajectories of vehicles' headlights are visible due to the long exposure. Superimposed at the center of the image, CR2018's logo in white: at about a 30 degree angle downward, left to right, the letters 'CR' shaped by small zeroes and ones, followed by a processor with a pixelated image of a key in its center, and the text 'UM SPECTRE NOS RONDA' ('A SPECTRE ROAMS ABOUT') in the line below. To the right, written without a slope, the number '2018' divided into two lines, with total height approximately equal to that of the sloped logo and text to the left."

sobre:
  id: "sobre"
  descricao:
    [
      "From May 4 to May 5 2018, in the city of São Paulo, over a period of 36 hours, CryptoRave (CR) hosts activities on security, cryptography, hacking, anonimity, privacy and freedom on the web. This is the **5ᵗʰ edition of the event!**",
      "CryptoRave is inspired on the global and descentralized global action of [CryptoParties](https://cryptoparty.in/), which have as their goal the diffusion of fundamental concepts and basic cryptographic software. Created in 2014, each edition received more than 2500 participants!",
      "Participation in the activities is open, you can subscribe [here](#inscricao). Check out CR2018's schedule [here](#programacao)!"
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "CR2018 logo in white on a black background: to the left, the letters 'CR' formed by tiny zeroes and ones; at the center, an image of a processor with a pixelated key at its center; to the right, the number '2018' written in two lines with a smaller font, so that the total height of both lines is equal to that of the letters 'CR'."
  logo: "crlogo.png"
  sobre: "About"
  financiamento: "Contribute"
  inscricao: "Subscribe"
  keynotes: "Keynotes"
  programacao: "Schedule"
  festa: "Party"
  realizacao: "Partners"
  apoio: "Support"
  patrocinadores: "Patrons"
  contato: "Contact"
  local: "Location"
  menu_extra:
    - name: "CFP"
      url: "https://cpa.cryptorave.org/pt-br/cr2018/cfp/session/new"
      weight: 9"
    - name: "Onion"
      url:  "https://onion.cryptorave.org"
      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Crowdfunding"
  descricao: "CryptoRave 2018's crowdfunding campaign ended successfully! Thank you very much to [all who contributed](#patrocinadores), our event could not happen without your collaboration."
  link_catarse: "https://www.catarse.me/pt/projects/70453/embed"
  titulo_btc: "Contribute with Bitcoin"
  descricao_btc: "We also have a Bitcoin wallet, which you can donate to using the address below! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code for the Bitcoin address 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
  recompensas:
    - linha:
      - valor: "R$15"
        img: ""
        descricao_img: ''
        descricao: "**FREE HUGS** - 'THANK YOU' on the cloud (in our website)"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"

      - valor: "R$25"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOSTICKER** - 2 CRYPTORAVE 2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"

      - valor: "R$40"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCAM** - 4 STICKERS TO COVER YOUR WEBCAM + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"

    - linha:
      - valor: "R$50"
        img: ""
        descricao_img: ""
        descricao: "**SENIORSTICKER** - 4 SPECIAL STICKERS + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"

      - valor: "R$60"
        img: ""
        descricao_img: ""
        descricao: "**MAGYCPEN** - NEW!!! Write with invisible ink. Reveal with blacklight. 1 MAGYC PEN + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"

      - valor: "R$75"
        img: ""
        descricao_img: ""
        descricao: "**MASTERSTICKER** - 4 SPECIAL STICKERS + 4 WEBCAM-COVERING STICKERS + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"

    - linha:
      - valor: "R$120"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTOCUP** - 1 CRYPTORAVE 2018 REUSABLE PLASTIC CUP + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"

      - valor: "R$135"
        img: ""
        descricao_img: ""
        descricao: "**CRYPTORAVER** - 1 SPECIAL SUPPORTER T-SHIRT + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"

      - valor: "R$375"
        img: ""
        descricao_img: ''
        descricao: "**MASTER CRYPTORAVER** - 1 CRYPTORAVE 2018 HOODIE + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"

    - linha:
      - valor: "R$475"
        img: ""
        descricao_img: ''
        descricao: "**HEROIC CRYPTORAVER** - 1 CRYPTORAVE 2018 HOODIE + 1 SPECIAL SUPPORTER T-SHIRT + 2 CR2018 STICKERS + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"

      - valor: "R$700"
        img: ""
        descricao_img: ''
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"
        descricao: "**CRYPTOJEDI** - 1 CRYPTORAVE 2018 HOODIE + 1 SPECIAL SUPPORTER T-SHIRT + 1 CRYPTORAVE 2018 REUSABLE PLASTIC CUP + 4 SPECIAL STICKERS + 4 WEBCAM-COVERING STICKERS + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"

      - valor: "R$1000"
        img: ""
        descricao_img: ''
        descricao: "**CRYPTOJOURNEY** - INFORMATION SECURITY WORKSHOP FOR YOUR COLLECTIVE OR ORGANIZATION + 'thank you' on the cloud"
        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Subscribe"
  descricao: "To participate in the event and to receive updates as preparations progress, subscribe to our newsletter."
  link: "[Click here to subscribe to CryptoRave](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resisting the dystopia of borderless surveillance"
  data: "May 4, Friday, at 8PM"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brazil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Team coordinator of the Tor Project, recently announced as the next Executive Director of the project."
  descricao: "The problem is much larger than Facebook. It is the economic model of the internet: the surveillance model if the internet. Comminucating, sharing and accessing information should not make you a target or a product. You should not be exploited when using the internet."

programacao:
  id: "programacao"
  titulo: "Schedule"
  descricao:
    [
      "Version 1.0 (almost final) of CryptoRave 2018's schedule is available [here](https://cpa.cryptorave.org/pt-BR/cr2018/public/schedule)! This schedule is tentative and subject to changes. The event begins at 7PM on May 4."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'CR2017 logo.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'CR2016 logo.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'CR2015 logo.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'CR2014 logo.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Location"
  descricao:
    [
      "CryptoRave 2018 will be hosted at [Cinemateca Brasileira](http://www.cinemateca.gov.br/)!",
      "Address: Largo Senador Raul Cardoso, 207"
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64816915988922%2C-23.59343726458705%2C-46.64406001567841%2C-23.59066461201592&amp;layer=mapnik&amp;marker=-23.592050945626116%2C-46.64611458778381"
    link: "https://www.openstreetmap.org/way/33968285"
    tooltip: "Click to open in a new tab"

festa:
  id: "festa"
  titulo: "Party at Hangar 110"
  descricao:
    [
      "CryptoRave's party begins on Saturday, May 5, at 8PM at [Hangar 110](http://www.hangar110.com.br/). Entry is free and exclusive to CryptoRave participants!",
      "Address: Rua Rodolfo Miranda, 110 - Bom Retiro (5 minute walk from the Armênia subway station - blue line)"
    ]
  img: "hangar110.jpg"
  link: "http://www.hangar110.com.br/"
  atracoes:
    [
      "20:00 - opening!",
      "**LINEUP**",
      "21:00 - 21:45 - cabeça",
      "22:00 - 23:00 - afrorep",
      "23:00 - 00:00 - datamosh",
      "00:00 - 01:00 - octarina",
      "01:00 - 01:45 - show do retrigger",
      "01:45 - 03:00 - arkanoid",
      "03:00 - 04:15 - swaaag",
      "04:45 - 06:00 - lines",
      "**PROJECTIONS**",
      "21:45 - 23:00 - Terms and conditions may apply",
      "23:30 - 00:45 - Bruno Treviso",
      "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (section)",
      "01:45 - 03:00 - NVVE MVE",
      "03:00 - 04:15 - chr0ma",
      "04:15 - 06:00 - Downloaded"
    ]

realizacao:
  id: "realizacao"
  titulo: "Partners"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Escola de Ativismo's logo: the text 'ESCOLA DE ATIVISMO' in white at the center of a black background rectangle. The rectangle is a bit wider than the text and height approximately four times that of the text."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Actantes's logo: on a white background, the letters 'ACT' in red, followed by the letters 'ANTES' formed by tiny zeroes and ones in black. There is no spacing between the two parts; the logo is a contiguous word."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Saravá's logo: at the center of a black background rectangle, the word 'SARAVÁ' written in white in a font resembling handwriting. The height of the letters varies from about a third to two thirds the height of the rectangle. The text occupies around four fifths of the rectangle's width."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Intervozes's logo, orange over a white background: a square with rounded vertices, overlapped by arcs of circles of various widths; to the right, in two lines, with total height a bit smaller than the sqare's, the following text: 'intervozes' in the first line, and 'coletivo brasil de comunicação social' (Brazil social communication collective)."
        url: "http://intervozes.org.br"
    - linha:
      - img: "encriptatudo.png"
        descricao_img: "Encripta's logo, in white: centered in a black background rectangle, the pixelated image of a key with one extremity surrounded by brackets, to the left. At its right, the text 'encripta'. The image occupies about half of the rectangle's height, while the text occupies about a third. The rectangle's width is a little larger than that of the image followed by the text."
        url: "https://encripta.org"

apoio:
  id: "apoio"
  titulo: "Support"
  parceiros:
    - linha:
      - img: "cinemateca.png"
        descricao_img: "Cinemateca Brasileira's logo: in red, over a transparent background, aligned to the right of the image, two circles positioned vertically with a rectangle in between. The rectangle has width approximately three times the diameter of the circles, and height of about a quarter of the diameter. Below, the text 'cinemateca brasileira' centered and in black."
        url: "http://www.cinemateca.gov.br/"
      - img: "ccc.jpg"
        descricao_img: "Chaos Computer Club (CCC)'s logo': in black over a white background, the outline of an image that resembles a key and also an electronic circuit. From the inside of a rectangle with chamfered corners, 4 lines direct point toward the midpoint of its right side; from there, they align perpendicularly to this side and leave the rectangle, parallel to one another. After a distance of about half the width of the rectangle, the lines twist, forming a knot. Resembling wires, the lines dangle, twisted, below the knot."
        url: "https://ccc.de/"
      - img: "popsp.png"
        descricao_img: "Logo do Ponto de Presença da Rede Nacional de Pesquisa: em azul em um fundo branco, alinhado à esquerda da imagem, um quadrado azul com o contorno do estado de São Paulo e um ramificações eletrônicas. Do lado direito do quadrado azul, as letras PoP-SP ocupam a parte inferior."
        url: "http://www.pop-sp.rnp.br/"
      - img: "rnp.png"
        descricao_img: "Logo da Rede Nacional de Pesquisa: quadrado azul à esquerda com linhas brancas com pontos que se conectam numa formata geométrica não definida. Ao lado direito da imagem, as siglas em azul da organização RNP."
        url: "http://rnp.br"


patrocinadores:
  id: "patrocinadores"
  titulo: "Patrons"
  descricao: "Our most heartfelt thanks to everyone who contributed with our [crowdfunding campaign](#financiamento):"
  doadores:
    [
      [
        "Adriano Ferreira",
        "Afonso Coutinho",
        "Airton Aparecido Zanon",
        "Akari Ueda",
        "Alan Capassi Zara",
        "Alberto Zanella",
        "Alexandra de Paula Yusiasu dos Santos",
        "Alexandre de Carvalho Dias",
        "Alexandre Gomes",
        "Alexandre Okita",
        "Alisson Moro Neocatto",
        "Álvaro Justen",
        "Amanda Rahra ",
        "Amanda Segnini",
        "Ana Carolina Guimarães Marques",
        "Ana Carolina Rezende",
        "Ana Cristina Lima",
        "Ana da Silva de Paula",
        "Ana Facundes",
        "Ana Luiza Portello Bastos",
        "Ana Maria Ghiringhello",
        "ANA SOUZA",
        "Anchises Moraes Guimaraes de Paula",
        "Anderson Campos de Oliveira",
        "Anderson Pereira Leal",
        "Anderson Ramos",
        "Andrei de Mesquita Almeida",
        "Andrius de Camargo",
        "André Andrade Baceti",
        "André Bonfatti",
        "André Figueiredo de Almeida",
        "André Moncaio Afonso",
        "André Pasti",
        "André Rodrigues Ferraz Barbosa",
        "André Scherma Soléo",
        "Angela Gennari",
        "Anna Cruz",
        "Anna Luíza Gannam",
        "Antonela",
      ],
      [
        "Antonio Arles dos Anjos Junior",
        "Ariel Morelli",
        "Arthur Fücher",
        "Aryel Bargas",
        "Augusto Bennemann",
        "Augusto Nery",
        "Beatriz Martins",
        "Beatriz Massaioli Franco dos Reis",
        "Bernardo Carvalho",
        "Bia Bouskela",
        "Bianca Moretto Ribeiro",
        "boklm",
        "brian lima dos santos",
        "Bruna Gonçalves",
        "Bruno Antunes Magrini",
        "Bruno Buys",
        "Bruno Caldas Vianna",
        "Bruno de Vasconcelos Cardoso",
        "Bruno Gama Silva Miranda",
        "Bruno Hannud",
        "brunz",
        "Caio Eduardo Zangirolami Saldanha",
        "Caio Emanuel Rhoden",
        "Caio Henrique Silva Ramos",
        "Caio Teixeira",
        "Cairo Aparecido Campos",
        "Camila Lainetti de Morais",
        "Capi Etheriel",
        "Carla Albertuni",
        "Carla Oliveira Santos",
        "Carlos Cabral",
        "Carlos Hifume",
        "Carol Caires",
        "Carol Caracol",
        "Carol Gutierrez",
        "Carol Pires",
        "Carolina Lemos de Oliveira",
        "Carolina Morandini",
        "Cauê Thiago Ribeiro",
        "Cecília Nunes do Lago Oliveira",
        "Corvolino"
      ],
      [
        "Centro de Autonomia Digital",
        "Cintia Sestelo Campos",
        "Clarissa Pellegrini Braga",
        "Clímax Brasil",
        "Cristiano Lincoln Mattos",
        "Cristina Barreto de Menezes Lopes",
        "César A. Kamiya",
        "Daniel Benvenutti",
        "Daniel Lenharo",
        "Daniel Mendes Cassiano",
        "Daniel Moro Iglesias",
        "Daniel Santini",
        "Daniel Seda",
        "Daniela Floriano Teixeira",
        "Daniela Lerda",
        "Daniela Palumbo",
        "Danilo Mendes",
        "Danilo Restaino",
        "Dann Luciano",
        "Davi Abreu Wasserberg",
        "Davi Amorim",
        "Davi Teofilo",
        "Deborah Lima Happ",
        "Dener Stassun Christinele",
        "Diego Borin Reeberg",
        "Diego de Freitas Aranha",
        "Diego Fernandes Barbosa",
        "Diego Rabatone Oliveira",
        "Diego Rossini Vieira",
        "Divina Vitorino",
        "Douglas Anunciação",
        "Douglas da Silva Costa",
        "Débora Prado ",
        "Edouard Jean Guillaume de Fraipont",
        "Eduardo Dias de Andrade",
        "Eduardo Marcondes",
        "Eduardo Mendes",
        "Elias Tandel Barrionovo",
        "Elisabete Santana",
        "Elisa X",
        "Emerson Marques Pedro"
      ],
      [
        "Emmanuel G Brandão",
        "Enzzo Cavallo",
        "Eric Ribeiro",
        "Erick Muller",
        "Erik Teixeira Gonçalves Rodrigues",
        "Escola de Ativismo",
        "Evandro Ap Dos Santos",
        "Evelize Pacheco Simões",
        "Evelyn Gomes ",
        "Ewerton Queiroz",
        "fabianne batista balvedi",
        "Fabio Diniz",
        "Fabricio Biazzotto",
        "Fatima Conti",
        "Felipe Celeri",
        "Felipe Correa",
        "Felipe da Silva Souza",
        "Felipe Duarte Domingues",
        "Felipe Pait",
        "felippe santos",
        "Fernanda Beirao",
        "Fernanda Bruno",
        "Fernanda Campagnucci",
        "Fernanda Fantelli",
        "Fernanda Monteiro",
        "Fernanda Shirakawa",
        "Fernanda Tosta",
        "Fernando Boaglio",
        "Fernando Lobato",
        "Fernando Paiva",
        "Filipe Meirelles",
        "Flavia Gannam",
        "Flora Cytrynowicz",
        "Florence Poznanski",
        "Flávia Lefèvre Guimarães",
        "Flávio Amieiro",
        "Flávio Sousa",
        "Francele Cocco",
        "Francine Emilia Costa",
        "Francisco Borges Fernandes Júnior"
      ],
      [
        "Francisco dos Santos Ekman Simões",
        "Francisco Haroldo Barbosa da Silva",
        "Francisco Namias Vicente",
        "Gabi Juns",
        "GABRIEL ALEJANDRO DA SILVA MARQUEZ ME EPP",
        "Gabriel Andrade",
        "Gabriel Becker",
        "Gabriel Borges",
        "Gabriel Caropreso",
        "Gabriel D'Ambrosio",
        "Gabriel Fanelli",
        "Gabriel Gortan",
        "Gabriel Lindenbach",
        "Gabriel Morais",
        "Gabriel S.",
        "Gabriel Torres Gomes Pato",
        "gabriel vituri",
        "Gabriela Dias Lopes",
        "Gabriela Gannam",
        "Gabriela Guerra",
        "Gabriela Pompermayer",
        "Gabriella De Biaggi",
        "Georgia Nicolau",
        "Geraldo Aleandro",
        "Geraldo Barros",
        "Gibran Sirena",
        "Giovane Santos Liberato",
        "Giovani Ferreira",
        "Giovanni Leoni Brito",
        "Gisele Kauer",
        "Graciela Natansohn",
        "Guilherme Augusto",
        "Guilherme Bigois",
        "Guilherme Botelho Diniz Junqueira",
        "Guilherme Gondim",
        "Guilherme Rodrigues Bueno",
        "Guilherme Xavier Silva",
        "Gustavo Avila",
        "Gustavo Felipe Vieira de Alencar",
        "gustavo gus"
      ],
      [
        "Gustavo Padovan",
        "Gustavo Pereira Dutra",
        "GUSTAVO ROBERTO RODRIGUES GONÇALVES",
        "Gut Simon",
        "HacKan",
        "Hanna F. Mariano",
        "Hans Zwolsman",
        "Haydee Svab",
        "Helder Betiol",
        "Henrique Bispo Dos Santos",
        "Henrique Bronzoni",
        "henrique dos reis miguel",
        "Henrique Fernanndes",
        "Henrique Machado Gonçalves",
        "Henrique Parra",
        "Henrique Specian",
        "Hiro",
        "Hitalo Cesar Alves",
        "Ian Fernandez",
        "Ian Thomaz Puech",
        "Ieremies Vieira da Fonseca Romero",
        "Igor Frederick Cabral Ferreira da Silva",
        "Igor Santos",
        "Igor Schwab Hoyer",
        "inae batistoni e silva",
        "Ingrid Elisabeth Spangler",
        "Instituto Alana ",
        "Intervozes",
        "isabela dias fernandes",
        "Ivan Korkischko",
        "Ivo Nascimento",
        "Ivone Rocha",
        "Izabel Marques Meo",
        "Jamila Venturini",
        "Janaina Centini",
        "Janaina Menegaz Spode",
        "Jean Paul Ruiz Depraz",
        "Jean Tible",
        "Jefferson Souza",
        "JENIFFER BRITO JOHANSEN"
      ],
      [
        "Jeronimo Cordoni Pellegrini",
        "Jessé Rodrigues de Souza",
        "joao moreno rodrigues falcao",
        "Joao Rafael Bonilha",
        "Jonas Lima de Amorim",
        "Jonaya de Castro Garbe",
        "JOSE ALONSO BALBI",
        "Joselia Leite Costa",
        "José",
        "Joyce Ariane de Souza",
        "João Cassino",
        "João Corrêa",
        "João Phillipe Cardenuto",
        "João Victor Maruca",
        "Julee D'Addio",
        "Julia Gimenes C Ferreira",
        "Julia Issa",
        "Juliana Amoasei dos Reis",
        "Juliana Felicidade Armede",
        "Juliana Ito",
        "Juliana Myaki Bueno",
        "julio antonio mendonca de marins",
        "Julio Monteiro",
        "Julio Pancracio Valim",
        "Jussara Ribeiro de Oliveira",
        "Júlia Carmona",
        "Júlio Cortijo",
        "Kevin Martins",
        "Laila Dell'Antonia Scarassati",
        "Lais Mastelari",
        "Laisa Beatris Silva Pereira",
        "Larissa Dominique",
        "Larissa Marques",
        "Larissa Pinho Alves",
        "Laura Alves Gonzaga",
        "Laura Sobral",
        "Laís Figueiredo",
        "Leandro Salvador",
        "Leo Barbosa Reis",
        "Leo Milano"
      ],
      [
        "Leonardo B.",
        "Leonardo Yvens Schwarzstein",
        "Ligia Luz ",
        "Liliane Moiteiro Caetano",
        "Lucas Albertine De Godoi",
        "LUCAS DAIKI",
        "Lucas dos Santos Tinti",
        "Lucas Lago",
        "Lucas Matheus Testa",
        "Lucas Matias Tei",
        "Lucas Monteiro de Oliveira",
        "Lucas Pereira Baumgartner",
        "Lucas Pretti",
        "Lucas Rogério de Oliveira",
        "Lucas Teixeira",
        "Luciana Masini",
        "Luciana Minami",
        "Luhdy Cavalcanti Sardinha",
        "Luis Arantes",
        "Luis Fernando Fagundes Rovai",
        "Luis Otavio Ribeiro",
        "Luiz Filipe Moresco da Silva",
        "Luiz Karl",
        "Luiza Peixe",
        "Luã Fergus Oliveira da Cruz",
        "Lívia Dias",
        "maiana abi",
        "Maicon Gabriel de Oliveira",
        "Maranha",
        "Marcela Cristina Fogaça Vieira",
        "Marcelo Calheiros",
        "Marcelo Gimenes de Oliveira",
        "Marcelo Gonçalves da Cruz",
        "Marcelo Marquesini",
        "Marcelo Miky Mine",
        "Marcio Ferreira de Araujo Junior",
        "Marcio Ivan",
        "Marco Antonio de Castro Melo",
        "Marcos Felipe",
        "Marcos Flávio de Oliveira"
      ],
      [
        "Marcus Tenório",
        "Marcus V. Kuquert",
        "Maria Rita Casagrande",
        "Maria Rita Guedes Carvalho",
        "Maria Teresa Aarao",
        "Mariana Belmont",
        "Mariana Camargo Simão",
        "Mariana Jó de Souza",
        "Marilia Ramos",
        "marina frota",
        "Marina Kohler Harkot",
        "Marina Veloso",
        "Mario Balan",
        "Marisa Sanematsu",
        "mariuza pregnolato",
        "Marília M Pisani",
        "Marília Nunes Paulino da Silva",
        "Mateus Rodrigues Costa",
        "Matheus Gaboardi Tralli",
        "Matheus Tadeu Rabelo Querino",
        "Mayara Ferreira",
        "Maíra Mendes Galvão",
        "Mellina Yonashiro",
        "meskio",
        "Micah",
        "Michel Marechal",
        "Miguel Faggioni Fernandes",
        "Miguel Vieira",
        "Mobi Yabiku Neto",
        "Monalisa Santos",
        "MUNDANO",
        "Narrira",
        "Natan Rodrigues",
        "Nathalia Sautchuk Patricio",
        "Natália Satie Odashima",
        "Nicole Zafalon Kovacs",
        "Nikos Nikola",
        "Odélio Porto Jr.",
        "Ola Bini",
        "Olavo Silva"
      ],
      [
        "Omayr B. C. Zanata",
        "Oscar Esgalha",
        "Pablo Valério Polônia",
        "Patrícia Sodré",
        "PAULO ALMEIDA",
        "Paulo Faltay",
        "Paulo Henrique de Lima Santana",
        "Pedro Abrahao Lameirinhas Malina",
        "Pedro Henrique Braz",
        "Pedro Kelson Batinga de Mendonça",
        "Pedro Luiz Santos",
        "Pedro Markun",
        "Pedro Pereira",
        "Pedro Roberto",
        "Pedro Teles",
        "Pedro Telles",
        "Philip Benutte Oliveira Lima",
        "Philip Sampaio Silva",
        "Pietro Di Consolo Gregorio",
        "PIMP MY CARROÇA",
        "Rafael Bantu",
        "Rafael Beraldo",
        "Rafael Chaud Frazão",
        "Rafael Humberto de Lima Moretti",
        "Rafael Perazza Mendes",
        "Rafael Rabelo",
        "Rafael Santos Sampaio",
        "rafaela rodrigues rossi",
        "Raul Luiz",
        "Regiane Silva",
        "Renata Aquino Ribeiro",
        "Renata Assumpção",
        "Renata Carvalho de Lara Campos",
        "Renato Contessotto",
        "Renato Leite Monteiro",
        "Ricardo Borges Martins",
        "Ricardo Oliveira",
        "Ricardo Poppi",
        "Ricardo Shiota Yasuda"
      ],
      [
        "Roberta Fernandes",
        "Rodolfo Rodrigues",
        "Rodrigo Alexandre Costa",
        "Rodrigo Ardissom de Souza",
        "Rodrigo José Firmino",
        "Rodrigo Machado ",
        "Rodrigo Perez",
        "Rodrigo Primo",
        "Rodrigo Vieira Campos",
        "Rosana Pereira de Miranda",
        "RTFM",
        "Samantha Rosa da Rosa",
        "Samuel Reghim Silva",
        "Sarah Nery",
        "Sergio Amadeu da Silveira",
        "Sergio da Silva",
        "Sergio Leandro do Nascimento Garcia",
        "Silas Batista",
        "Silas Justiniano Veiga da Silva",
        "Stefanie Melo",
        "Talita Rodrigues",
        "Tanous Camara Azzi Melo",
        "Tarcizio Matheus Zanfolin",
        "Tatiana Helena Criscione",
        "Tatiane Bolsonaro Guimarães ",
        "Thais de Almeida Olmos",
        "Thais Nascimento Dantas",
        "Thais Teodoro Perez",
        "Thamy Matarozzi",
        "Thaynan Wigor Magalhães de Almeida",
        "thiago augusto de siqueira",
        "Thiago Benicchio",
        "Thiago Carrapatoso",
        "Thiago Firbida",
        "Thiago Maia",
        "Thiago Nacao",
        "Thiago Ruiz da Silva Matos",
        "Tiago Kiill",
        "Tiago P L Rubini",
        "Tica Moreno"
      ],
      [
        "Tobias Sette Ferreira",
        "Tuanny Ruiz",
        "Tulio Malaspina",
        "Túlio Casagrande Alberto",
        "Vanessa da Silva",
        "Vanessa Me Tonini",
        "Veridiana Alimonti",
        "Victor Ferreira Silva",
        "Victor Miguez",
        "Victor Perin",
        "Victor Veloso",
        "Vilma Santos",
        "Vinicius Barrionuevo",
        "Vinicius Freitas",
        "Vinicius guilherme da silva",
        "Vinicius Rezende Costa",
        "Vinícius Andrade",
        "Vinícius de Jesus Smadeski",
        "Vinícius Felisberto",
        "Vitor",
        "Vitor Abud",
        "Wagner Bertolini Junior",
        "Wallace Felipe Rodrigues Francisco",
        "Wandemberg Gibaut",
        "Wenceslao Machado de Oliveira Jr",
        "Wesley Mataran",
        "William Oliveira",
        "William Saraiva Cimino",
        "Yoav Passy",
        "Yorik van Havre",
        "Yumi Sakate",
        "Yuri Maia Santos",
        "Yuri Robert Censi",
        "Zeesy Powers"
      ]
    ]

contato:
  id: "contato"
  titulo: "Contact"
  subtitulo: "Send your questions and suggestions."
  descricao: "Long exposure image of a city at night. A starless sky occupies the upper fifth of the image, while buildings with lit windows occupy the remaining space. In the left lower half of the image, part of a street is visible, with the trajectories of headlights visible due to the long exposure."
  button_text: "Send"
  formspree_language_code: "en"
  validacao:
    nome:
      placeholder: "Name (optional)"
    email:
      placeholder: "Email *"
      aviso: "Enter your email address."
    mensagem:
      placeholder: "Your message *"
      aviso: "Type your message here."

footer:
  copyright: "CryptoRave * https://cryptorave.org/ * 2018 | Onion service: http://utw4svtv5ccjastc.onion/"
  antiassedio:
    texto: "Code of Conduct"
    link: "https://we.riseup.net/cryptorave/codigo-de-conduta"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
---
