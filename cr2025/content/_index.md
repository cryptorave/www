---
language_code: "pt-br"
title: "CryptoRave 2025"

header:
  descricao:
    "CryptoRave 2025"
sobre:
  id: "sobre"
  descricao:
    [
      "A **CryptoRave** é um evento anual que reúne, em 24 horas (+ festa a ser divulgada), diversas atividades sobre segurança, criptografia, hacking, anonimato, privacidade e liberdade na rede ",
      "A CryptoRave é **aberta e gratuita** e realizada na cidade de São Paulo, as inscrições serão feitas no local.",
      "Inspirada em uma [ação global](https://cryptoparty.in), descentralizada para disseminar e democratizar o conhecimento e conceitos básicos de criptografia e software livre, o evento teve início em 2014, como reação à divulgação de informações que confirmaram a ação de governos e corporações para manter a população mundial sob vigilância e monitoramento constante. Junte-se a nós!" 
    ]
  vimeo_link: "https://www.youtube.com/embed/Ow8CEj665xM"

nav:
  descricao:
    "Logo da CryptoRave 2025: a palavra 'Cryptorave' escrita na parte superior com o desenho de uma chave em cima da letra 'O'. Ao lado direito uma sequência de zeros e ums. Abaixo da palavra 'Cryptorave' há um olho com folhas saíndo de trás. O logo todo está na cor verde limão."
  logo: "crlogo.webp"
  sobre: "Sobre"
  financiamento: "Financiamento"
  inscricao: "Atualizações"
  programacao: "Programação"
  divulgacao: "Divulgação"
  realizacao: "Realização"
  apoio: "Apoio"
  patrocinadores: "Patrocinadores"
  contato: "Contato"
  local: "Local"
  menu_extra:
#    - name: "Grade"
#      url: "https://cpa.cryptorave.org/pt-BR/cr2019/public/schedule"
#      weight: 9"
#    - name:  "Blog"
#      url:  "https://blog.cryptorave.org"
#      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamento Coletivo"
  descricao: |
    A Cryptorave é realizada com apoio financeiro e de divulgação de uma comunidade distribuída, anônima, mas que garante, ano após ano, que mais pessoas participem do evento.\
    \
    Apoie a #CR25 através do [catarse.me/cryptorave2025](https://www.catarse.me/cryptorave2025)
  link_catarse: "https://www.catarse.me/pt/projects/185807/embed"
  titulo_btc: "Colabore em Bitcoin"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para o endereço Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."

inscricao:
  id: "inscricao"
  titulo: "Inscrições"
  # descricao_2024_v1: "Estamos com inscrições abertas para voluntários que querem ajudar no dia com a organização do evento. Inscreva-se no [formulário](https://dandara.vedetas.org/index.php/678642?lang=pt-BR)."
  # link_boletim: ""
  # descricao_2024_v2: "Estamos com as inscrições abertas para atividades. Quer apresentar algo na edição deste ano, se inscreva no [formulário](https://dandara.vedetas.org/index.php/173617?lang=pt-br)."
  # link_boletim_2024: "Também estamos com as inscrições aberta para voluntários que querem ajudar no dia com a organização do evento, se inscreva no [formulário](https://dandara.vedetas.org/index.php/678642?lang=pt-BR)."
  descricao: "Chamada de atividades: Estamos com as inscrições abertas para atividades. Quer apresentar algo na edição deste ano, se inscreva no [formulário](https://cpa.cryptorave.org/cryptorave-2025/cfp)."
  link_boletim: "Para acompanhar as últimas novidades sobre a Cryptorave, [inscreva-se para receber o boletim do evento](https://lists.riseup.net/www/subscribe/cryptorave-boletim)." 

divulgacao:
  id: divulgacao
  titulo: "Divulgação"
  descricao: "A divulgação da CryptoRave é feita por todos nós! Espalhe a palavra nas redes sociais"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: ""
  nome: "[]()"
  bio: "."
  descricao: "."


programacao:
  id: "programacao"
  titulo: "Programação"
  img: "programacao.png"
  descricao:
    [
      "A versão 2.0 da programação da CryptoRave 2025 já está [disponível](https://we.riseup.net/cryptorave/cr2024-programacao)!",
      "Acesse diretamente a versão completa ([colorida](https://we.riseup.net/assets/928538/programacao-cr2024-v2.0.pdf), [preto e branco](https://we.riseup.net/assets/928539/programacao-cr2024-v2.0+PB.pdf), e [versão para impressão](https://we.riseup.net/assets/929393/programacao-cr2024-v2.0+impressaoA4.pdf))."
    ]
#    [
#      "A versão 0.1 da programação da CryptoRave 2020 já está disponível: [agenda.cryptorave.org](https://agenda.cryptorave.org)!", 
#      "A CR2020 começará às 19 horas do dia 15 de maio de 2020 e vai até às 22h do dia 16 de maio de 2020.",
#      "Baixe o APP da programação para [Android](https://play.google.com/store/apps/details?id=org.cryptorave.schedule&hl=en_US)."
#    ]
#    [ 
#      "O evento acontecerá nos dias 10 e 11 de maio."
#    ]

#  anteriores:
#    - linha:
#      - titulo: "CR2019"
#        img: "cr2019logo.png"
#        descricao_img: 'Logo da CR19.'
#        url: "https://2019.cryptorave.org"
#    - linha:
#      - titulo: "CR2018"
#        img: "cr2018logo.png"
#        descricao_img: 'Logo da CR18.'
#        url: "https://2018.cryptorave.org"
#    - linha:
#      - titulo: "CR2017"
#        img: "cr2017logo.png"
#        descricao_img: 'Logo da CR2017.'
#        url: "https://2017.cryptorave.org"    
#      - titulo: "CR2016"
#        img: "cr2016logo.png"
#        descricao_img: 'Logo da CR2016.'
#        url: "https://2016.cryptorave.org"
#    - linha:
#      - titulo: "CR2015"
#        img: "cr2015logo.png"
#        descricao_img: 'Logo da CR2015.'
#        url: "https://2015.cryptorave.org"
#      - titulo: "CR2014"
#        img: "cr2014logo.png"
#        descricao_img: 'Logo da CR2014.'
#        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Local"
  descricao:
    [
      "A CryptoRave 2025 será realizada na [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "R. da Consolação, 94, São Paulo - SP. Ao lado da estação Anhangabaú do metrô."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64421558380128%2C-23.548517016003938%2C-46.64067506790161%2C-23.54628437009063&amp;layer=mapnik"
    link: "https://osm.org/go/M~ziJkQVh?way=48661210"
    tooltip: "Clique para abrir em outra aba"

realizacao:
  id: "realizacao"
  titulo: "Realização"
  parceiros:
    - linha:
      - img: "actantes.png"
        descricao_img: "Logo da Actantes: com fundo branco, as letras 'ACT' em vermelho, e, em seguida, as letras 'ANTES' formadas pelos números 0 e 1 em preto. Não há espaçamento entre as duas partes; o logo é uma palavra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo do Saravá: ao centro de um fundo retangular preto, a palavra 'SARAVÁ' em branco em uma fonte que se assemelha a texto manuscrito. A altura das letras varia entre cerca de um terço e dois terços da altura do retângulo. Horizontalmente, o texto ocupa cerca de quatro quintos do comprimento do retângulo."
        url: "https://sarava.org"
      - img: "marialab.png"
        descricao_img: "Logo do MariaLab com fundo branco: as palavras 'maria' em vermelho, logo abaixo 'lab' em preto entre colchetes."
        url: "https://www.marialab.org/"

apoio:
  id: "apoio"
  titulo: "Apoio"
  parceiros:
    - linha:
      # - img: "boll.png"
      #   descricao_img: "Logo da Fundação Heinrich Böll: três retângulos compondo um degradê de verde  seguidos, ao lado o nome 'Heinrich Böll Stiftung' escrito em preto, abaixo escrito 'Rio de janeiro' em verde, abaixo 'Brasil' escrito em verde."
      #   url: "https://br.boell.org/"
      - img: "ccc.jpg"
        descricao_img: "Logo do Chaos Computer Club (CCC): em preto em um fundo branco, os contornos de uma imagem que se assemelha a uma imagem e a um circuito eletrônico. De dentro de um retângulo com os cantos chanfrados, 4 linhas se direcionam ao centro do lado direito; de lá, se alinham perpendicularmente a este lado e saem do retângulo, paralelas entre si. Após uma distância de cerca de metade do comprimento do retângulo, as linhas se torcem, formando um nó. Se assemelhando a fios, as linhas pendem torcidas abaixo do nó."
        url: "https://ccc.de/"
    - linha:
      - img: "bma.png"
        descricao_img: "Logo da Biblioteca Mario de Andrade: a inscrição Biblioteca Mário de Andrade escrita em fonte maiúscula preta sobre um fundo branco, dividida em 5 linhas, formatada em um retângulo que se assemelha a uma capa de livro."
        url: "https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/"
      - img: "sec-cultura-sp.png"
        descricao_img: "Logo da Secretaria de cultura: Na parte superior apresenta o brasão da cidade de são paulo, abaixo está o nome CIDADE DE SÃO PAULO e na parte inferior CULTURA E ECONOMIA CRIATIVA"
        url: "https://capital.sp.gov.br/web/cultura/"
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo da Escola de Ativismo: o texto 'ESCOLA DE ATIVISMO' em branco no centro de um retângulo preto. O retângulo tem largura pouco maior que a do texto e altura aproximadamente quatro vezes a altura do texto."
        url: "https://ativismo.org.br"

patrocinadores:
  id: "patrocinadores"
  titulo: "Patrocinadores"
  descricao: "Agradecemos de coração a todas e todos que contribuíram com a nossa campanha de [financiamento coletivo](#financiamento):"
  doadores: 
    [
[
"Edward Snowden",
"Quer ver seu nome aqui?",
"Contribua na campanha do catarse"
],
     ] 

contato:
  id: "contato"
  titulo: "Contato"
  subtitulo: "Envie sua sugestão ou dúvida."
  descricao: "Imagem noturna de longa exposição de uma cidade. Um céu sem estrelas ocupa o quinto superior da imagem, e prédios com janelas iluminadas ocupam o espaço restante. Na parte inferior esquerda, parte de uma rua é visível, com os trajetos de faróis de carros visíveis devido à exposição longa."
  button_text: "Enviar"
  formspree_language_code: "pt-BR"
  validacao:
    nome:
      placeholder: "Nome (opcional)"
    email:
      placeholder: "Email *"
      aviso: "Digite seu endereço de e-mail."
    mensagem:
      placeholder: "Sua mensagem *"
      aviso: "Digite a sua mensagem."

footer:
  # copyright: "CryptoRave.ORG 2025 - Sem privacidade não há liberdade - [Onion services](https://onion.cryptorave.org/)"
  copyright: "CryptoRave.org 2025 - Sem privacidade não há liberdade"
  antiassedio:
    texto: "Código de Conduta"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-instagram"
      link: "https://www.instagram.com/cryptoravebr/"
    - icon: "fa-maxcdn"
      link: "https://mastodon.social/@cryptorave"
    - icon: "fa-telegram"
      link: "https://t.me/CryptoRave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    # - icon: "fa-ticket"
    #   link: "https://tickets.cryptorave.org/2020/free"
---
