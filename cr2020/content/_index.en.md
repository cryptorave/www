---
language_code: "en"
title: "CryptoRave"

header:
  descricao:
    ""
sobre:
  id: "sobre"
  descricao:
    [
      "The CryptoRave is an annual event that brings together, in 36 hours, various activities on security, encryption, hacking, anonymity, privacy and network freedom.",
      "CryptoRave is **open and free** - with just an [online registration](#inscricao) - and have been held in [São Paulo, Brazil](#local). It's inspired by the decentralized [global action](https://cryptoparty.in) to disseminate and democratize knowledge and basic concepts of cryptography and free software. The event began in 2014 as a reaction to the disclosure of information that confirmed the action governments to keep the world population under constant surveillance and monitoring."
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "CR2020 logo in white on a black background: to the left, the letters 'CR' formed by tiny zeroes and ones; at the center, an image of a processor with a pixelated key at its center; to the right, the number '2019' written in two lines with a smaller font, so that the total height of both lines is equal to that of the letters 'CR'."
  logo: "crlogo.png"
  sobre: "About"
  financiamento: "Contribute"
  inscricao: "Attend"
  keynotes: "Keynotes"
  programacao: "Schedule"
  festa: "Party"
  realizacao: "Partners"
  apoio: "Support"
  patrocinadores: "Backers"
  contato: "Contact"
  local: "Venue"
  menu_extra:
#    - name: "CFP"
#      url: "https://cpa.cryptorave.org/pt-br/cr2019/cfp/session/new"
#      weight: 9"
#    - name: "Onion"
#      url:  "https://onion.cryptorave.org"
#      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Crowdfunding"
  descricao: "The CryptoRave is made possible through the crowdfunding of an anonymous and distributed community. Is this community that makes possible that each year more and more people can participate in the event."
  link_catarse: "https://www.catarse.me/pt/projects/109977/embed"
  titulo_btc: "Contribute with Bitcoin"
  descricao_btc: "We also have a Bitcoin wallet, which you can donate to using the address below! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code for the Bitcoin address 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
#  recompensas:
#    - linha:
#      - valor: "R$15"
#        img: ""
#        descricao_img: ''
#        descricao: "**FREE HUGS** - 'THANK YOU' on the cloud (in our website)"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"
#
#      - valor: "R$25"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTOSTICKER** - 2 CRYPTORAVE 2019 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"
#
#      - valor: "R$40"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTOCAM** - 4 STICKERS TO COVER YOUR WEBCAM + 1 CR2019 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"
#
#    - linha:
#      - valor: "R$50"
#        img: ""
#        descricao_img: ""
#        descricao: "**SENIORSTICKER** - 4 SPECIAL STICKERS + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"
#
#      - valor: "R$60"
#        img: ""
#        descricao_img: ""
#        descricao: "**MAGYCPEN** - NEW!!! Write with invisible ink. Reveal with blacklight. 1 MAGYC PEN + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"
#
#      - valor: "R$75"
#        img: ""
#        descricao_img: ""
#        descricao: "**MASTERSTICKER** - 4 SPECIAL STICKERS + 4 WEBCAM-COVERING STICKERS + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"
#
#    - linha:
#      - valor: "R$120"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTOCUP** - 1 CRYPTORAVE 2018 REUSABLE PLASTIC CUP + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"
#
#      - valor: "R$135"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTORAVER** - 1 SPECIAL SUPPORTER T-SHIRT + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"
#
#      - valor: "R$375"
#        img: ""
#        descricao_img: ''
#        descricao: "**MASTER CRYPTORAVER** - 1 CRYPTORAVE 2018 HOODIE + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"
#
#    - linha:
#      - valor: "R$475"
#        img: ""
#        descricao_img: ''
#        descricao: "**HEROIC CRYPTORAVER** - 1 CRYPTORAVE 2018 HOODIE + 1 SPECIAL SUPPORTER T-SHIRT + 2 CR2018 STICKERS + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"
#
#      - valor: "R$700"
#        img: ""
#        descricao_img: ''
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"
#        descricao: "**CRYPTOJEDI** - 1 CRYPTORAVE 2018 HOODIE + 1 SPECIAL SUPPORTER T-SHIRT + 1 CRYPTORAVE 2018 REUSABLE PLASTIC CUP + 4 SPECIAL STICKERS + 4 WEBCAM-COVERING STICKERS + 1 CR2018 BACKPACK PIN + 2 CR2018 STICKERS + 'thank you' on the cloud"
#
#      - valor: "R$1000"
#        img: ""
#        descricao_img: ''
#        descricao: "**CRYPTOJOURNEY** - INFORMATION SECURITY WORKSHOP FOR YOUR COLLECTIVE OR ORGANIZATION + 'thank you' on the cloud"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Attend"
  descricao: "The registration and participation in the CryptoRave is completely free. You must present the registration ticket on the day of the event. The registration will open soon!"
  link_ticket: ""
  descricao_boletim: "Get the latest news about the event and the activities,"
  link_boletim: "[subscribe to CryptoRave newsletter](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resisting the dystopia of borderless surveillance"
  data: "May 4, Friday, at 8PM"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brazil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Team coordinator of the Tor Project, recently announced as the next Executive Director of the project."
  descricao: "The problem is much larger than Facebook. It is the economic model of the internet: the surveillance model if the internet. Comminucating, sharing and accessing information should not make you a target or a product. You should not be exploited when using the internet."

programacao:
  id: "programacao"
  titulo: "Schedule"
  descricao:
    [
      "CryptoRave schedule is available in [agenda.cryptorave.org](https://agenda.cryptorave.org/)! The schedule is tentative and subject to changes. The event starts at 7PM on May 15th."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'CR2017 logo.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'CR2016 logo.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'CR2015 logo.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'CR2014 logo.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Venue"
  descricao:
    [
      "CryptoRave 2019 will be hosted at [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/)!",
      "Address: Rua da Consolação, 94 - República, São Paulo. Close to subway station Anhangabaú."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64346992969513%2C-23.548138354058587%2C-46.64142608642579%2C-23.546663037375726&amp;layer=mapnik&amp;marker=-23.547400697786546%2C-46.642448008060455"
    link: "https://osm.org/go/M~ziJkQVL?way=48661210"
    tooltip: "Click to open in a new tab"


festa:
  id: "festa"
  titulo: "Party"
  descricao:
    [
      "CryptoRave's after party begins on Saturday, May 4, at 11PM at [Trackers](http://www.trackers.cx/). The entrance is free for CryptoRave 2019 [supporters](#patrocinadores)!",
      "Address: Avenida São João,  (10 minutes walk from Biblioteca Mário de Andrade)"
    ]
  img: "hangar110.jpg"
  link: "http://www.trackers.cx/"

realizacao:
  id: "realizacao"
  titulo: "Partners"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Escola de Ativismo's logo: the text 'ESCOLA DE ATIVISMO' in white at the center of a black background rectangle. The rectangle is a bit wider than the text and height approximately four times that of the text."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Actantes's logo: on a white background, the letters 'ACT' in red, followed by the letters 'ANTES' formed by tiny zeroes and ones in black. There is no spacing between the two parts; the logo is a contiguous word."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Saravá's logo: at the center of a black background rectangle, the word 'SARAVÁ' written in white in a font resembling handwriting. The height of the letters varies from about a third to two thirds the height of the rectangle. The text occupies around four fifths of the rectangle's width."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Intervozes's logo, orange over a white background: a square with rounded vertices, overlapped by arcs of circles of various widths; to the right, in two lines, with total height a bit smaller than the sqare's, the following text: 'intervozes' in the first line, and 'coletivo brasil de comunicação social' (Brazil social communication collective)."
        url: "http://intervozes.org.br"
    - linha:

apoio:
  id: "apoio"
  titulo: "Support"
  parceiros:
    - linha:
      - img: "ccc.jpg"
        descricao_img: "Chaos Computer Club (CCC)'s logo': in black over a white background, the outline of an image that resembles a key and also an electronic circuit. From the inside of a rectangle with chamfered corners, 4 lines direct point toward the midpoint of its right side; from there, they align perpendicularly to this side and leave the rectangle, parallel to one another. After a distance of about half the width of the rectangle, the lines twist, forming a knot. Resembling wires, the lines dangle, twisted, below the knot."
        url: "https://ccc.de/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Backers"
  descricao: "We sincerely thank everyone who contributed to our [crowdfunding campaign](#financiamento):"
  doadores:
    [
[
"Adriana Leticya Gontijo",
"Alessio Esteves",
"Alexandre Isaac Siqueira",
"Alexandre Medeiros",
"Alice Christina Matsuo",
"Alice Lana",
"Aline Freitas",
"Ana Elisa",
"Ana Luiza Portello Bastos",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson C",
"Anderson de Jesus Nascimento Ribeiro",
"Anderson Pereira Leal",
"André F. Viana",
"André Luiz ",
"André Rodrigues Ferraz Barbosa",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Anon",
"Antonio Arles dos Anjos Junior",
"Ariel Ferreira Rodrigues",
"Arthur Costa Lima",
"Augusto Bennemann",
"Bruno Kim Medeiros Cesar",
"Bruno Martins",
"BRUNO PAIVA DE OLIVEIRA",
"brunz",
"Cadós Sanchez",
"caioau",
"Caio Eduardo Zangirolami Saldanha",
"Caio Fontes de Castro",
"Caio Henrique Silva Ramos",
"Carla Oliveira Santos",
"Carlos Cabral",
"Carlos S",
"Carolina Vergotti Ferrigno",
"Coalinha das Planilhas",
"cybelle",
"Daniel Miranda Birochi",
"Daniel Rondinelli Roquetti"
],
[
"Dara Gonçalves",
"Diana Iliescu",
"Divina Vitorino",
"Douglas da Silva Costa",
"Douglas Vinicius Esteves",
"Editora Monstro dos Mares",
"Eduardo Costa Lisboa",
"Elisa X",
"Emilio Simoni",
"Enzzo Pessanha Cavallo",
"Estanislau Gonçalves",
"Euler Neto",
"Evelize Pacheco Simões",
"Fabio Barros",
"Fábio Meneghetti",
"Felipe",
"Fernanda Campagnucci",
"Fernanda Fantelli",
"Fernanda Shirakawa",
"Fernao Vellozo",
"Filipe Monguilhott Falcone",
"Francine Emilia Costa",
"Gabriela Bittencourt",
"Gabriel Gortan",
"Gabriella De Biaggi",
"glauber noccioli de souza",
"Guilherme Otero",
"Guilherme Ribeiro de Lima",
"Gustavo Felipe Vieira de Alencar",
"Gustavo Gus",
"Gustavo Suto",
"Gyssele Mendes",
"Henrique Nascimento Santos",
"Ian Fernandez",
"Ian Fernandez",
"Ines Aisengart Menezes",
"Ingrid Elisabeth Spangler",
"Ingrid Elisabeth Spangler",
"Itamar Silva",
"Janaina Menegaz Spode",
"João Moreno Rodrigues Falcão",
"Joao Rafael Bonilha"
],
[
"João Vitor Bizotto Ferreira",
"Júlia Morone",
"Juliana de Melo Barbosa",
"Juliana Soares Rosa",
"Jurre Bourbaki",
"Karine Fernandes Batista",
"Laez Barbosa da Fonseca Filho",
"Laís Figueiredo",
"Larissa Dionisio ",
"Laudelina LP",
"Leonardo dos reis Carvalho",
"Leticia Rodrigues",
"LUCAS DE BARROS",
"Lucas dos Santos TInti",
"Lucas Lago",
"Luci Hidaka",
"Luis Arantes",
"Luis Otavio Ribeiro",
"Luiza Wainer",
"Luiz Guilherme Pereira de Almeida Lins",
"Marcelo Marquesini",
"Marcia Ohlson",
"Maria Cecília Gomes",
"Mariana Pereira Leal",
"marina frota",
"Marisa Sanematsu",
"Marlus Araujo",
"Matheus Paulo Batista Grandi",
"Mayara Ferreira",
"Michel Marechal",
"Miguel Vieira",
"Narrira",
"Nathalia Rodrigues",
"Nathalia Vieira Ferreira",
"Olívia Bandeira",
"Paulo F",
"PRISCILA CORREA BURACOSKY DOS SANTOS",
"Rafael Bantu",
"Rafael Ramblas",
"Rebecca Oliveira",
"Renata Assumpção"
],
[
"Renato Farias de Araujo",
"Renato Zannon",
"Ricardo Shiota Yasuda",
"Rita Taraborelli",
"Roberto Rodrigues",
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo Viana",
"Rodrigo Abrantes da Silva",
"Rodrigo Didier Anderson",
"Rodrigo Ghedin",
"Ronaldo Toshio",
"Rondineli Saad",
"Samer Maalouli",
"Sergio Amadeu da Silveira",
"Thais Mendes",
"Thaís Tavares Lima",
"Thiago Benicchio",
"Thiago Carvalho Bayerlein",
"Thiago Dantas",
"Thiago Leoncio Scherrer",
"Thiago Matos",
"Thiago Wolf Scalabrini",
"Tiago Filgueiras Pimentel",
"Tiago Lubiana",
"Tiago Lubiana",
"Tulio Malaspina",
"Valentina Garavaglia de Souza",
"Valéria Barros",
"Vanessa Pereira dos Anjos",
"Veridiana Alimonti",
"Victor",
"Vinicius ",
"Vinícius Shoiti Koike Graciliano",
"Vitor Hirata Sanches",
"Waldo Almeida Ramalho",
"Wallace Viana",
"William Saraiva Cimino",
"Willian Lopes"
]
     ] 

contato:
  id: "contato"
  titulo: "Contact"
  subtitulo: "Send your questions and suggestions."
  descricao: "Long exposure image of a city at night. A starless sky occupies the upper fifth of the image, while buildings with lit windows occupy the remaining space. In the left lower half of the image, part of a street is visible, with the trajectories of headlights visible due to the long exposure."
  button_text: "Send"
  formspree_language_code: "en"
  validacao:
    nome:
      placeholder: "Name (optional)"
    email:
      placeholder: "Email *"
      aviso: "Enter your email address."
    mensagem:
      placeholder: "Your message *"
      aviso: "Type your message here."

footer:
  copyright: "CryptoRave 2020 - Sem privacidade não há liberdade - [Onion service](https://onion.cryptorave.org/)"
  antiassedio:
    texto: "Code of Conduct"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
#    - icon: "fa-ticket"
#      link: "https://tickets.cryptorave.org/cr2020/free"

---
