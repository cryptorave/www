---
language_code: "es"
title: "CryptoRave"

header:
  descricao:
    ""
sobre:
  id: "sobre"
  descricao:
    [
      "**la CryptoRave**, es un evento anual de 36 horas que acoge actividades diversas sobre seguridad, criptografía, hacking, anonimato, privacidad y libertad en la red.",
      "CryptoRave es **abierta y gratuita** - sólo hace falta [inscripción online](#inscricao) y realizada en la [Ciudad de São Paulo](#local). Inspirada por una [acción global](https://cryptoparty.in), descentralizada para diseminar y democratizar el conocimiento y conceptos básicos de criptografía y software libre, el evento nació en 2014 como reacción a la difusión de informaciones que confirmaron la acción de gobiernos para mantener a la población mundial bajo permanente vigilancia y escucha."
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "Logo de la CR2020, en color blanca con fondo negro: a la izquierda, las letras 'CR' compuestas por los números 0 y 1; al medio, una imagen de un procesador con una llave pixelada en su centro; a la derecha, un número '2020' escrito en dos lineas (con dos guarismos por linea) en fuente mas pequeña, de manera que la soma de sus alturas sea igual a la altura del texto 'CR'."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamiento"
  inscricao: "Inscripciones"
  keynotes: "Keynotes"
  programacao: "Programación"
  festa: "Fiesta"
  realizacao: "Realización"
  apoio: "Soporte"
  patrocinadores: "Auspiciantes"
  contato: "Contacto"
  local: "Ubicación"
  menu_extra:
#    - name: "CPA"
#      url: "https://cpa.cryptorave.org/pt-br/cr2020/cfp/session/new"
#      weight: 9"
#    - name: "Onion"
#      url:  "https://onion.cryptorave.org"
#      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamiento Colectivo"
  descricao: "La CryptoRave sólo es posible gracias al apoyo - financiero y de difusión -  de una comunidad distribuida y anónima pero que garantiza que cada año más personas participen en el evento. Nosotros, de la organización, aportamos nuestro granito de arena creando recompensas fantásticas para los donadores pero no nos quedamos con nada: todo se invierte en el evento. Muchas gracias a [todos los que contribuyeran](#patrocinadores), el evento no podría acontecer sin su colaboración."
  link_catarse: "https://www.catarse.me/pt/projects/109977/embed"
  titulo_btc: "Colabore con Bitcoin"
  descricao_btc: "Tenemos una billetera de bitcoin, a que puedes donar con la dirección abajo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para la dirección Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
#  recompensas:
#    - linha:
#      - valor: "R$15"
#        img: ""
#        descricao_img: ''
#        descricao: "**FREE HUGS** - AGRADECIMIENTO escrito en las nubes (sitio)"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134314"
#
#      - valor: "R$25"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTOSTICKER** - 2 ETIQUETAS ADHESIVAS DE LA CRYPTORAVE 2018 + agradecimiento en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134315"
#
#      - valor: "R$40"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTOCAM** - 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134316"
#
#    - linha:
#      - valor: "R$50"
#        img: ""
#        descricao_img: ""
#        descricao: "**SENIORSTICKER** - 4 ETIQUETAS ADHESIVAS ESPECIALES + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimiento en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134317"
#
#      - valor: "R$60"
#        img: ""
#        descricao_img: ""
#        descricao: "**ESFEROMÁGYCA** - NOVIDAD!!! Escriba con tinta invisible. Revele con luz mágica. 1 ESFERO MÁGYCA + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=136566"
#
#      - valor: "R$75"
#        img: ""
#        descricao_img: ""
#        descricao: "**MASTERSTICKER** - 4 ETIQUETAS ADHESIVAS ESPECIALES + 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nuvens."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134318"
#
#    - linha:
#      - valor: "R$120"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRIPTOKNEK** - 1 TAZA DE LA CRYPTORAVE 2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimiento en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=13432"
#
#      - valor: "R$135"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRYPTORAVER** - 1 CAMISETA ESPECIAL DE LOS APOYADORES + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimento en las nuvens."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134321"
#
#      - valor: "R$375"
#        img: ""
#        descricao_img: ''
#        descricao: "**MASTER CRYPTORAVER** - 1 CHOMPA CRYPTORAVE 2018 + 2 ETIQUETAS ADHESIVAS DE LA CR2018 + agradecimiento en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134322"
#
#    - linha:
#      - valor: "R$475"
#        img: ""
#        descricao_img: ''
#        descricao: "**HEROIC CRYPTORAVER** - 1 CHOMPA CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE LOS APOYADORES + 2 ETIQUETA ADHESIVA CR2018 + agradecimientos en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134323"
#
#      - valor: "R$700"
#        img: ""
#        descricao_img: ''
#        descricao: "**CRYPTOJEDI** - 1 CHOMPA CRYPTORAVE 2018 + 1 CAMISETA ESPECIAL DE LOS APOYADORES + 1 TAZA CRYPTORAVE 2018 + 4 ETIQUETAS ADHESIVAS ESPECIALES + 4 ETIQUETAS ADHESIVAS PARA TAPAR LA WEBCAM + 1 PIN CR2018 + 2 ETIQUETAS ADHESIVAS CR2018 + agradecimientos en las nubes."
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134324"
#
#      - valor: "R$1000"
#        img: ""
#        descricao_img: ''
#        descricao: "**CRIPTOJORNADA** - TALLER DE SEGURIDAD DE LA INFORMACIÓN PARA SU COLECTIVO O ORGANIZACIÓN + agradecimiento en las nubes"
#        url: "https://www.catarse.me/pt/projects/70453/contributions/new?reward_id=134325"

inscricao:
  id: "inscricao"
  titulo: "Inscripciones"
  descricao_ticket: "La inscripción y participación en la CryptoRave son totalmente gratuitas. La presentación de la entrada será imprescindible para poder acceder al evento. La inscripción se abrirán pronto."
  link_ticket: ""
  link_boletim: "Para seguir las actividades y saber más sobre la programación, [recibe las novedades más recientes sobre la CryptoRave](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brasil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Coordenadora dos times do Projeto Tor, recentemente foi anunciada como a próxima Diretora Executiva do projeto."
  descricao: "O problema é muito maior que o Facebook. É o modelo econômico da Internet: o modelo de vigilância da Internet. Comunicar, compartilhar e acessar informações não deveria tornar você um alvo ou um produto. Você não deveria ser explorado/a ao usar a Internet."

programacao:
  id: "programacao"
  titulo: "Programación"
  descricao:
    [
      "La versión del evento de 2019 está disponible en [agenda.cryptorave.org](https://agenda.cryptorave.org/). La programación deste año está [abierta para colaboraciones](https://hola.cryptorave.org/2020/cfp) hasta día 10 de marzo. El evento empieza a las 19 horas del 15 de mayo."
    ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'Logo de la CR2017.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'Logo de la CR2016.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'Logo de la CR2015.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'Logo de la CR2014.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Ubicación"
  descricao:
    [
      "La CryptoRave 2020 será realizada en [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "Dirección: Rua da Consolação, 94 - República, São Paulo - SP. Cerca de la estación del metro Anhangabaú."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64346992969513%2C-23.548138354058587%2C-46.64142608642579%2C-23.546663037375726&amp;layer=mapnik&amp;marker=-23.547400697786546%2C-46.642448008060455"
    link: "https://osm.org/go/M~ziJkQVL?way=48661210"
    tooltip: "Click para abrir en otra pestaña"


festa:
  id: "festa"
  titulo: "Fiesta en Hangar 110"
  descricao:
    [
      "La fiesta de CryptoRave será el sábado, el 05/05, a las 20h en [Hangar 110](http://www.hangar110.com.br/). La entrada es gratis y exclusiva para participantes de CryptoRave!",
      "Dirección: Rua Rodolfo Miranda, 110 - Bom Retiro (5 minutos a pie del metro Armênia - linea azul)"
    ]
  img: "hangar110.jpg"
  link: "http://www.hangar110.com.br/"
  atracoes:
    [
      "20:00 - abertura de la casa!",
      "**LINEUP**",
      "21:00 - 21:45 - cabeça",
      "22:00 - 23:00 - afrorep",
      "23:00 - 00:00 - datamosh",
      "00:00 - 01:00 - octarina",
      "01:00 - 01:45 - show do retrigger",
      "01:45 - 03:00 - arkanoid",
      "03:00 - 04:15 - swaaag",
      "04:45 - 06:00 - lines",
      "**PROJEÇÕES**",
      "21:45 - 23:00 - Terms and conditions may apply",
      "23:30 - 00:45 - Bruno Treviso",
      "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (trecho)",
      "01:45 - 03:00 - NVVE MVE",
      "03:00 - 04:15 - chr0ma",
      "04:15 - 06:00 - Downloaded"
    ]

realizacao:
  id: "realizacao"
  titulo: "Realización"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo de la Escola de Ativismo: el texto 'ESCOLA DE ATIVISMO' (escuela de activismo) en blanco en el centro de uno rectángulo negro. El largo del rectángulo es un pouco mas grande que el largo del texto y su altura casi cuatro veces mas que la altura del texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo del Actantes: con fondo blanco, las letras 'ACT' en rojo, y en secuencia las letras 'ANTES' hechas con los números 0 e 1 en negro. No hay espacio entre las dos partes; el logo es una palabra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo del Saravá: al centro de un fondo rectangular negro, la palabra 'SARAVÁ' en blanco en una fuente que se asemeja al texto manuscrito. La altura de las letras varia entre cerca de un tercio y dos tercios de la altura del rectángulo. Horizontalmente, el texto ocupa cerca de cuatro quintos del ancho del rectángulo."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Logo del Intervozes, naranja en fondo blanco: un cuadrado con vértices redondeadas y con arcos de círculos blancos con diversas larguras le atravesando; en su derecha, en dos lineas con altura total poco mas pequeña que la altura del cuadrado, los textos 'intervozes' (en la primeira linea) y 'coletivo brasil de comunicação social' (en la segunda linea)."
        url: "http://intervozes.org.br"

apoio:
  id: "apoio"
  titulo: "Soporte"
  parceiros:
    - linha:
      - img: "ccc.jpg"
        descricao_img: "Logo de Chaos Computer Club (CCC): en negro sobre un fondo blanco, los contornos de una imagen que se parece a una llave y a un circuito electrónico. Desde el interior de un rectángulo con las esquinas chaflanadas, 4 lineas direccionanse al centro del lado derecho; desde allá, alineanse perpendicularmente a este lado y salen del rectángulo, paralelas entre si. Después de una distancia de cerca de la mitad del comprimento del rectángulo, las lineas se retorcen, formando un nudo. Como hilos, las lineas colgan abajo del nudo."
        url: "https://ccc.de/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Auspiciantes"
  descricao: "Nuestro más sincero agradecimiento a todos y todas las que contribuyeran con nuestra [campaña de financiamento colectivo](#financiamento):"
  doadores:
    [
[
"Adriana Leticya Gontijo",
"Alessio Esteves",
"Alexandre Isaac Siqueira",
"Alexandre Medeiros",
"Alice Christina Matsuo",
"Alice Lana",
"Aline Freitas",
"Ana Elisa",
"Ana Luiza Portello Bastos",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson C",
"Anderson de Jesus Nascimento Ribeiro",
"Anderson Pereira Leal",
"André F. Viana",
"André Luiz ",
"André Rodrigues Ferraz Barbosa",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Anon",
"Antonio Arles dos Anjos Junior",
"Ariel Ferreira Rodrigues",
"Arthur Costa Lima",
"Augusto Bennemann",
"Bruno Kim Medeiros Cesar",
"Bruno Martins",
"BRUNO PAIVA DE OLIVEIRA",
"brunz",
"Cadós Sanchez",
"caioau",
"Caio Eduardo Zangirolami Saldanha",
"Caio Fontes de Castro",
"Caio Henrique Silva Ramos",
"Carla Oliveira Santos",
"Carlos Cabral",
"Carlos S",
"Carolina Vergotti Ferrigno",
"Coalinha das Planilhas",
"cybelle",
"Daniel Miranda Birochi",
"Daniel Rondinelli Roquetti"
],
[
"Dara Gonçalves",
"Diana Iliescu",
"Divina Vitorino",
"Douglas da Silva Costa",
"Douglas Vinicius Esteves",
"Editora Monstro dos Mares",
"Eduardo Costa Lisboa",
"Elisa X",
"Emilio Simoni",
"Enzzo Pessanha Cavallo",
"Estanislau Gonçalves",
"Euler Neto",
"Evelize Pacheco Simões",
"Fabio Barros",
"Fábio Meneghetti",
"Felipe",
"Fernanda Campagnucci",
"Fernanda Fantelli",
"Fernanda Shirakawa",
"Fernao Vellozo",
"Filipe Monguilhott Falcone",
"Francine Emilia Costa",
"Gabriela Bittencourt",
"Gabriel Gortan",
"Gabriella De Biaggi",
"glauber noccioli de souza",
"Guilherme Otero",
"Guilherme Ribeiro de Lima",
"Gustavo Felipe Vieira de Alencar",
"Gustavo Gus",
"Gustavo Suto",
"Gyssele Mendes",
"Henrique Nascimento Santos",
"Ian Fernandez",
"Ian Fernandez",
"Ines Aisengart Menezes",
"Ingrid Elisabeth Spangler",
"Ingrid Elisabeth Spangler",
"Itamar Silva",
"Janaina Menegaz Spode",
"João Moreno Rodrigues Falcão",
"Joao Rafael Bonilha"
],
[
"João Vitor Bizotto Ferreira",
"Júlia Morone",
"Juliana de Melo Barbosa",
"Juliana Soares Rosa",
"Jurre Bourbaki",
"Karine Fernandes Batista",
"Laez Barbosa da Fonseca Filho",
"Laís Figueiredo",
"Larissa Dionisio ",
"Laudelina LP",
"Leonardo dos reis Carvalho",
"Leticia Rodrigues",
"LUCAS DE BARROS",
"Lucas dos Santos TInti",
"Lucas Lago",
"Luci Hidaka",
"Luis Arantes",
"Luis Otavio Ribeiro",
"Luiza Wainer",
"Luiz Guilherme Pereira de Almeida Lins",
"Marcelo Marquesini",
"Marcia Ohlson",
"Maria Cecília Gomes",
"Mariana Pereira Leal",
"marina frota",
"Marisa Sanematsu",
"Marlus Araujo",
"Matheus Paulo Batista Grandi",
"Mayara Ferreira",
"Michel Marechal",
"Miguel Vieira",
"Narrira",
"Nathalia Rodrigues",
"Nathalia Vieira Ferreira",
"Olívia Bandeira",
"Paulo F",
"PRISCILA CORREA BURACOSKY DOS SANTOS",
"Rafael Bantu",
"Rafael Ramblas",
"Rebecca Oliveira",
"Renata Assumpção"
],
[
"Renato Farias de Araujo",
"Renato Zannon",
"Ricardo Shiota Yasuda",
"Rita Taraborelli",
"Roberto Rodrigues",
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo Viana",
"Rodrigo Abrantes da Silva",
"Rodrigo Didier Anderson",
"Rodrigo Ghedin",
"Ronaldo Toshio",
"Rondineli Saad",
"Samer Maalouli",
"Sergio Amadeu da Silveira",
"Thais Mendes",
"Thaís Tavares Lima",
"Thiago Benicchio",
"Thiago Carvalho Bayerlein",
"Thiago Dantas",
"Thiago Leoncio Scherrer",
"Thiago Matos",
"Thiago Wolf Scalabrini",
"Tiago Filgueiras Pimentel",
"Tiago Lubiana",
"Tiago Lubiana",
"Tulio Malaspina",
"Valentina Garavaglia de Souza",
"Valéria Barros",
"Vanessa Pereira dos Anjos",
"Veridiana Alimonti",
"Victor",
"Vinicius ",
"Vinícius Shoiti Koike Graciliano",
"Vitor Hirata Sanches",
"Waldo Almeida Ramalho",
"Wallace Viana",
"William Saraiva Cimino",
"Willian Lopes"
]
     ] 

contato:
  id: "contato"
  titulo: "Contacto"
  subtitulo: "Envíe su sugerencia o inquietud."
  descricao: "Imagen de una ciudad por la noche, foto tomada con longa exposición. En la parte superior, un cielo sin estrellas rellena un quinto de la imagen, edificios con ventanas alumbradas llenan el espacio restante. En la parte inferior izquierda esta visible parte de una calle, con camino hechos con los faros de los coches debido a la longa exposición."
  button_text: "Enviar"
  enviando: "Aguardando la confirmación del envio..."
  obrigado: "Gracias por ponerse en contacto con nosotros!"
  erro: "Ups, la mensaje no logró ser enviada. Por favor póngase en contacto por el correo: contato@cryptorave.org."
  validacao:
    nome:
      placeholder: "Nombre (opcional)"
    email:
      placeholder: "Correo *"
      aviso: "Digite la dirección de su correo electrónico."
    mensagem:
      placeholder: "Su mensaje *"
      aviso: "Digite su mensaje."


footer:
  copyright: "CryptoRave.org 2021 - Sem privacidade não há liberdade - [Onion service](https://onion.cryptorave.org/)"
  antiassedio:
    texto: "Código de Conducta"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
#    - icon: "fa-ticket"
#      link: "https://tickets.cryptorave.org/cr2020/free"

---
