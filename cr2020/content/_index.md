---
language_code: "pt-br"
title: "CryptoRave"

header:
  descricao:
    "CryptoRave"
sobre:
  id: "sobre"
  descricao:
    [
      "A **CryptoRave** é um evento anual que reúne, em 36 horas, diversas atividades sobre segurança, criptografia, hacking, anonimato, privacidade e liberdade na rede ",
      "A CryptoRave é **aberta e gratuita** - basta [inscrição online](#inscricao) - e realizada na cidade de São Paulo.",
      "Inspirada em uma [ação global](https://cryptoparty.in), descentralizada para disseminar e democratizar o conhecimento e conceitos básicos de criptografia e software livre, o evento teve início em 2014, como reação à divulgação de informações que confirmaram a ação de governos e corporações para manter a população mundial sob vigilância e monitoramento constante. Junte-se a nós!" 
    ]
  vimeo_link: "https://www.youtube.com/embed/Ow8CEj665xM"

nav:
  descricao:
    "Logo da CR2020, em cor branca com fundo preto: à esquerda, as letras 'CR' formadas pelos números 0 e 1; ao meio, uma imagem de um processador com uma chave pixelada em seu centro; à direita, o número '2020' escrito em duas linhas (com dois algarismos por linha) em fonte menor, de forma que a soma de suas alturas seja igual à altura do texto 'CR'."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamento"
  inscricao: "Atualizações"
  programacao: "Programação"
  realizacao: "Realização"
  apoio: "Apoio"
  patrocinadores: "Patrocinadores"
  contato: "Contato"
  local: "Local"
  menu_extra:
#    - name: "Grade"
#      url: "https://cpa.cryptorave.org/pt-BR/cr2019/public/schedule"
#      weight: 9"
#    - name:  "Blog"
#      url:  "https://blog.cryptorave.org"
#      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamento Coletivo"
  descricao: "A Cryptorave é realizada com apoio financeiro e de divulgação de uma comunidade distribuída, anônima, mas que garante, ano após ano, que mais pessoas participem do evento. Nós da organização fazemos a nossa parte ao elaborarmos recompesas maravilhosas para quem apoia, mas não ficamos com nada. Tudo é investido no evento. "
  link_catarse: "https://www.catarse.me/pt/projects/109977/embed"
  titulo_btc: "Colabore em Bitcoin"
  descricao_btc: "Nós temos uma carteira de bitcoin, você pode doar para o endereço abaixo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para o endereço Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."
#  recompensas:
#    - linha:
#      - valor: "R$10"
#        img: ""
#        descricao_img: ""
#        descricao: "**CORAÇÃO DE MÃE** - AGRADECIMENTO escrito nas nuvens (site)"
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202114"
#
#      - valor: "R$20"
#        img: ""
#        descricao_img: ""
#        descricao: "**COLA AQUI** - 2 ADESIVOS CR2020 + agradecimento nas nuvens. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202115"
#
#      - valor: "R$42"
#        img: ""
#        descricao_img: ""
#        descricao: "**TAPA CIBERSEXO** - 1 CARTELA DE ADESIVOS PARA COBRIR A WEBCAM + recompensas acima. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202117"
#
#    - linha:
#      - valor: "R$70"
#        img: ""
#        descricao_img: ""
#        descricao: "**TAPA TUDO** - 1 TAPA CÂMERA ABRE E FECHA + recompensas acima. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202120"
#
#      - valor: "R$100"
#        img: ""
#        descricao_img: ""
#        descricao: "**CAFEÍNA DEALER** - 1 CANECA ESPECIAL CR2020 + 1 livro + recompensas acima. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202126"
#
#      - valor: "R$150"
#        img: ""
#        descricao_img: ""
#        descricao: "**RESPEITA! CAMISETA EXCLUSIVA** - 1 CAMISETA EXCLUSIVA CR2020 + 2 adesivos da CR2020 + 1 cartela de adesivos para câmeras e agradecimento nas nuvens. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202138"
#
#    - linha:
#      - valor: "R$250"
#        img: ""
#        descricao_img: ""
#        descricao: "**BLOCK DE SINAL** -1 capa bloqueadora de sinal para o seu tracking device (aka celular) + 2 adesivos da CR2020 + 1 cartela de adesivos para câmeras e agradecimento nas nuvens. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202139"
#
#      - valor: "R$350"
#        img: ""
#        descricao_img: ""
#        descricao: "**CIBER MOLETOM** - 1 MOLETOM CR2020 + 2 adesivos CR2020 + agradecimento nas nuvens. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202140"
#
#      - valor: "R$450"
#        img: ""
#        descricao_img: ""
#        descricao: "**CRIPTO Guerrilha** - 1 MOLETOM CR2020 + 1 CAMISETA CR2020 + 1 CARTELA DE ADESIVOS PARA COBRIR A WEBCAM + 2 adesivos CR2020 + agradecimento nas nuvens. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202141"
#
#    - linha:
#      - valor: "R$650"
#        img: ""
#        descricao_img: ""
#        descricao: "**Hacker Resistência Underground Epic** - 1 MOLETOM CR2020 + 1 CAMISETA EXCLUSIVA CR2020 + 1 CANECA CR2020 + 1 TAPA CÂMERA ABRE E FECHA + 1 CARTELA DE ADESIVOS PARA COBRIR A CAM + 2 adesivos CR2020 + CAPA BLOQUEADORA + LIVROS + agradecimento nas nuvens. Todas as recompensas devem ser retiradas no dia do evento."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202142"
#
#      - valor: "R$1000"
#        img: ""
#        descricao_img: ""
#        descricao: "**Segurança na PRÁTICA** - 1 OFICINA DE SEGURANÇA PARA SEU GRUPO + agradecimento nas nuvens. Essa recompensa deve ser agendada com a equipe da CryptoRave e executada até Outubro de 2020."
#        url: "https://www.catarse.me/projects/109977/contributions/new?reward_id=202143"

inscricao:
  id: "inscricao"
  titulo: "Atualizações"
#  descricao: "A inscrição e a participação na CryptoRave é totalmente gratuita. No dia do evento, você deverá apresentar o tíquete na entrada."
#  link_ticket: "[Faça a sua inscrição gratuita na CryptoRave](https://tickets.cryptorave.org/2020/free)"
  descricao_boletim: "Para acompanhar as últimas novidades sobre a Cryptorave,"
  link_boletim: "[inscreva-se para receber o boletim do evento](https://lists.riseup.net/www/subscribe/cryptorave-boletim)"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: ""
  nome: "[]()"
  bio: "."
  descricao: "."


programacao:
  id: "programacao"
  titulo: "Programação"
  descricao:
#    [
#      "A versão 0.1 da programação da CryptoRave 2020 já está disponível: [agenda.cryptorave.org](https://agenda.cryptorave.org)!", 
#      "A CR2020 começará às 19 horas do dia 15 de maio de 2020 e vai até às 22h do dia 16 de maio de 2020.",
#      "Baixe o APP da programação para [Android](https://play.google.com/store/apps/details?id=org.cryptorave.schedule&hl=en_US)."
#    ]
     [ 
       "Envie a sua proposta de atividade para a CryptoRave 2020! As inscrições para as propostas de atividade estarão abertas até o dia 9 de março de 2020. Leia o Chamado de Proposta de Atividades desta edição:",
        "[Chamado de Proposta de Atividade](https://hola.cryptorave.org/2020/cfp)"
     ]

#  anteriores:
#    - linha:
#      - titulo: "CR2019"
#        img: "cr2019logo.png"
#        descricao_img: 'Logo da CR19.'
#        url: "https://2019.cryptorave.org"
#    - linha:
#      - titulo: "CR2018"
#        img: "cr2018logo.png"
#        descricao_img: 'Logo da CR18.'
#        url: "https://2018.cryptorave.org"
#    - linha:
#      - titulo: "CR2017"
#        img: "cr2017logo.png"
#        descricao_img: 'Logo da CR2017.'
#        url: "https://2017.cryptorave.org"    
#      - titulo: "CR2016"
#        img: "cr2016logo.png"
#        descricao_img: 'Logo da CR2016.'
#        url: "https://2016.cryptorave.org"
#    - linha:
#      - titulo: "CR2015"
#        img: "cr2015logo.png"
#        descricao_img: 'Logo da CR2015.'
#        url: "https://2015.cryptorave.org"
#      - titulo: "CR2014"
#        img: "cr2014logo.png"
#        descricao_img: 'Logo da CR2014.'
#        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Local"
  descricao:
    [
      "A CryptoRave 2020 será realizada na [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "Endereço:  R. da Consolação, 94 - República, São Paulo - SP. Ao lado da estação do metrô Anhangabaú."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64346992969513%2C-23.548138354058587%2C-46.64142608642579%2C-23.546663037375726&amp;layer=mapnik&amp;marker=-23.547400697786546%2C-46.642448008060455"
    link: "https://osm.org/go/M~ziJkQVL?way=48661210"
    tooltip: "Clique para abrir em outra aba"

realizacao:
  id: "realizacao"
  titulo: "Realização"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo da Escola de Ativismo: o texto 'ESCOLA DE ATIVISMO' em branco no centro de um retângulo preto. O retângulo tem largura pouco maior que a do texto e altura aproximadamente quatro vezes a altura do texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo da Actantes: com fundo branco, as letras 'ACT' em vermelho, e, em seguida, as letras 'ANTES' formadas pelos números 0 e 1 em preto. Não há espaçamento entre as duas partes; o logo é uma palavra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo do Saravá: ao centro de um fundo retangular preto, a palavra 'SARAVÁ' em branco em uma fonte que se assemelha a texto manuscrito. A altura das letras varia entre cerca de um terço e dois terços da altura do retângulo. Horizontalmente, o texto ocupa cerca de quatro quintos do comprimento do retângulo."
        url: "https://sarava.org"
      - img: "intervozes.png"
        descricao_img: "Logo do Intervozes, laranja em fundo branco: um quadrado com vértices arredondados com arcos de círculo brancos de larguras diversas atravessando-o; à direita, em duas linhas com altura total pouco menor que a do quadrado, os textos 'intervozes' (na primeira liha) e 'coletivo brasil de comunicação social (na segunda linha)."
        url: "http://intervozes.org.br"

apoio:
  id: "apoio"
  titulo: "Apoio"
  parceiros:
    - linha:
      - img: "ccc.jpg"
        descricao_img: "Logo do Chaos Computer Club (CCC): em preto em um fundo branco, os contornos de uma imagem que se assemelha a uma imagem e a um circuito eletrônico. De dentro de um retângulo com os cantos chanfrados, 4 linhas se direcionam ao centro do lado direito; de lá, se alinham perpendicularmente a este lado e saem do retângulo, paralelas entre si. Após uma distância de cerca de metade do comprimento do retângulo, as linhas se torcem, formando um nó. Se assemelhando a fios, as linhas pendem torcidas abaixo do nó."
        url: "https://ccc.de/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Patrocinadores"
  descricao: "Agradecemos de coração a todas e todos que contribuíram com a nossa campanha de [financiamento coletivo](#financiamento):"
  doadores: 
    [
[
"Adriana Leticya Gontijo",
"Alessio Esteves",
"Alexandre Isaac Siqueira",
"Alexandre Medeiros",
"Alice Christina Matsuo",
"Alice Lana",
"Aline Freitas",
"Ana Elisa",
"Ana Luiza Portello Bastos",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson C",
"Anderson de Jesus Nascimento Ribeiro",
"Anderson Pereira Leal",
"André F. Viana",
"André Luiz ",
"André Rodrigues Ferraz Barbosa",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Anon",
"Antonio Arles dos Anjos Junior",
"Ariel Ferreira Rodrigues",
"Arthur Costa Lima",
"Augusto Bennemann",
"Bruno Kim Medeiros Cesar",
"Bruno Martins",
"BRUNO PAIVA DE OLIVEIRA",
"brunz",
"Cadós Sanchez",
"caioau",
"Caio Eduardo Zangirolami Saldanha",
"Caio Fontes de Castro",
"Caio Henrique Silva Ramos",
"Carla Oliveira Santos",
"Carlos Cabral",
"Carlos S",
"Carolina Vergotti Ferrigno",
"Coalinha das Planilhas",
"cybelle",
"Daniel Miranda Birochi",
"Daniel Rondinelli Roquetti"
],
[
"Dara Gonçalves",
"Diana Iliescu",
"Divina Vitorino",
"Douglas da Silva Costa",
"Douglas Vinicius Esteves",
"Editora Monstro dos Mares",
"Eduardo Costa Lisboa",
"Elisa X",
"Emilio Simoni",
"Enzzo Pessanha Cavallo",
"Estanislau Gonçalves",
"Euler Neto",
"Evelize Pacheco Simões",
"Fabio Barros",
"Fábio Meneghetti",
"Felipe",
"Fernanda Campagnucci",
"Fernanda Fantelli",
"Fernanda Shirakawa",
"Fernao Vellozo",
"Filipe Monguilhott Falcone",
"Francine Emilia Costa",
"Gabriela Bittencourt",
"Gabriel Gortan",
"Gabriella De Biaggi",
"glauber noccioli de souza",
"Guilherme Otero",
"Guilherme Ribeiro de Lima",
"Gustavo Felipe Vieira de Alencar",
"Gustavo Gus",
"Gustavo Suto",
"Gyssele Mendes",
"Henrique Nascimento Santos",
"Ian Fernandez",
"Ian Fernandez",
"Ines Aisengart Menezes",
"Ingrid Elisabeth Spangler",
"Ingrid Elisabeth Spangler",
"Itamar Silva",
"Janaina Menegaz Spode",
"João Moreno Rodrigues Falcão",
"Joao Rafael Bonilha"
],
[
"João Vitor Bizotto Ferreira",
"Júlia Morone",
"Juliana de Melo Barbosa",
"Juliana Soares Rosa",
"Jurre Bourbaki",
"Karine Fernandes Batista",
"Laez Barbosa da Fonseca Filho",
"Laís Figueiredo",
"Larissa Dionisio ",
"Laudelina LP",
"Leonardo dos reis Carvalho",
"Leticia Rodrigues",
"LUCAS DE BARROS",
"Lucas dos Santos TInti",
"Lucas Lago",
"Luci Hidaka",
"Luis Arantes",
"Luis Otavio Ribeiro",
"Luiza Wainer",
"Luiz Guilherme Pereira de Almeida Lins",
"Marcelo Marquesini",
"Marcia Ohlson",
"Maria Cecília Gomes",
"Mariana Pereira Leal",
"marina frota",
"Marisa Sanematsu",
"Marlus Araujo",
"Matheus Paulo Batista Grandi",
"Mayara Ferreira",
"Michel Marechal",
"Miguel Vieira",
"Narrira",
"Nathalia Rodrigues",
"Nathalia Vieira Ferreira",
"Olívia Bandeira",
"Paulo F",
"PRISCILA CORREA BURACOSKY DOS SANTOS",
"Rafael Bantu",
"Rafael Ramblas",
"Rebecca Oliveira",
"Renata Assumpção"
],
[
"Renato Farias de Araujo",
"Renato Zannon",
"Ricardo Shiota Yasuda",
"Rita Taraborelli",
"Roberto Rodrigues",
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo Viana",
"Rodrigo Abrantes da Silva",
"Rodrigo Didier Anderson",
"Rodrigo Ghedin",
"Ronaldo Toshio",
"Rondineli Saad",
"Samer Maalouli",
"Sergio Amadeu da Silveira",
"Thais Mendes",
"Thaís Tavares Lima",
"Thiago Benicchio",
"Thiago Carvalho Bayerlein",
"Thiago Dantas",
"Thiago Leoncio Scherrer",
"Thiago Matos",
"Thiago Wolf Scalabrini",
"Tiago Filgueiras Pimentel",
"Tiago Lubiana",
"Tiago Lubiana",
"Tulio Malaspina",
"Valentina Garavaglia de Souza",
"Valéria Barros",
"Vanessa Pereira dos Anjos",
"Veridiana Alimonti",
"Victor",
"Vinicius ",
"Vinícius Shoiti Koike Graciliano",
"Vitor Hirata Sanches",
"Waldo Almeida Ramalho",
"Wallace Viana",
"William Saraiva Cimino",
"Willian Lopes"
]
     ] 

contato:
  id: "contato"
  titulo: "Contato"
  subtitulo: "Envie sua sugestão ou dúvida."
  descricao: "Imagem noturna de longa exposição de uma cidade. Um céu sem estrelas ocupa o quinto superior da imagem, e prédios com janelas iluminadas ocupam o espaço restante. Na parte inferior esquerda, parte de uma rua é visível, com os trajetos de faróis de carros visíveis devido à exposição longa."
  button_text: "Enviar"
  formspree_language_code: "pt-BR"
  validacao:
    nome:
      placeholder: "Nome (opcional)"
    email:
      placeholder: "Email *"
      aviso: "Digite seu endereço de e-mail."
    mensagem:
      placeholder: "Sua mensagem *"
      aviso: "Digite a sua mensagem."

footer:
  copyright: "CryptoRave.ORG 2021 - Sem privacidade não há liberdade - [Onion services](https://onion.cryptorave.org/)"
  antiassedio:
    texto: "Código de Conduta"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    - icon: "fa-ticket"
      link: "https://tickets.cryptorave.org/2020/free"
---
