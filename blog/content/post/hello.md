+++
title = "Agora temos um blog!"
description = "uma pequena novidade"
tags = [
   ]
date = "2017-10-28"
categories = [
    "",
]
+++

Às vezes sentimos falta de ter um lugar para anunciar as boas novas da CryptoRave. Um blog é uma ótima ferramenta para centralizar esses pequenos anúncios. Às vezes os algoritmos das mídias sociais são perversos e você fica sem ver na sua timeline o que nós estamos aprontando. Então, estamos inaugurando esse novo serviço: [blog.cryptorave.org](https://blog.cryptorave.org). Primeiro publicaremos aqui e depois replicaremos nos nossos outros canais: [newsletter](https://lists.riseup.net/www/subscribe/cryptorave-boletim), [twitter](https://twitter.com/cryptoravebr) e [facebook](https://facebook.com/cryptorave).


Sentimos a sua falta,


CryptoRave
