+++
title = "Sobre"
date = "2017-10-28"
menu = "main"
weight = 100
+++

A CryptoRave inspira-se na ação global e descentralizada da [CryptoParty](https://cryptoparty.in), a qual têm como objetivo difundir os conceitos fundamentais e softwares básicos de criptografia. Criada no início de 2014, a cada edição mais de 2500 pessoas participaram!

São 24 horas de conversas, trocas e aprendizagem na área de segurança de comunicação e criptografia. A CryptoRave acontece apenas **uma vez** por ano. Portanto, programe-se!

* [CryptoRave](https://cryptorave.org)
