#!/bin/env bash

set -ex

build_website() {
    WEBSITE_FOLDER="$1"
    echo "build_website func $WEBSITE_FOLDER"
    cd "$ROOT_PWD"/$dir
    source BUILD_CONF.env
    echo "BUILD_CONF is TYPE=${TYPE}, DEST_FOLDER=${DEST_FOLDER}"
    mkdir -p "$ROOT_DEST_FOLDER"/"$DEST_FOLDER"/
    rm -rf public/ # some years has public/ committed to the repo

    case "$TYPE" in 
        hugo)
            hugo --gc
            rsync -avh public/ "$ROOT_DEST_FOLDER"/"$DEST_FOLDER"/
            ;;
        old_hugo)
            eval "$OLD_HUGO_exec"
            rsync -avh public/ "$ROOT_DEST_FOLDER"/"$DEST_FOLDER"/
            ;;
        raw | dummy_php)
            rsync -avh --include-from="$ROOT_PWD"/rsync-filters.txt "$PWD"/ "$ROOT_DEST_FOLDER"/"$DEST_FOLDER"/
            ;;
    esac

    rm -rf public/ # cleanup
    cd "$ROOT_PWD" # return to origin
}

build_all() {
    for dir in */; do
        # Check if it is a directory
        if [ -d "$ROOT_PWD"/"$dir" ]; then
            if [ -e "$ROOT_PWD"/"$dir"BUILD_CONF.env ] ; then
                build_website "$dir"
            fi
        fi
    done
}

# https://github.com/gohugoio/hugo/releases/download/v0.18/hugo_0.18_Linux-64bit.tar.gz
# https://github.com/gohugoio/hugo/releases/download/v0.80.0/hugo_0.80.0_Linux-64bit.tar.gz

ROOT_PWD=$(pwd)
ROOT_DEST_FOLDER="${ROOT_PWD}/output_build_pipeline/"

echo "cleaning ROOT_DEST_FOLDER ..."
rm -rf "$ROOT_DEST_FOLDER"
mkdir -p "$ROOT_DEST_FOLDER"

build_all
