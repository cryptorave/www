
# NOTE in ther webserver, the ~/.ssh/authorized_keys should look like this
# command="/usr/bin/rrsync /path/to_htmls",restrict ssh-ed25519 AAAAC[...]
# also make sure rsync is installed

# All available Hugo docker images are listed here: https://gitlab.com/pages/hugo/container_registry

variables:
  ACTIVE_WEBSITE_DIR: ${CI_PROJECT_DIR}/cr2025 # only the currently active website gets tested and deployed automatically
  SERVER_SSH_URL: 'pipeline-consumer@cryptorave.org:/' # server location where rsync will send the results
  SERVER_SSH_FINGERPRINT: 'SHA256:ePxk7VMWeguVPF2lFIRYD/EIU+jw7dMPRzQiYgqLzgM' # NOTE that this is ed25519
  SERVER_SSH_PORT: 22 
  HUGO_VERSION: 0.111.3  # 0.111.3  was released on 12Mar2023
  HUGO_DOWNLOAD_URL: https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_linux-amd64.tar.gz
  HUGO_DOWNLOAD_URL_OLD1: https://github.com/gohugoio/hugo/releases/download/v0.18/hugo_0.18_Linux-64bit.tar.gz
  HUGO_DOWNLOAD_URL_OLD2: https://github.com/gohugoio/hugo/releases/download/v0.80.0/hugo_0.80.0_Linux-64bit.tar.gz
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - test
  - deploy

# doc: https://docs.gitlab.com/ee/ci/jobs/ssh_keys.html
# load a ssh key into the file variable called SSH_PRIVATE_KEY

.setup-ssh-priv-key: |
  mkdir -p ~/.ssh && chmod 0700 ~/.ssh
  mv -v "$SSH_PRIVATE_KEY" ~/.ssh/id_rsa
  chmod 400 ~/.ssh/id_rsa # give the correct permissions
  SSH_HOST="$(echo $SERVER_SSH_URL | awk -F'[@:]' '{print $2}')"
  echo "matching ssh key fingerprint SSH_HOST=${SSH_HOST} FINGERPRINT=${SERVER_SSH_FINGERPRINT}"
  ACTUAL_FINGERPRINT="$(ssh-keyscan -t ed25519 ${SSH_HOST} | ssh-keygen -lf -)"
  echo "$ACTUAL_FINGERPRINT" | grep --color=auto "$SERVER_SSH_FINGERPRINT" || { echo "SSH fingerprint verification FAILED!" ; exit 1 ; }
  ssh-keyscan -t ed25519 "$SSH_HOST" > ~/.ssh/known_hosts && chmod 0600 ~/.ssh/known_hosts

.install-hugo: |
  apt update && apt install --no-install-recommends -y rsync openssh-client wget ca-certificates
  echo "installing hugo version ${HUGO_VERSION}"
  rm -rf /tmp/hugo_tmp && mkdir -p /tmp/hugo_tmp
  wget -nv $HUGO_DOWNLOAD_URL -O /tmp/hugo_tmp/hugo.tar.gz
  cd /tmp/hugo_tmp/ && tar -xvf hugo.tar.gz && mv hugo /usr/bin/ && chmod 0755 /usr/bin/hugo
  cd /tmp && rm -rf hugo_tmp && mkdir -p /tmp/hugo_tmp
  echo "installing hugo version 0.18"
  wget -nv $HUGO_DOWNLOAD_URL_OLD1 -O /tmp/hugo_tmp/hugo.tar.gz
  cd /tmp/hugo_tmp/ && tar -xvf hugo.tar.gz && mv hugo_0.18_linux_amd64/hugo_0.18_linux_amd64 /usr/bin/
  chmod 0755 /usr/bin/hugo_0.18_linux_amd64 && cd /tmp/ && rm -rf /tmp/hugo_tmp/ && mkdir -p /tmp/hugo_tmp
  echo "installing hugo version 0.80.0"
  wget -nv $HUGO_DOWNLOAD_URL_OLD2 -O /tmp/hugo_tmp/hugo.tar.gz
  cd /tmp/hugo_tmp/ && tar -xvf hugo.tar.gz && mv hugo /usr/bin/hugo_0.80.0 && chmod 0755 /usr/bin/hugo_0.80.0


test:
  image: registry.gitlab.com/pages/hugo/hugo:${HUGO_VERSION}
  cache: [] # we don't need cache or artifacts from previous jobs/stages
  dependencies: []
  before_script:
    - cd ${ACTIVE_WEBSITE_DIR}
    - pwd
  script:
    - hugo --gc

.deploy:
  image: debian:bookworm-slim
  stage: deploy
  cache: [] # we don't need cache or artifacts from previous jobs/stages
  dependencies: []
  before_script:
    - !reference [.install-hugo]
    - !reference [.setup-ssh-priv-key]
    - rm -rf ${CI_PROJECT_DIR}/output_build_pipeline
    - mkdir -p ${CI_PROJECT_DIR}/output_build_pipeline
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH # only runs on the default branch (main/master)

deploy_active:
  extends: .deploy
  script:
    - echo "only building active website = ${ACTIVE_WEBSITE_DIR}"
    - cd ${ACTIVE_WEBSITE_DIR} && source BUILD_CONF.env
    - hugo --gc
    - mv -v public/ ${DEST_FOLDER} 
    - mv -v ${DEST_FOLDER} ${CI_PROJECT_DIR}/output_build_pipeline/
    - time rsync -avh --stats --delete -e "ssh -o LogLevel=ERROR -p "$SERVER_SSH_PORT"" 
      ${CI_PROJECT_DIR}/output_build_pipeline/${DEST_FOLDER}/ ${SERVER_SSH_URL}${DEST_FOLDER}

deploy_all:
  extends: .deploy
  when: manual # only run on a manual button click
  script:
    - echo "building all the websites"
    - cd ${CI_PROJECT_DIR}
    - ./build-all-pipeline.sh
    - time rsync -avh --stats --delete -e "ssh -o LogLevel=ERROR -p "$SERVER_SSH_PORT"" 
      ${CI_PROJECT_DIR}/output_build_pipeline/ ${SERVER_SSH_URL}

