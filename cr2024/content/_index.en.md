---
language_code: "en"
title: "CryptoRave"

header:
  descricao:
    ""
sobre:
  id: "sobre"
  descricao:
    [
      "The CryptoRave is an annual event that brings together, in 24 hours (+ party to be announced), various activities on security, encryption, hacking, anonymity, privacy and network freedom.",
      "CryptoRave is **open and free** and will be held in São Paulo, Brazil, registration will be on site.",
      "It's inspired by the decentralized [global action](https://cryptoparty.in) to disseminate and democratize knowledge and basic concepts of cryptography and free software. The event began in 2014 as a reaction to the disclosure of information that confirmed the action governments to keep the world population under constant surveillance and monitoring."
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "CryptoRave 2024 logo: the word 'Cryptorave' written at the top with a drawing of a key over the letter 'O'. On the right side a sequence of zeros and ones. Below the word 'Cryptorave' is an eye with leaves coming out of the back. The entire logo is in lime green."
  logo: "crlogo.png"
  sobre: "About"
  financiamento: "Contribute"
  inscricao: "Attend"
  divulgacao: "Promoting"
  keynotes: "Keynotes"
  programacao: "Schedule"
  festa: "Party"
  realizacao: "Partners"
  apoio: "Support"
  patrocinadores: "Backers"
  contato: "Contact"
  local: "Venue"
  menu_extra:
#    - name: "CFP"
#      url: "https://cpa.cryptorave.org/pt-br/cr2019/cfp/session/new"
#      weight: 9"
#    - name: "Onion"
#      url:  "https://onion.cryptorave.org"
#      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Crowdfunding"
  descricao: "The CryptoRave is made possible through the crowdfunding of an anonymous and distributed community. Is this community that makes possible that each year more and more people can participate in the event. This edition is being financed with the contributions raised for the 2020 edition. But, if you still want to contribute we also have a Bitcoin wallet, which you can donate to using the address below! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code for the Bitcoin address 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."


inscricao:
  id: "inscricao"
  titulo: "Registration"
  # descricao: "We are open for registration for volunteers who want to help with the organization of the event on the day. Sign up using the [form](https://dandara.vedetas.org/index.php/678642?lang=pt-BR)"
  # link_boletim: ""
  # descricao: "We have registrations open for activities. Do you want to present something in this year's edition, sign up using [form](https://dandara.vedetas.org/index.php/173617?lang=pt-br)"
  # link_boletim: "We are also open for registration for volunteers who want to help with the organization of the event on the day, sign up using the [form](https://dandara.vedetas.org/index.php/678642?lang=pt-BR)"
  descricao: "Register for the event using this [form](https://dandara.vedetas.org/index.php/263227?lang=pt-BR)."
  link_boletim: "To keep up with the latest news about Cryptorave, [sign up to receive the event newsletter](https://lists.riseup.net/www/subscribe/cryptorave-boletim)."

divulgacao:
  id: divulgacao
  titulo: "Promoting"
  descricao: "It's up to all of us to promote CryptoRave! Print [our promotional poster](https://we.riseup.net/assets/925438/cartaz-oficial-v3+cr24-a3.png) and place it in your territory, school, college! Spread the word!"
  
keynotes:
  id: "keynotes"
  titulo: "Tor - resisting the dystopia of borderless surveillance"
  data: "May 4, Friday, at 8PM"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brazil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Team coordinator of the Tor Project, recently announced as the next Executive Director of the project."
  descricao: "The problem is much larger than Facebook. It is the economic model of the internet: the surveillance model if the internet. Comminucating, sharing and accessing information should not make you a target or a product. You should not be exploited when using the internet."

programacao:
  id: "programacao"
  titulo: "Schedule"
  img: "programacao.png"
  descricao:
    [
      "Version 2.0 of the CryptoRave 2024 schedule is now [available](https://we.riseup.net/cryptorave/cr2024-programacao)!",
      "Directly access the full version ([color](https://we.riseup.net/assets/928538/programacao-cr2024-v2.0.pdf), [black and white](https://we.riseup.net/assets/928539/programacao-cr2024-v2.0+PB.pdf), and [printable version](https://we.riseup.net/assets/929393/programacao-cr2024-v2.0+impressaoA4.pdf))."
    ]
    # [
    #   "The event will take place on May 10th and 11th."
    # ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'CR2017 logo.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'CR2016 logo.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'CR2015 logo.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'CR2014 logo.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Venue"
  descricao:
    [
      "CryptoRave 2024 will be hosted at [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "Address: R. da Consolação, 94, São Paulo - SP. Close to Anhangabaú subway station."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64421558380128%2C-23.548517016003938%2C-46.64067506790161%2C-23.54628437009063&amp;layer=mapnik"
    link: "https://osm.org/go/M~ziJkQVh?way=48661210"
    tooltip: "Click to open in a new tab"


festa:
  id: "festa"
  titulo: "Party"
  descricao:
    [
      "CryptoRave's after party begins on Saturday, May 4, at 11PM at [Trackers](http://www.trackers.cx/). The entrance is free for CryptoRave 2019 [supporters](#patrocinadores)!",
      "Address: Avenida São João,  (10 minutes walk from Biblioteca Mário de Andrade)"
    ]
  img: "hangar110.jpg"
  link: "http://www.trackers.cx/"

realizacao:
  id: "realizacao"
  titulo: "Partners"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Escola de Ativismo's logo: the text 'ESCOLA DE ATIVISMO' in white at the center of a black background rectangle. The rectangle is a bit wider than the text and height approximately four times that of the text."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Actantes's logo: on a white background, the letters 'ACT' in red, followed by the letters 'ANTES' formed by tiny zeroes and ones in black. There is no spacing between the two parts; the logo is a contiguous word."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Saravá's logo: at the center of a black background rectangle, the word 'SARAVÁ' written in white in a font resembling handwriting. The height of the letters varies from about a third to two thirds the height of the rectangle. The text occupies around four fifths of the rectangle's width."
        url: "https://sarava.org"
      - img: "marialab.png"
        descricao_img: "Marialab's logo, red and black over white background: the words 'maria' in red, under them in black the words 'lab' between square brackets."
        url: "https://www.marialab.org/"
    - linha:

apoio:
  id: "apoio"
  titulo: "Support"
  parceiros:
    - linha:
      - img: "boll.png"
        descricao_img: "Logo of the Heinrich Böll Foundation: three rectangles forming a gradient of green in a row, the name 'Heinrich Böll Stiftung' written in black, by the side below written 'Rio de Janeiro' in green, below 'Brazil' written in green."
        url: "https://br.boell.org/"
      - img: "ccc.jpg"
        descricao_img: "Chaos Computer Club (CCC)'s logo': in black over a white background, the outline of an image that resembles a key and also an electronic circuit. From the inside of a rectangle with chamfered corners, 4 lines direct point toward the midpoint of its right side; from there, they align perpendicularly to this side and leave the rectangle, parallel to one another. After a distance of about half the width of the rectangle, the lines twist, forming a knot. Resembling wires, the lines dangle, twisted, below the knot."
        url: "https://ccc.de/"
    - linha:
      - img: "bma.png"
        descricao_img: "Logo of Biblioteca Mario de Andrade: the inscription Biblioteca Mário de Andrade written in black capital font on a white background, divided into 5 lines, formatted in a rectangle that resembles a book cover."
        url: "https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/"


patrocinadores:
  id: "patrocinadores"
  titulo: "Backers"
  descricao: "We sincerely thank everyone who contributed to our [crowdfunding campaign](#financiamento):"
  doadores:
    [
[
"Adriana Leticya Gontijo",
"Alessio Esteves",
"Alexandre Isaac Siqueira",
"Alexandre Medeiros",
"Alice Christina Matsuo",
"Alice Lana",
"Aline Freitas",
"Ana Elisa",
"Ana Luiza Portello Bastos",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson C",
"Anderson de Jesus Nascimento Ribeiro",
"Anderson Pereira Leal",
"André F. Viana",
"André Luiz ",
"André Rodrigues Ferraz Barbosa",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Anon",
"Antonio Arles dos Anjos Junior",
"Ariel Ferreira Rodrigues",
"Arthur Costa Lima",
"Augusto Bennemann",
"Bruno Kim Medeiros Cesar",
"Bruno Martins",
"BRUNO PAIVA DE OLIVEIRA",
"brunz",
"Cadós Sanchez",
"caioau",
"Caio Eduardo Zangirolami Saldanha",
"Caio Fontes de Castro",
"Caio Henrique Silva Ramos",
"Carla Oliveira Santos",
"Carlos Cabral",
"Carlos S",
"Carolina Vergotti Ferrigno",
"Coalinha das Planilhas",
"cybelle",
"Daniel Miranda Birochi",
"Daniel Rondinelli Roquetti"
],
[
"Dara Gonçalves",
"Diana Iliescu",
"Divina Vitorino",
"Douglas da Silva Costa",
"Douglas Vinicius Esteves",
"Editora Monstro dos Mares",
"Eduardo Costa Lisboa",
"Elisa X",
"Emilio Simoni",
"Enzzo Pessanha Cavallo",
"Estanislau Gonçalves",
"Euler Neto",
"Evelize Pacheco Simões",
"Fabio Barros",
"Fábio Meneghetti",
"Felipe",
"Fernanda Campagnucci",
"Fernanda Fantelli",
"Fernanda Shirakawa",
"Fernao Vellozo",
"Filipe Monguilhott Falcone",
"Francine Emilia Costa",
"Gabriela Bittencourt",
"Gabriel Gortan",
"Gabriella De Biaggi",
"glauber noccioli de souza",
"Guilherme Otero",
"Guilherme Ribeiro de Lima",
"Gustavo Felipe Vieira de Alencar",
"Gustavo Gus",
"Gustavo Suto",
"Gyssele Mendes",
"Henrique Nascimento Santos",
"Ian Fernandez",
"Ian Fernandez",
"Ines Aisengart Menezes",
"Ingrid Elisabeth Spangler",
"Ingrid Elisabeth Spangler",
"Itamar Silva",
"Janaina Menegaz Spode",
"João Moreno Rodrigues Falcão",
"Joao Rafael Bonilha"
],
[
"João Vitor Bizotto Ferreira",
"Júlia Morone",
"Juliana de Melo Barbosa",
"Juliana Soares Rosa",
"Jurre Bourbaki",
"Karine Fernandes Batista",
"Laez Barbosa da Fonseca Filho",
"Laís Figueiredo",
"Larissa Dionisio ",
"Laudelina LP",
"Leonardo dos reis Carvalho",
"Leticia Rodrigues",
"LUCAS DE BARROS",
"Lucas dos Santos TInti",
"Lucas Lago",
"Luci Hidaka",
"Luis Arantes",
"Luis Otavio Ribeiro",
"Luiza Wainer",
"Luiz Guilherme Pereira de Almeida Lins",
"Marcelo Marquesini",
"Marcia Ohlson",
"Maria Cecília Gomes",
"Mariana Pereira Leal",
"marina frota",
"Marisa Sanematsu",
"Marlus Araujo",
"Matheus Paulo Batista Grandi",
"Mayara Ferreira",
"Michel Marechal",
"Miguel Vieira",
"Narrira",
"Nathalia Rodrigues",
"Nathalia Vieira Ferreira",
"Olívia Bandeira",
"Paulo F",
"PRISCILA CORREA BURACOSKY DOS SANTOS",
"Rafael Bantu",
"Rafael Ramblas",
"Rebecca Oliveira",
"Renata Assumpção"
],
[
"Renato Farias de Araujo",
"Renato Zannon",
"Ricardo Shiota Yasuda",
"Rita Taraborelli",
"Roberto Rodrigues",
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo Viana",
"Rodrigo Abrantes da Silva",
"Rodrigo Didier Anderson",
"Rodrigo Ghedin",
"Ronaldo Toshio",
"Rondineli Saad",
"Samer Maalouli",
"Sergio Amadeu da Silveira",
"Thais Mendes",
"Thaís Tavares Lima",
"Thiago Benicchio",
"Thiago Carvalho Bayerlein",
"Thiago Dantas",
"Thiago Leoncio Scherrer",
"Thiago Matos",
"Thiago Wolf Scalabrini",
"Tiago Filgueiras Pimentel",
"Tiago Lubiana",
"Tiago Lubiana",
"Tulio Malaspina",
"Valentina Garavaglia de Souza",
"Valéria Barros",
"Vanessa Pereira dos Anjos",
"Veridiana Alimonti",
"Victor",
"Vinicius ",
"Vinícius Shoiti Koike Graciliano",
"Vitor Hirata Sanches",
"Waldo Almeida Ramalho",
"Wallace Viana",
"William Saraiva Cimino",
"Willian Lopes"
]
     ] 

contato:
  id: "contato"
  titulo: "Contact"
  subtitulo: "Send your questions and suggestions."
  descricao: "Long exposure image of a city at night. A starless sky occupies the upper fifth of the image, while buildings with lit windows occupy the remaining space. In the left lower half of the image, part of a street is visible, with the trajectories of headlights visible due to the long exposure."
  button_text: "Send"
  formspree_language_code: "en"
  validacao:
    nome:
      placeholder: "Name (optional)"
    email:
      placeholder: "Email *"
      aviso: "Enter your email address."
    mensagem:
      placeholder: "Your message *"
      aviso: "Type your message here."

footer:
  # copyright: "CryptoRave.ORG 2024 - Sem privacidade não há liberdade - [Onion services](https://onion.cryptorave.org/)"
  copyright: "CryptoRave.org 2024 - Sem privacidade não há liberdade"
  antiassedio:
    texto: "Code of Conduct"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-instagram"
      link: "https://www.instagram.com/cryptoravebr/"
    - icon: "fa-maxcdn"
      link: "https://mastodon.social/@cryptorave"
    - icon: "fa-telegram"
      link: "https://t.me/CryptoRave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    # - icon: "fa-ticket"
    #   link: "https://tickets.cryptorave.org/2020/free"

---
