---
language_code: "es"
title: "CryptoRave"

header:
  descricao:
    ""
sobre:
  id: "sobre"
  descricao:
    [
      "**la CryptoRave**, es un evento anual de 24 horas (+ fiesta por anunciar) que acoge actividades diversas sobre seguridad, criptografía, hacking, anonimato, privacidad y libertad en la red.",
      "CryptoRave es **abierta y gratuita** y realizada en la Ciudad de São Paulo, el registro se realizará en el sitio.", 
      "Inspirada por una [acción global](https://cryptoparty.in), descentralizada para diseminar y democratizar el conocimiento y conceptos básicos de criptografía y software libre, el evento nació en 2014 como reacción a la difusión de informaciones que confirmaron la acción de gobiernos para mantener a la población mundial bajo permanente vigilancia y escucha."
    ]
  vimeo_link: "https://player.vimeo.com/video/254294396"

nav:
  descricao:
    "Logotipo de CryptoRave 2024: la palabra 'Cryptorave' escrita en la parte superior con el diseño de una clave sobre la letra 'O'. En el lado derecho una secuencia de ceros y unos. Debajo de la palabra 'Cryptorave' hay un ojo del que salen hojas en la parte posterior. Todo el logo está en verde lima."
  logo: "crlogo.png"
  sobre: "Sobre"
  financiamento: "Financiamiento"
  inscricao: "Inscripciones"
  divulgacao: "Divulgación"
  keynotes: "Keynotes"
  programacao: "Programación"
  festa: "Fiesta"
  realizacao: "Realización"
  apoio: "Soporte"
  patrocinadores: "Auspiciantes"
  contato: "Contacto"
  local: "Ubicación"
  menu_extra:
#    - name: "CPA"
#      url: "https://cpa.cryptorave.org/pt-br/cr2020/cfp/session/new"
#      weight: 9"
#    - name: "Onion"
#      url:  "https://onion.cryptorave.org"
#      weight:  11
    - name:  "Blog"
      url:  "https://blog.cryptorave.org"
      weight:  12

financiamento:
  id: "financiamento"
  titulo: "Financiamiento Colectivo"
  descricao: "Cryptorave se realiza con el apoyo financiero y la difusión de una comunidad distribuida y anónima, que asegura, año tras año, que más personas participen en el evento. Esta edición se financia con las contribuciones recaudadas para la edición de 2020. Pero si aún desea contribuir, tenemos un monedero bitcoin al que puedes donar a la siguiente dirección abajo! :-)"
  endereco: "1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  link_blockchain: "https://blockchain.info/address/1K35RGQPCWX3nght23Jetym1WVe2BU14Vu"
  descricao_qrcode: "QR Code para la dirección Bitcoin 1K35RGQPCWX3nght23Jetym1WVe2BU14Vu."

inscricao:
  id: "inscricao"
  titulo: "Inscripciones"
  # descricao: "Estamos abiertos a la inscripción para voluntarios que quieran ayudar con la organización del evento ese día. Regístrate usando el [formulario](https://dandara.vedetas.org/index.php/678642?lang=pt-BR)"
  # link_boletim: ""
  # descricao: "Tenemos inscripciones abiertas para actividades. Quieres presentar algo en la edición de este año, inscríbete mediante [formulario](https://dandara.vedetas.org/index.php/173617?lang=pt-br)."
  # link_boletim: "También estamos abiertos a la inscripción para voluntarios que quieran ayudar con la organización del evento ese día, regístrate usando el [formulario](https://dandara.vedetas.org/index.php/678642?lang=pt-BR)"
  descricao: "Regístrese para el evento usando este [formulario](https://dandara.vedetas.org/index.php/263227?lang=pt-BR)."
  link_boletim: "Para mantenerse al día con las últimas noticias sobre Cryptorave, [regístrese para recibir el boletín del evento](https://lists.riseup.net/www/subscribe/cryptorave-boletim)."

divulgacao:
  id: divulgacao
  titulo: "Divulgación"
  descricao: "¡La divulgación de CryptoRave la hacemos todos nosotros! Imprima [nuestro cartel promocional](https://we.riseup.net/assets/925438/cartaz-oficial-v3+cr24-a3.png) y colóquelo en su territorio , escuela, universidad! ¡Corre la voz!"

keynotes:
  id: "keynotes"
  titulo: "Tor - resistir à distopia da vigilância sem fronteiras"
  data: "04/05, sexta-feira às 20h"
  img: "keynote_cr2018_isabela_tor.png"
  origem: "Brasil"
  nome: "[Isabela Bagueros](https://www.torproject.org/about/corepeople.html.en#isabela)"
  bio: "Coordenadora dos times do Projeto Tor, recentemente foi anunciada como a próxima Diretora Executiva do projeto."
  descricao: "O problema é muito maior que o Facebook. É o modelo econômico da Internet: o modelo de vigilância da Internet. Comunicar, compartilhar e acessar informações não deveria tornar você um alvo ou um produto. Você não deveria ser explorado/a ao usar a Internet."

programacao:
  id: "programacao"
  titulo: "Programación"
  img: "programacao.png"
  descricao:
    [
      "¡La versión 2.0 del calendario CryptoRave 2024 ya está [disponible](https://we.riseup.net/cryptorave/cr2024-programacao)!",
       "Acceda directamente a la versión completa ([color](https://we.riseup.net/assets/928538/programacao-cr2024-v2.0.pdf), [blanco y negro](https://we.riseup.net/assets/928539/programacao-cr2024-v2.0+PB.pdf), y [versión imprimible](https://we.riseup.net/assets/929393/programacao-cr2024-v2.0+impressaoA4.pdf))."
    ]
    # [
    #   "El evento se llevará a cabo los días 10 y 11 de mayo."
    # ]
  anteriores:
    - linha:
      - titulo: "CR2017"
        img: "cr2017logo.png"
        descricao_img: 'Logo de la CR2017.'
        url: "https://2017.cryptorave.org"    
      - titulo: "CR2016"
        img: "cr2016logo.png"
        descricao_img: 'Logo de la CR2016.'
        url: "https://2016.cryptorave.org"
    - linha:
      - titulo: "CR2015"
        img: "cr2015logo.png"
        descricao_img: 'Logo de la CR2015.'
        url: "https://2015.cryptorave.org"
      - titulo: "CR2014"
        img: "cr2014logo.png"
        descricao_img: 'Logo de la CR2014.'
        url: "https://2014.cryptorave.org"

local:
  id: "local"
  titulo: "Ubicación"
  descricao:
    [
      "La CryptoRave 2024 será realizada en [Biblioteca Mário de Andrade](https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/).",
      "Dirección: R. da Consolação, 94, São Paulo - SP. Cerca de la estación Anhangabaú del metro."
    ]
  mapa:
    iframe_src: "https://www.openstreetmap.org/export/embed.html?bbox=-46.64421558380128%2C-23.548517016003938%2C-46.64067506790161%2C-23.54628437009063&amp;layer=mapnik"
    link: "https://osm.org/go/M~ziJkQVh?way=48661210"
    tooltip: "Click para abrir en otra pestaña"


festa:
  id: "festa"
  titulo: "Fiesta en Hangar 110"
  descricao:
    [
      "La fiesta de CryptoRave será el sábado, el 05/05, a las 20h en [Hangar 110](http://www.hangar110.com.br/). La entrada es gratis y exclusiva para participantes de CryptoRave!",
      "Dirección: Rua Rodolfo Miranda, 110 - Bom Retiro (5 minutos a pie del metro Armênia - linea azul)"
    ]
  img: "hangar110.jpg"
  link: "http://www.hangar110.com.br/"
  atracoes:
    [
      "20:00 - abertura de la casa!",
      "**LINEUP**",
      "21:00 - 21:45 - cabeça",
      "22:00 - 23:00 - afrorep",
      "23:00 - 00:00 - datamosh",
      "00:00 - 01:00 - octarina",
      "01:00 - 01:45 - show do retrigger",
      "01:45 - 03:00 - arkanoid",
      "03:00 - 04:15 - swaaag",
      "04:45 - 06:00 - lines",
      "**PROJEÇÕES**",
      "21:45 - 23:00 - Terms and conditions may apply",
      "23:30 - 00:45 - Bruno Treviso",
      "00:45 - 01:45 - The Internet's Own Boy: The Story of Aaron Swartz (trecho)",
      "01:45 - 03:00 - NVVE MVE",
      "03:00 - 04:15 - chr0ma",
      "04:15 - 06:00 - Downloaded"
    ]

realizacao:
  id: "realizacao"
  titulo: "Realización"
  parceiros:
    - linha:
      - img: "eativismo.png"
        descricao_img: "Logo de la Escola de Ativismo: el texto 'ESCOLA DE ATIVISMO' (escuela de activismo) en blanco en el centro de uno rectángulo negro. El largo del rectángulo es un pouco mas grande que el largo del texto y su altura casi cuatro veces mas que la altura del texto."
        url: "https://ativismo.org.br"
      - img: "actantes.png"
        descricao_img: "Logo del Actantes: con fondo blanco, las letras 'ACT' en rojo, y en secuencia las letras 'ANTES' hechas con los números 0 e 1 en negro. No hay espacio entre las dos partes; el logo es una palabra contígua."
        url: "https://actantes.org.br/"
    - linha:
      - img: "sarava.png"
        descricao_img: "Logo del Saravá: al centro de un fondo rectangular negro, la palabra 'SARAVÁ' en blanco en una fuente que se asemeja al texto manuscrito. La altura de las letras varia entre cerca de un tercio y dos tercios de la altura del rectángulo. Horizontalmente, el texto ocupa cerca de cuatro quintos del ancho del rectángulo."
        url: "https://sarava.org"
      - img: "marialab.png"
        descricao_img: "Logotipo de MariaLab con fondo blanco: las palabras 'maria' en rojo, debajo de 'lab' en negro entre corchetes."
        url: "https://www.marialab.org/"

apoio:
  id: "apoio"
  titulo: "Soporte"
  parceiros:
    - linha:
      - img: "boll.png"
        descricao_img: "Logotipo de la Fundación Heinrich Böll: tres rectángulos que forman un degradado de verde en una fila, junto al nombre 'Heinrich Böll Stiftung' escrito en negro, debajo escrito 'Rio de Janeiro' en verde, debajo 'Brasil' escrito en verde."
        url: "https://br.boell.org/"
      - img: "ccc.jpg"
        descricao_img: "Logotipo de Chaos Computer Club (CCC): en negro sobre un fondo blanco, los contornos de una imagen que se parece a una llave y a un circuito electrónico. Desde el interior de un rectángulo con las esquinas chaflanadas, 4 lineas direccionanse al centro del lado derecho; desde allá, alineanse perpendicularmente a este lado y salen del rectángulo, paralelas entre si. Después de una distancia de cerca de la mitad del comprimento del rectángulo, las lineas se retorcen, formando un nudo. Como hilos, las lineas colgan abajo del nudo."
        url: "https://ccc.de/"
    - linha:
      - img: "bma.png"
        descricao_img: "Logotipo de la Biblioteca Mario de Andrade: la inscripción Biblioteca Mário de Andrade escrita en mayúscula negra sobre fondo blanco, dividida en 5 líneas, formateada en un rectángulo que se asemeja a la portada de un libro."
        url: "https://www.prefeitura.sp.gov.br/cidade/secretarias/cultura/bma/"

patrocinadores:
  id: "patrocinadores"
  titulo: "Auspiciantes"
  descricao: "Nuestro más sincero agradecimiento a todos y todas las que contribuyeran con nuestra [campaña de financiamento colectivo](#financiamento):"
  doadores:
    [
[
"Adriana Leticya Gontijo",
"Alessio Esteves",
"Alexandre Isaac Siqueira",
"Alexandre Medeiros",
"Alice Christina Matsuo",
"Alice Lana",
"Aline Freitas",
"Ana Elisa",
"Ana Luiza Portello Bastos",
"Anchises Moraes Guimaraes de Paula",
"Anchises Moraes Guimaraes de Paula",
"Anderson C",
"Anderson de Jesus Nascimento Ribeiro",
"Anderson Pereira Leal",
"André F. Viana",
"André Luiz ",
"André Rodrigues Ferraz Barbosa",
"Andreza Aparecida dos Santos",
"Anna Luíza Gannam",
"Anon",
"Antonio Arles dos Anjos Junior",
"Ariel Ferreira Rodrigues",
"Arthur Costa Lima",
"Augusto Bennemann",
"Bruno Kim Medeiros Cesar",
"Bruno Martins",
"BRUNO PAIVA DE OLIVEIRA",
"brunz",
"Cadós Sanchez",
"caioau",
"Caio Eduardo Zangirolami Saldanha",
"Caio Fontes de Castro",
"Caio Henrique Silva Ramos",
"Carla Oliveira Santos",
"Carlos Cabral",
"Carlos S",
"Carolina Vergotti Ferrigno",
"Coalinha das Planilhas",
"cybelle",
"Daniel Miranda Birochi",
"Daniel Rondinelli Roquetti"
],
[
"Dara Gonçalves",
"Diana Iliescu",
"Divina Vitorino",
"Douglas da Silva Costa",
"Douglas Vinicius Esteves",
"Editora Monstro dos Mares",
"Eduardo Costa Lisboa",
"Elisa X",
"Emilio Simoni",
"Enzzo Pessanha Cavallo",
"Estanislau Gonçalves",
"Euler Neto",
"Evelize Pacheco Simões",
"Fabio Barros",
"Fábio Meneghetti",
"Felipe",
"Fernanda Campagnucci",
"Fernanda Fantelli",
"Fernanda Shirakawa",
"Fernao Vellozo",
"Filipe Monguilhott Falcone",
"Francine Emilia Costa",
"Gabriela Bittencourt",
"Gabriel Gortan",
"Gabriella De Biaggi",
"glauber noccioli de souza",
"Guilherme Otero",
"Guilherme Ribeiro de Lima",
"Gustavo Felipe Vieira de Alencar",
"Gustavo Gus",
"Gustavo Suto",
"Gyssele Mendes",
"Henrique Nascimento Santos",
"Ian Fernandez",
"Ian Fernandez",
"Ines Aisengart Menezes",
"Ingrid Elisabeth Spangler",
"Ingrid Elisabeth Spangler",
"Itamar Silva",
"Janaina Menegaz Spode",
"João Moreno Rodrigues Falcão",
"Joao Rafael Bonilha"
],
[
"João Vitor Bizotto Ferreira",
"Júlia Morone",
"Juliana de Melo Barbosa",
"Juliana Soares Rosa",
"Jurre Bourbaki",
"Karine Fernandes Batista",
"Laez Barbosa da Fonseca Filho",
"Laís Figueiredo",
"Larissa Dionisio ",
"Laudelina LP",
"Leonardo dos reis Carvalho",
"Leticia Rodrigues",
"LUCAS DE BARROS",
"Lucas dos Santos TInti",
"Lucas Lago",
"Luci Hidaka",
"Luis Arantes",
"Luis Otavio Ribeiro",
"Luiza Wainer",
"Luiz Guilherme Pereira de Almeida Lins",
"Marcelo Marquesini",
"Marcia Ohlson",
"Maria Cecília Gomes",
"Mariana Pereira Leal",
"marina frota",
"Marisa Sanematsu",
"Marlus Araujo",
"Matheus Paulo Batista Grandi",
"Mayara Ferreira",
"Michel Marechal",
"Miguel Vieira",
"Narrira",
"Nathalia Rodrigues",
"Nathalia Vieira Ferreira",
"Olívia Bandeira",
"Paulo F",
"PRISCILA CORREA BURACOSKY DOS SANTOS",
"Rafael Bantu",
"Rafael Ramblas",
"Rebecca Oliveira",
"Renata Assumpção"
],
[
"Renato Farias de Araujo",
"Renato Zannon",
"Ricardo Shiota Yasuda",
"Rita Taraborelli",
"Roberto Rodrigues",
"Rodolfo Augusto de Araujo Almeida",
"Rodolfo Viana",
"Rodrigo Abrantes da Silva",
"Rodrigo Didier Anderson",
"Rodrigo Ghedin",
"Ronaldo Toshio",
"Rondineli Saad",
"Samer Maalouli",
"Sergio Amadeu da Silveira",
"Thais Mendes",
"Thaís Tavares Lima",
"Thiago Benicchio",
"Thiago Carvalho Bayerlein",
"Thiago Dantas",
"Thiago Leoncio Scherrer",
"Thiago Matos",
"Thiago Wolf Scalabrini",
"Tiago Filgueiras Pimentel",
"Tiago Lubiana",
"Tiago Lubiana",
"Tulio Malaspina",
"Valentina Garavaglia de Souza",
"Valéria Barros",
"Vanessa Pereira dos Anjos",
"Veridiana Alimonti",
"Victor",
"Vinicius ",
"Vinícius Shoiti Koike Graciliano",
"Vitor Hirata Sanches",
"Waldo Almeida Ramalho",
"Wallace Viana",
"William Saraiva Cimino",
"Willian Lopes"
]
     ] 

contato:
  id: "contato"
  titulo: "Contacto"
  subtitulo: "Envíe su sugerencia o inquietud."
  descricao: "Imagen de una ciudad por la noche, foto tomada con longa exposición. En la parte superior, un cielo sin estrellas rellena un quinto de la imagen, edificios con ventanas alumbradas llenan el espacio restante. En la parte inferior izquierda esta visible parte de una calle, con camino hechos con los faros de los coches debido a la longa exposición."
  button_text: "Enviar"
  enviando: "Aguardando la confirmación del envio..."
  obrigado: "Gracias por ponerse en contacto con nosotros!"
  erro: "Ups, la mensaje no logró ser enviada. Por favor póngase en contacto por el correo: contato@cryptorave.org."
  validacao:
    nome:
      placeholder: "Nombre (opcional)"
    email:
      placeholder: "Correo *"
      aviso: "Digite la dirección de su correo electrónico."
    mensagem:
      placeholder: "Su mensaje *"
      aviso: "Digite su mensaje."


footer:
  # copyright: "CryptoRave.ORG 2024 - Sem privacidade não há liberdade - [Onion services](https://onion.cryptorave.org/)"
  copyright: "CryptoRave.org 2024 - Sem privacidade não há liberdade"
  antiassedio:
    texto: "Código de Conducta"
    link: "https://we.riseup.net/cryptorave/politica-anti-assedio"
  social:
    - icon: "fa-twitter"
      link: "https://twitter.com/cryptoravebr"
    - icon: "fa-facebook"
      link: "https://facebook.com/cryptorave"
    - icon: "fa-instagram"
      link: "https://www.instagram.com/cryptoravebr/"
    - icon: "fa-maxcdn"
      link: "https://mastodon.social/@cryptorave"
    - icon: "fa-telegram"
      link: "https://t.me/CryptoRave"
    - icon: "fa-envelope"
      link: "https://lists.riseup.net/www/subscribe/cryptorave-boletim"
    # - icon: "fa-ticket"
    #   link: "https://tickets.cryptorave.org/2020/free"

---
